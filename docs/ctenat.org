* -------------------------------------
*   documentation des process ctenat
* -------------------------------------

* -------------------------------------
* fiches actions
* -------------------------------------
  ** candiature 
    :view: [[~/d/modules/survey/views/custom/ctenat/dossierEditCTENat.php]]
  ** changement de status d'une action d'un cter 
    [[~/d/modules/costum/controllers/actions/ctenat/admin/PrioAction.php]] 
      Ctenat::updatePrioAnswers
      all states Ctenat.php
      - modifie l'attribut :priorisation: dans :answers:
  ** export des ACTIONs nationales
    {{https://cte.ecologique-solidaire.gouv.fr/costum/ctenat/answerscsv/slug/ctenat/admin/true}} 
    * process 
        [[~/d/modules/costum/controllers/actions/ctenat/api/AnswerscsvAction.php]] 
        - get all projects.CTER
        - can use search to filter results 

* -------------------------------------
* CTER home 
* -------------------------------------
  * petit chiffre de homepage  
  [[~/d/modules/costum/views/custom/ctenat/cterrHPChiffres.php ]] 
    million : [[~/d/modules/costum/views/custom/ctenat/graph/pieMillionCTErr.php]]
  ** onglet
    *** en chiffre
    {{http://127.0.0.1/ph/costum/ctenat/dashboard/slug/cteDuGrandArrasSigneLe11Octobre2018}}
      [[~/d/modules/costum/views/custom/ctenat/dashboard.php ]]
      {{http://127.0.0.1/ph/graph/co/dash/g/costum.views.custom.ctenat.graph.barActionsCTErr/id/barActionsCTErr}}
        [[~/d/modules/graph/views/co/bar.php]]
      {{http://127.0.0.1/ph/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany/id/pieMany}}
        [[~/d/modules/costum/views/custom/ctenat/graph/pieMany.php ]]
* -------------------------------------
* DASHBOARD
* -------------------------------------
:ajax:{{http://127.0.0.1/ph/costum/ctenat/dashboard/url/costum.views.custom.ctenat.dashboard}}
    :view:[[~/d/modules/costum/views/custom/ctenat/dashboard.php]]
      :ajax:{{http://127.0.0.1/ph/graph/co/dash/g/costum.views.custom.ctenat.graph.lineCTE/size/S/id/lineCTE}}
      :ajax:{{http://127.0.0.1/ph/graph/co/dash/g/costum.views.custom.ctenat.graph.barPorteurbyDomaine/size/S/id/barPorteurbyDomaine}}
        :view:[[~/d/modules/costum/views/custom/ctenat/graph/barPorteurbyDomaine.php]]
      :ajax:{{http://127.0.0.1/ph/graph/co/dash/g/costum.views.custom.ctenat.graph.pieFinance/size/S/id/pieFinance}}
        :view:[[~/d/modules/graph/views/co/piePorteurbyDomaine.php]]
      
* -------------------------------------
* PDF
* -------------------------------------
  ** copil 
  [[~/d/modules/costum/controllers/actions/ctenat/pdf/GenerateCopilAction.php]]
  ** fiche action 
  [[~/d/modules/citizenToolKit/controllers/export/PdfElementAction.php]] 
  ** orientation
  [[~/d/modules/costum/controllers/actions/ctenat/pdf/GenerateOrientationAction.php]] 

