<?php
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Bouboule <clement.damiens@communecter.org>
 * Date: 13/11/2019
 */
class MeusecampagnesController extends CommunecterController {


    protected function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'importorga'  		=> 'costum.controllers.actions.meuseCampagnes.admin.ImportorgaAction',
	        'importevent'  		=> 'costum.controllers.actions.meuseCampagnes.admin.ImporteventAction',
	        'saveorga'  		=> 'costum.controllers.actions.meuseCampagnes.admin.SaveOrgaAction',
	        'saveevent'  		=> 'costum.controllers.actions.meuseCampagnes.admin.SaveEventAction',
	    );
	}
}
