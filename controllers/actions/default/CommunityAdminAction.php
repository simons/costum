<?php 
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class CommunityAdminAction extends CAction
{
	public function run( $tpl=null, $view=null ){
		$controller = $this->getController();
		$panelAdmin = $_POST;
		$limitMin=0;
		$stepLim=100;
		if(@$_POST["page"]){
			$limitMin=$limitMin+(100*($_POST["page"]-1));
		}

		$search="";
		if(@$_POST["name"] && !empty($_POST["name"])){
			$search = trim(urldecode($_POST['name']));
		}



		$panelAdmin["context"] = array("id" => $_POST["id"], "collection" => $_POST["collection"]);
		$query = array() ;
		$query = Search::searchString($search, $query);
		$query = array('$and' => array( $query , array("links.".Link::$linksTypes[Person::COLLECTION][$_POST["collection"]].".".$_POST["id"]=> array('$exists' => 1) ) ) ) ;
		//var_dump($query);

		//$query = array("links.".$_POST["collection"].".".$_POST["id"]=> array('$exists' => 1) ) ;

		//$query = Search::searchSourceKey(Yii::app()->session["costum"]["slug"], $query);
		$params["typeDirectory"]=(!empty($panelAdmin["types"]) ? $panelAdmin["types"] : [Organization::COLLECTION]);
		$params["results"] = array();
		if(!empty($panelAdmin["types"])){
			foreach ($panelAdmin["types"] as $key => $value) {
				$params["results"][$value] = PHDB::findAndLimitAndIndex ( $value , $query, $stepLim, $limitMin);

				//if($tpl!="json")
					$params["results"]["count"][$value] = PHDB::count( $value , $query);
			}
		}
		$params["panelAdmin"] = $panelAdmin;
		$page = "groupAdmin";
		if($tpl=="json")
			Rest::json( $params );
		else if(Yii::app()->request->isAjaxRequest)
			echo $controller->renderPartial("/custom/default/".$page,$params,true);
		
		
	}
}
