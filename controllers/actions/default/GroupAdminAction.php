<?php 
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class GroupAdminAction extends CAction
{
	public function run( $tpl=null, $view=null ){
		$controller = $this->getController();

			$params = array();
			$panelAdmin = $_POST;
			$limitMin=0;
			$stepLim=100;
			if(@$_POST["page"]){
				$limitMin=$limitMin+(100*($_POST["page"]-1));
			}

			$search="";
			if(@$_POST["name"] && !empty($_POST["name"])){
				$search = trim(urldecode($_POST['name']));
			}
			$query = array();
			$query = Search::searchString($search, $query); 
			$query = Search::searchSourceKey(Yii::app()->session["costum"]["slug"], $query);
			//Rest::json( $query ); exit ;
			if(!empty($panelAdmin['filters'])){
				$query = Search::searchFilters($panelAdmin['filters'], $query);

			}
			if(!empty($panelAdmin['forced'])){
				foreach ($panelAdmin['forced'] as $key => $value) {
					$query = array('$and' => array( $query , array($key => $value))) ;
				}
				//$query = Search::searchFilters($panelAdmin['forced'], $query);
			}
			if(isset($_POST["toBeValidated"]) && !empty($_POST["toBeValidated"])){
		        $query = array('$and' => 
		              array(  $query , 
		                  array("source.toBeValidated" => array('$exists'=>true) )
		        ) );
		    }
		    if(isset($_POST["private"]) && !empty($_POST["private"])){
		        $query = array('$and' => 
		              array(  $query , 
		                  array("preferences.private" => array('$exists'=>true) )
		        ) );
		    }
			//Rest::json($query); exit;
			$params["forced"]=(!empty($panelAdmin["forced"])) ? $panelAdmin["forced"] : null;
			$params["stepLim"]=$stepLim;
			$params["limitMin"]=$limitMin;
			$params["typeDirectory"]=(!empty($panelAdmin["types"]) ? $panelAdmin["types"] : [Organization::COLLECTION]);
			$params["results"] = array();
			if(!empty($panelAdmin["types"])){
				foreach ($panelAdmin["types"] as $key => $value) {
					//Rest::json($query); exit;
					$params["results"][$value] = PHDB::findAndLimitAndIndex ( $value , $query, $stepLim, $limitMin);
					foreach($params["results"][$value] as $e => $v){
						if(isset($v["creator"])){
							$creator=Element::getElementById($v["creator"], Person::COLLECTION, null, array("name", "email"));
							$creator["id"]=$v["creator"];
							$params["results"][$value][$e]["creator"]=$creator;
						}
					}
					//if($tpl!="json")
					$params["results"]["count"][$value] = PHDB::count( $value , $query);
				}
			}
			$params["panelAdmin"] = $panelAdmin;
			$page = "groupAdmin";
			//Rest::json( $params );
			if($tpl=="json")
				Rest::json( $params );
			else if(Yii::app()->request->isAjaxRequest)
				echo $controller->renderPartial("/custom/default/".$page,$params,true);
		
		
	}
}
