<?php
class GetEventAction extends CAction{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = CoeurNumerique::getEvent();
        
        Rest::json($params);
    }
}