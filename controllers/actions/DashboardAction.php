<?php
//http://127.0.0.1/ph/costum/co/dashboard/sk/laCompagnieDesTierslieux
class DashboardAction extends CAction
{
    public function run($sk=null,$tag=null)
    {
    	$controller = $this->getController();    	

    	$tpl = "costum.views.co.dashboard";
    	$type = Organization::COLLECTION;
    	if(isset(Yii::app()->session['costum']["lists"])) 
    		$lists = Yii::app()->session['costum']["lists"];
    	else 
    		$controller->render("co2.views.default.unTpl",array("msg"=>"Missing Costum Config List","icon"=>"fa-hand-stop-o"));


    	if($tag && false){

    	}
    	else{ 
    		
			$blocks = [];
			$listData = [];
			$listLbls = [];
			foreach ($lists as $family => $lbls) 
			{
				$listData[$family] = [];
				$listLbls[$family] = [];
				$familyCount = 0;
				foreach ($lbls as $ix => $lbl) 
				{
					$fmc = PHDB::count( $type, array("source.key"=>$sk,"tags"=>$lbl ) );
					$listData[$family][] = $fmc;
					$familyCount += $fmc;
					$listLbls[$family][] = $lbl;
				}
				
				$blocks["pie".$family] = [
					"title"   => $family,
					"counter" => $familyCount,
					"graph" => [
						"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany/size/S",
						"key"=>"pieMany".$family,
						"data"=> [
							"datasets"=> [
								[
									"data"=> $listData[$family],
									"backgroundColor"=> Ctenat::$COLORS
									]
								],
							"labels"=> $listLbls[$family]
						]
					]
				];
			}
		
		
    	$params = [
				"title" => "Tableau de bord",
	    		"blocks" 	=> $blocks
	    	];	
    	 }
    	if(Yii::app()->request->isAjaxRequest)
            echo $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		$this->getController()->render($tpl,$params);
        }
    	
    }
}