<?php 
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class UpdateStatusCterAction extends CAction
{
	public function run(){
		$controller = $this->getController();
		$project = PHDB::findOneById( $_POST["collection"] , $_POST["id"], array("name", "preferences", "source"));
		
		// "Territoire Candidat",
		// 				"Territoire Candidat refusé", 
		// 				"Territoire lauréat", 
		// 				"Dossier Territoire Complet",
		// 				"CTE Signé"

		

		if(empty($project["source"]))
			$project["source"] = array();

		if(empty($project["source"]["status"]))
			$project["source"]["status"] = array();

		if(empty($project["source"]["status"]["ctenat"]))
			$project["source"]["status"]["ctenat"] = array();

		$project["source"]["status"]["ctenat"] = $_POST["value"];

		$set = array("source" => $project["source"]);

		if( $_POST["value"] == Ctenat::STATUT_CTER_LAUREAT ){
			unset($project["preferences"]["private"]);
			$set["preferences"] = $project["preferences"];
		} else if($_POST["value"] == Ctenat::STATUT_CTER_REFUSE || $_POST["value"] == Ctenat::STATUT_CTER_CANDIDAT){
			$project["preferences"]["private"] = true;
			$set["preferences"] = $project["preferences"];
		}

		$resUpdate = PHDB::update( Project::COLLECTION,
				    array("_id"=>new MongoId($_POST["id"])), 
				    array('$set' => $set) ) ;

		$res = array(
			"resUpdate" => $resUpdate,
			"project" => $project
		);

		Rest::json($res); exit;
		// $answer = PHDB::findOne( Form::ANSWER_COLLECTION , array("_id"=>new MongoId( $_POST["answerId"] ) ));

		// $form = PHDB::findOne( Form::COLLECTION , array("id"=> $_POST["formId"] ) );

		// $formEl = PHDB::findOne( $form["parentType"] , array("_id" =>new MongoId($form["parentId"] ) ) );
		// $addTags = array( "actionPrincipal", "actionSecondaire", "cibleDDPrincipal", "cibleDDSecondaire");

		// $res = array("result"=> false, "answer"=> $answer , "form"=> $form, "formEl"=> $formEl);
		// if(!empty($answer) && !empty($form)  && !empty($formEl)){
		// 	$key = $_POST["answerSection"];
		// 	$value = (!empty($_POST["answers"]) ? $_POST["answers"] : null);
		// 	$verb = '$set';

		// 	PHDB::update( Form::ANSWER_COLLECTION,
		// 	    array("_id"=>new MongoId($_POST["answerId"])), 
		// 	    array($verb => array($key => $value)));

		// 	$setP =  array();
		// 	$setF =  array();
		// 	if( !empty($answer["answers"]) && 
		// 		!empty($answer["answers"][$_POST["formId"]]) &&
		// 		!empty($answer["answers"][$_POST["formId"]]["answers"]) &&
		// 		!empty($answer["answers"][$_POST["formId"]]["answers"]["project"]) ){
		// 		$idP = $answer["answers"][$_POST["formId"]]["answers"]["project"]["id"] ;
				
		// 		$project = PHDB::findOne( Project::COLLECTION , array("_id"=>new MongoId( $idP ) ));
				
		// 		if($_POST["answers"] == "Action lauréate"){
		// 			if(!empty($answer["answers"][$_POST["formId"]]["answers"]["caracter"])){
		// 				$caracter = $answer["answers"][$_POST["formId"]]["answers"]["caracter"];
		// 				foreach ($addTags as $kLT => $valLT) {
		// 					if( !empty($caracter[$valLT]) ){
		// 						if( empty($project["tags"]) )
		// 							$project["tags"] = array();

		// 						if( empty($formEl["tags"]) )
		// 							$formEl["tags"] = array();
		// 						if(is_array($caracter[$valLT])){
		// 							foreach ($caracter[$valLT] as $kT => $valT) {
		// 								if(!in_array($valT, $project["tags"]))
		// 									$project["tags"][] = $valT ;

		// 								if(!in_array($valT, $formEl["tags"]))
		// 									$formEl["tags"][] = $valT ;
		// 							}
		// 						}else if(is_string($caracter[$valLT])){
		// 							if(!in_array($caracter[$valLT], $project["tags"]))
		// 								$project["tags"][] = $caracter[$valLT] ;

		// 							if(!in_array($caracter[$valLT], $formEl["tags"]))
		// 								$formEl["tags"][] = $caracter[$valLT] ;
		// 						}
		// 					}
		// 				}
		// 				unset($project["preferences"]["private"]);
		// 				$setP["tags"] = $project["tags"];
		// 				$setP["preferences"] = $project["preferences"];
		// 				$setF["tags"] = $formEl["tags"];
		// 			}
		// 		} else if($_POST["answers"] == "Action refusée"){
		// 			$project["preferences"]["private"] = true ;
		// 			$setP["preferences"] = $project["preferences"];

		// 		}

		// 		if(!empty($setP)){
		// 			PHDB::update( Project::COLLECTION,
		// 		    array("_id"=>new MongoId($idP)), 
		// 		    array($verb => $setP) ) ;
		// 		}
				
		// 		if(!empty($setF)){
		// 			PHDB::update( $form["parentType"],
		// 			    array("_id"=>new MongoId($form["parentId"])), 
		// 			    array($verb => $setF ) );
		// 		}

		// 	}

		// 	$res = array("result"=> true, "answer"=> $answer, "setF"=> $setF, "setP"=> $setP);
		// }
		
		Rest::json( $res );
	}
}
