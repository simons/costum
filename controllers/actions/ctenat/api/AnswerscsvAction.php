<?php
class AnswerscsvAction extends CAction{
	public function run($slug = null,$admin=null,$id=null) {
		$controller=$this->getController();
		ini_set('max_execution_time',10000000);
		ini_set('memory_limit', '512M');
		if( !empty($admin) ){
			$cter = PHDB::find(Project::COLLECTION, array("source.key" => $slug, "category" => "cteR"), array("slug"));
		} else 	{
			$cter[$id] = array("slug" => $slug);
		}

		$search="";
		//Rest::json($cter); exit;
		$searchRegExp = Search::accentToRegex($search);
		if( !empty($admin) ){
        	$query = array("source.key" => $slug ) ;
        	PHDB::remove( Form::ANSWER_COLLECTION , 
									[ "formId"=>$slug, 
									  "answers"=> ['$exists'=>0] ] );
		} else {
        	$query = array("formId" => $slug ) ;
        	PHDB::remove( Form::ANSWER_COLLECTION , 
									[ "formId"=>$slug, 
									  "answers.".$slug.".answers.project.id"=> ['$exists'=>0] ] );
        }
        $querySearch =  array();
		if(!empty($_POST["name"])){
			foreach ($cter as $keyC => $valC) {
				array_push($querySearch,array( '$or' => array(
								array( "name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "email" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "answers.".$valC["slug"].".answers.organization.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "answers.".$valC["slug"].".answers.project.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
							)));
			}
			$querySearch = array('$or' => $querySearch);

		}else if(count($cter) > 1){
			$querySearch = array( '$or' => array(
								array( "name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "email" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "answers.".$slug.".answers.organization.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "answers.".$slug.".answers.project.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
							));
			
		}
		

		if(!empty($querySearch))
			$query = array('$and' => array( $query , $querySearch ) ) ;

		// if(!empty($panelAdmin['filters'])){
		// 	$query = Search::searchFilters($panelAdmin['filters'], $query);
		// }
		//$results = array();
		//Rest::json($query); exit;
		$answers = PHDB::find ( Form::ANSWER_COLLECTION , $query);
		$answerList = Form::listForAdmin($answers) ;

		//$results["count"][Form::ANSWER_COLLECTION] = PHDB::count( Form::ANSWER_COLLECTION , $query);
		$newList = array();

		//Rest::json($answerList); exit;
		foreach ($answerList as $key => $value) {
			if( !empty($answerList[$key]["answers"]) &&
				!empty($answerList[$key]["formId"]) &&
				!empty($answerList[$key]["answers"][$answerList[$key]["formId"]]) &&
				!empty($answerList[$key]["answers"][$answerList[$key]["formId"]]["answers"]) ) {
				$answerList[$key]["answers"] = $answerList[$key]["answers"][$answerList[$key]["formId"]]["answers"];
			} else {
				$answerList[$key]["answers"] = array();
			}

			if(	!empty($answerList[$key]["user"]) && 
				!empty($answerList[$key]["answers"]) &&
				!empty($answerList[$key]["answers"]["project"]) && 
				!empty($answerList[$key]["answers"]["project"]["id"]) ) {
				$newList[$key] = array();

				//$newList[$key]["countComment"] = PHDB::count(Comment::COLLECTION, array("contextId"=>$key,"contextType"=>Form::ANSWER_COLLECTION));
				$newList[$key]["Id Action"] = $key;
				
				if(!empty($answerList[$key]["name"])){
					$newList[$key]["Nom Createur"] = $answerList[$key]["name"];
				}

				if(!empty($answerList[$key]["created"])){
					$newList[$key]["Date Creation"] = date("d/m/Y", $answerList[$key]["created"]);
				}

				if(!empty($answerList[$key]["formId"])){
					$cter = PHDB::findOne(Project::COLLECTION,array("slug" => $answerList[$key]["formId"]), array("name") );
					$newList[$key]["Nom CTE"] = $cter["name"] ;
				}

				

				if(!empty($answerList[$key]["answers"]["project"]) && !empty($answerList[$key]["answers"]["project"]["id"])){
					$project = PHDB::findOneById(Project::COLLECTION,$answerList[$key]["answers"]["project"]["id"], array("name", "description", "shortDescription", "tags", "expected") );
					$newList[$key]["Nom Action"] = $project["name"];
					unset($answerList[$key]["answers"]["project"]);
				}

				if(!empty($answerList[$key]["priorisation"])){
					$newList[$key]["Statut Action"] = $answerList[$key]["priorisation"];
				}

				if(!empty($project["shortDescription"])){
					$newList[$key]["Description courte"] = $project["shortDescription"];
				}

				$newList[$key]["Porteurs"] = array();
				if(!empty($answerList[$key]["answers"]["organization"])  && !empty($answerList[$key]["answers"]["organization"]["id"])){
					$org = PHDB::findOneById(Organization::COLLECTION,$answerList[$key]["answers"]["organization"]["id"], array("name", "type") );
					$newList[$key]["Porteurs"][] = array("Nom" => $org["name"],
											"Type" => $org["type"]) ;
					unset($answerList[$key]["answers"]["organization"]);
				}

				if(!empty($answerList[$key]["answers"]["organizations"])){

					foreach ($answerList[$key]["answers"]["organizations"] as $keyORG => $valueORG) {

						if($keyORG != "orgasLinked"){
							//var_dump($value);
							$org = PHDB::findOneById(Organization::COLLECTION,$valueORG["id"], array("name", "type") );
							//var_dump($org);
							$newList[$key]["Porteurs"][] = array("Nom" => $org["name"],
																	"Type" => $org["type"]) ;
						}else{
                            foreach ($valueORG as $keyORG2 => $valueORG2) {
                                if($keyORG2 !== "orgasLinked"){
                                    $org = PHDB::findOneById(Organization::COLLECTION,$valueORG2["id"], array("name", "type") );
									if(!empty($org) && !empty($org["name"])){
                                        $newList[$key]["Porteurs"][] = array("Nom" => $org["name"],
																	"Type" => $org["type"]) ;
                                    }
                                }
                            }
                        }
					}
					unset($answerList[$key]["answers"]["organizations"]);
				}

				if(!empty($project["expected"])){
					$newList[$key]["Attentes"] = $project["expected"];
				}

				if(!empty($project["description"])){
					$newList[$key]["Description détaillée"] = $project["description"];
				}

				$newList[$key]["Mot-clés"] = array();
				if(!empty($project["tags"])){
					$t = array();
					foreach ($project["tags"] as $keyT => $valT) {
						$t[] = $valT;
					}
					$where = array("name" => array('$in' => $t),
									"source.key" => "ctenat" ) ;
					$badges = PHDB::find(Badge::COLLECTION, $where, array("name") );
					if( !empty($badges)){
						foreach ($badges as $keyB => $valB) {
							if(in_array($valB["name"], $t)){
								$i = array_search($valB["name"], $t);
								unset( $t[$i]);
							}
						}
					}
					$t2 = array();
					foreach ($t as $keyT => $valT) {
						$t2[] = $valT;
					}
					// $newList[$key]["Badges"] = $badges;
					// $newList[$key]["where"] = $where;
					$newList[$key]["Mot-clés"] = $t2;
				}

				if( !empty($answerList[$key]["answers"]["caracter"]) ) {

					if(!empty($answerList[$key]["answers"]["caracter"]["actionPrincipal"])){
						$newList[$key]["Domaine action principal"] = $answerList[$key]["answers"]["caracter"]["actionPrincipal"];
					}else
						$newList[$key]["Domaine action principal"] = "";
						
					if(!empty($answerList[$key]["answers"]["caracter"]["actionSecondaire"])){
						$newList[$key]["Domaine action secondaire"] = $answerList[$key]["answers"]["caracter"]["actionSecondaire"];
					}else
						$newList[$key]["Domaine action secondaire"] = array();

					if(!empty($answerList[$key]["answers"]["caracter"]["cibleDDPrincipal"])){

						if(is_string($answerList[$key]["answers"]["caracter"]["cibleDDPrincipal"]))
							$newList[$key]["Objectif principal"] = $answerList[$key]["answers"]["caracter"]["cibleDDPrincipal"];
						else
							$newList[$key]["Objectif principal"] = $answerList[$key]["answers"]["caracter"]["cibleDDPrincipal"][0];
					}else
						$newList[$key]["Objectif principal"] = array();

					if(!empty($answerList[$key]["answers"]["caracter"]["cibleDDSecondaire"])){
						$newList[$key]["Objectif secondaire"] = $answerList[$key]["answers"]["caracter"]["cibleDDSecondaire"];
					}else
						$newList[$key]["Objectif secondaire"] = array();
				} else {
					$newList[$key]["Domaine action principal"] = "";
					$newList[$key]["Domaine action secondaire"] = array();
					$newList[$key]["Objectif principal"] = array();
					$newList[$key]["Objectif secondaire"] = array();
				}

				$dependFonction = array();
				$dependInvest = array();
				$totalF = 0;
				$totalI = 0;

				$region = array();
				$etat = array();
				$departement = array();
				$autre = array();
				$public = array();
				$europe = array();
				$private = array();
				$acteur = array();
				$totalregion = 0;
				$totaletat = 0;
				$totaldepartement = 0;
				$totalpublic = 0;
				$totalautre = 0;
				$totaleurope = 0;
				$totalprivate = 0;
				$totalacteur = 0;

				$colfinanceur = array() ;
				$totalcolfinanceur = 0;

				$ademe= array();
				$totalademe = 0;
				$cerema= array();
				$totalcerema = 0;
				$bpi= array();
				$totalbpi = 0;
				$eau= array();
				$totaleau = 0;
				$bio= array();
				$totalbio = 0;
				$afd= array();
				$totalafd = 0;
				$vnf= array();
				$totalvnf = 0;
				$fam= array();
				$totalfam = 0;
				$bdt= array();
				$totalbdt = 0;
				$officeNatForet= array();
				$totalofficeNatForet = 0;
								

				if( !empty($answerList[$key]["answers"]["murir"]) ) {
					if(!empty($answerList[$key]["answers"]["murir"]["budget"])){
						foreach ($answerList[$key]["answers"]["murir"]["budget"] as $keyB => $valB) {
							$fin = array("Libellé" => $valB["poste"],
												"2019" => $valB["amount2019"],
												"2020" => $valB["amount2020"],
												"2021" => $valB["amount2021"],
												"2022" => $valB["amount2022"] ) ;
							if($valB["nature"] == "fonctionnement"){
								$dependFonction[] = $fin ;
								$totalF += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							} else if($valB["nature"] == "investissement"){
								$dependInvest[] = $fin ;
								$totalI += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							}
						}
					}

					if(!empty($answerList[$key]["answers"]["murir"]["planFinancement"])){
						// $newList[$key]["Depense fonctionnement"] = array();
						// $newList[$key]["Depense Investissement"] = array();

						
						foreach ($answerList[$key]["answers"]["murir"]["planFinancement"] as $keyB => $valB) {

							// if(!empty($valB["poste"]))
							// 	$valB["title"] = $valB["poste"];

							// if(!empty($valB["nature"]))
							// 	$valB["financer"] = $valB["nature"];

							$fin = array( "Libellé" => $valB["title"],
											"2019" => $valB["amount2019"],
											"2020" => $valB["amount2020"],
											"2021" => $valB["amount2021"],
											"2022" => $valB["amount2022"],
											"validation" => " ") ;
							
							if(!empty($valB["valid"]))
								$fin["validation"] = $valB["valid"];

							if($valB["financerType"] == "acteursocioeco"){
								$acteur[] = $fin ;
								$totalacteur += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							}else if($valB["financerType"] == "region"){
								$region[] = $fin ;
								$totalregion += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							} else if($valB["financerType"] == "etat"){
								$etat[] = $fin ;
								$totaletat += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							} else if($valB["financerType"] == "departement"){
								$departement[] = $fin ;
								$totaldepartement += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							} else if($valB["financerType"] == "autre"){
								$autre[] = $fin ;
								$totalautre += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							} else if($valB["financerType"] == "public"){
								$public[] = $fin ;
								$totalpublic += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							} else if($valB["financerType"] == "europe"){
								$europe[] = $fin ;
								$totaleurope += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							}else if($valB["financerType"] == "private"){
								$private[] = $fin ;
								$totalprivate += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							}else if($valB["financerType"] == "ademe"){
								$ademe[] = $fin ;
								$totalademe += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							}else if($valB["financerType"] == "cerema"){
								$cerema[] = $fin ;
								$totalcerema += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							}else if($valB["financerType"] == "bpi"){
								$bpi[] = $fin ;
								$totalbpi += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							}else if($valB["financerType"] == "bdt"){
								$bdt[] = $fin ;
								$totalbdt += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								
							}else if($valB["financerType"] == "agenceLeau"){
								$eau[] = $fin ;
								$totaleau += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							}else if($valB["financerType"] == "agenceBiodiv"){
								$bio[] = $fin ;
								$totalbio += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							}else if($valB["financerType"] == "afd"){
								$afd[] = $fin ;
								$totalafd += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							}else if($valB["financerType"] == "vn2f"){
								$vnf[] = $fin ;
								$totalvnf += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							}else if($valB["financerType"] == "franceAgirMer"){
								$fam[] = $fin ;
								$totalfam += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							}else if($valB["financerType"] == "colfinanceur"){
								$colfinanceur[] = $fin ;
								$totalcolfinanceur += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							}else if($valB["financerType"] == "officeNatForet"){
								$officeNatForet[] = $fin ;
								$totalofficeNatForet += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							}
							
						}
					}

					if(!empty($answerList[$key]["answers"]["murir"]["calendar"])){
						foreach ($answerList[$key]["answers"]["murir"]["calendar"] as $keyC => $valC) {
							$newList[$key]["Calendrier"][] = array("Etape" => $valC["step"],
												"Date Début" => $valC["startDate"],
												"Date Fin" => $valC["endDate"] ) ;
						}
					}


					if(!empty($answerList[$key]["answers"]["murir"]["partenaires"])){
						foreach ($answerList[$key]["answers"]["murir"]["partenaires"] as $keyC => $valP) {
							$part = array("Qui" => $valP["qui"],
											"Engagement" => $valP["engagement"],
											"Statut" => $valP["statut"],
											"Next" => $valP["next"] ) ;
							if(!empty($valP["role"])){
								$part["Roles"] =  implode(";", $valP["role"]);
							}
							$newList[$key]["Partenaires"][] = $part;
						}
					}

					if(!empty($answerList[$key]["answers"]["murir"]["results"])){
						foreach ($answerList[$key]["answers"]["murir"]["results"] as $keyC => $valR) {
							if(!empty($valR["indicateur"])){
								$name = PHDB::findOneById(Poi::COLLECTION, $valR["indicateur"], array("name") );
								if(!empty($name)){
									$nene = array( "indicateur" => $name["name"] );

									if(!empty($valR["objectif"])){
										$nene["objectif"] = $valR["objectif"];
									}
									if(!empty($valR["reality"])){
										$nene["reality"] = $valR["reality"];
									}
									$newList[$key]["Indicateurs"][] = $nene;
								}
							}
						}
					}
				}

				$newList[$key]["Total Dépenses HT"] = $totalF + $totalI;
				$newList[$key]["Total dépenses fonctionnement (toutes années)"] = $totalF ;
				$newList[$key]["Depense fonctionnement"] = $dependFonction;
				$newList[$key]["Total dépenses investissement (toutes années)"] = $totalI ;
				$newList[$key]["Depense Investissement"] = $dependInvest;
				$newList[$key]["Plan Financement - Acteur socio-économique"] = $acteur;
				$newList[$key]["Plan Financement - Communes - intercommunalité-syndicat"] = $colfinanceur;
				$newList[$key]["Plan Financement - Département"] = $departement;
				$newList[$key]["Plan Financement - Region"] = $region;
				$newList[$key]["Plan Financement - Europe"] = $europe;
				$newList[$key]["Plan Financement - Etat - Services déconcentrés / préfecture"] = $etat;
				$newList[$key]["Plan Financement - Etat - ADEME"] = $ademe;
				$newList[$key]["Plan Financement - Etat - CEREMA"] = $cerema;
				$newList[$key]["Plan Financement - Etat - Banque des territoires"] = $bdt;
				$newList[$key]["Plan Financement - Etat - BPI"] = $bpi;
				$newList[$key]["Plan Financement - Etat - Agence / office de l’eau"] = $eau;
				$newList[$key]["Plan Financement - Etat - Office français de la biodiversité"] = $bio;
				$newList[$key]["Plan Financement - Etat - Office national des forêts"] = $officeNatForet;
				$newList[$key]["Plan Financement - Etat - Agence française de développement"] = $afd;
				$newList[$key]["Plan Financement - Etat - Voies navigables de France"] = $vnf;
				$newList[$key]["Plan Financement - Etat - FranceAgriMer"] = $fam;
				$newList[$key]["Plan Financement - Etat - Autre"] = $autre;
				//$newList[$key]["Plan Financement - Public"] = $public;
				
				//$newList[$key]["Plan Financement - Private"] = $private;

				// $newList[$key]["Plan Financement Total - Etat"] = $totaletat;
				// $newList[$key]["Plan Financement Total - Département"] = $totaldepartement;
				// $newList[$key]["Plan Financement Total - Region"] = $totalregion;
				// $newList[$key]["Plan Financement Total - Autre"] = $totalautre;
				// $newList[$key]["Plan Financement Total - Europe"] = $totaleurope;
				// $newList[$key]["Plan Financement Total - Private"] = $totalprivate;


				if(!empty($answerList[$key]["validation"])){
					$newList[$key]["État de la validation copil"] = (( !empty($answerList[$key]["validation"]["cter"]) && !empty($answerList[$key]["validation"]["cter"]["valid"])) ? Ctenat::$listValid[$answerList[$key]["validation"]["cter"]["valid"]] :  " ") ;
					$newList[$key]["Commentaire copil"] = (( !empty($answerList[$key]["validation"]["cter"]) && !empty($answerList[$key]["validation"]["cter"]["description"])) ? $answerList[$key]["validation"]["cter"]["description"] :  " ") ;

					$newList[$key]["Etat de la validation régional"] = (( !empty($answerList[$key]["validation"]["etat"]) && !empty($answerList[$key]["validation"]["etat"]["valid"])) ? Ctenat::$listValid[$answerList[$key]["validation"]["etat"]["valid"]] :  " ") ;
					$newList[$key]["Commentaire régional"] = (( !empty($answerList[$key]["validation"]["etat"]) && !empty($answerList[$key]["validation"]["etat"]["description"])) ? $answerList[$key]["validation"]["etat"]["description"] :  " ") ;

					$newList[$key]["Etat de la validation national"] = (( !empty($answerList[$key]["validation"]["ctenat"]) && !empty($answerList[$key]["validation"]["ctenat"]["valid"])) ? Ctenat::$listValid[$answerList[$key]["validation"]["ctenat"]["valid"]] :  " ") ;
					$newList[$key]["Commentaire national"] = (( !empty($answerList[$key]["validation"]["ctenat"]) && !empty($answerList[$key]["validation"]["ctenat"]["description"])) ? $answerList[$key]["validation"]["ctenat"]["description"] :  " ") ;
				}
			}
			
		}

		
		$order = array(
			"Id Action",
			"Nom Createur",
			"Date Creation",
			"Nom CTE",
			"Nom Action",
			"Statut Action",
			"Description courte",
			"Description détaillée",
			"Mot-clés",
			"Attentes",
			"Porteurs",
			"Domaine action principal",
			"Domaine action secondaire",
			"Objectif principal",
			"Objectif secondaire",
			"Indicateurs",
			"Partenaires",
			"Calendrier",
			"Depense fonctionnement",
			"Depense Investissement",
			"Total Dépenses HT",
			"Total dépenses fonctionnement (toutes années)",
			"Total dépenses investissement (toutes années)",
			"Plan Financement - Acteur socio-économique",
			"Plan Financement - Communes - intercommunalité-syndicat",
			"Plan Financement - Département",
			"Plan Financement - Region",
			"Plan Financement - Europe",
			"Plan Financement - Etat - Services déconcentrés / préfecture",
			"Plan Financement - Etat - ADEME",
			"Plan Financement - Etat - CEREMA",
			"Plan Financement - Etat - Banque des territoires",
			"Plan Financement - Etat - BPI",
			"Plan Financement - Etat - Agence / office de l’eau",
			"Plan Financement - Etat - Office français de la biodiversité",
			"Plan Financement - Etat - Office national des forêts",
			"Plan Financement - Etat - Agence française de développement",
			"Plan Financement - Etat - Voies navigables de France",
			"Plan Financement - Etat - FranceAgriMer",
			"Plan Financement - Etat - Autre",
			"État de la validation copil",
			"Commentaire copil",
			"Etat de la validation régional",
			"Commentaire régional",
			"Etat de la validation national",
			"Commentaire national",
		);

		Rest::csv($newList, false, false, null, $order);

		Yii::app()->end();

	}
}