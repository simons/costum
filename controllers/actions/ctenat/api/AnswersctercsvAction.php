<?php
class AnswersctercsvAction extends CAction{
	public function run($slug = null,$admin=null,$id=null) {
		$controller=$this->getController();
		ini_set('max_execution_time',10000000);
		

		if( !empty($admin) ){
			$cter = PHDB::find(Project::COLLECTION, array("source.key" => $slug, "category" => "cteR"), array("slug"));
		} else 	{
			$cter[$id] = array("slug" => $slug);
		}

		$search="";

		$searchRegExp = Search::accentToRegex($search);
		if( !empty($admin) ){
        	$query = array("source.key" => $slug ) ;
        	PHDB::remove( Form::ANSWER_COLLECTION , 
									[ "formId"=>$slug, 
									  "answers"=> ['$exists'=>0] ] );
		} else {
        	$query = array("formId" => $slug ) ;
        	PHDB::remove( Form::ANSWER_COLLECTION , 
									[ "formId"=>$slug, 
									  "answers.".$slug.".answers.project.id"=> ['$exists'=>0] ] );
        }
        $querySearch =  array();
		if(!empty($_POST["name"])){
			foreach ($cter as $keyC => $valC) {
				array_push($querySearch,array( '$or' => array(
								array( "name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "email" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "answers.".$valC["slug"].".answers.organization.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "answers.".$valC["slug"].".answers.project.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
							)));
			}
			$querySearch = array('$or' => $querySearch);

		}else if(count($cter) > 1){
			$querySearch = array( '$or' => array(
								array( "name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "email" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "answers.".$slug.".answers.organization.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
								array( "answers.".$slug.".answers.project.name" => new MongoRegex("/.*{$searchRegExp}.*/i")),
							));
			
		}
		

		if(!empty($querySearch))
			$query = array('$and' => array( $query , $querySearch ) ) ;

		// if(!empty($panelAdmin['filters'])){
		// 	$query = Search::searchFilters($panelAdmin['filters'], $query);
		// }
		//$results = array();
		$answers = PHDB::find ( Form::ANSWER_COLLECTION , $query);
		$answerList = Form::listForAdmin($answers) ;

		//$results["count"][Form::ANSWER_COLLECTION] = PHDB::count( Form::ANSWER_COLLECTION , $query);
		$newList = array();
		foreach ($answerList as $key => $value) {

			if(!empty($answerList[$key]["priorisation"]) && $answerList[$key]["priorisation"]== Ctenat::STATUT_ACTION_VALID ){
				$newList[$key]["Statut Action"] = $answerList[$key]["priorisation"];
			
				$newList[$key] = array();
				

				//$newList[$key]["countComment"] = PHDB::count(Comment::COLLECTION, array("contextId"=>$key,"contextType"=>Form::ANSWER_COLLECTION));
				$newList[$key]["Id Action"] = $key;
				
				// if(!empty($answerList[$key]["name"])){
				// 	$newList[$key]["Nom Createur"] = $answerList[$key]["name"];
				// }

				// if(!empty($answerList[$key]["created"])){
				// 	$newList[$key]["Date Creation"] = date("d/m/Y", $answerList[$key]["created"]);
				// }

				if(!empty($answerList[$key]["formId"])){
					$cter = PHDB::findOne(Project::COLLECTION,array("slug" => $answerList[$key]["formId"]), array("name") );
					$newList[$key]["Nom CTE"] = $cter["name"] ;
				}

				if( !empty($answerList[$key]["answers"]) &&
					!empty($answerList[$key]["formId"]) &&
					!empty($answerList[$key]["answers"][$answerList[$key]["formId"]]) &&
					!empty($answerList[$key]["answers"][$answerList[$key]["formId"]]["answers"]) ) {
					$answerList[$key]["answers"] = $answerList[$key]["answers"][$answerList[$key]["formId"]]["answers"];
				} else {
					$answerList[$key]["answers"] = array();
				}

				if(!empty($answerList[$key]["answers"]["project"]) && !empty($answerList[$key]["answers"]["project"]["id"])){
					$project = PHDB::findOneById(Project::COLLECTION,$answerList[$key]["answers"]["project"]["id"], array("name", "description", "shortDescription", "tags", "expected") );
					$newList[$key]["Nom Action"] = $project["name"];
					unset($answerList[$key]["answers"]["project"]);
				}

				
				// if(!empty($project["shortDescription"])){
				// 	$newList[$key]["Description courte"] = $project["shortDescription"];
				// }

				$newList[$key]["Porteurs"] = "";
				if(!empty($answerList[$key]["answers"]["organization"])  && !empty($answerList[$key]["answers"]["organization"]["id"])){
					$org = PHDB::findOneById(Organization::COLLECTION,$answerList[$key]["answers"]["organization"]["id"], array("name", "type") );
					$newList[$key]["Porteurs"] .= $org["name"]." " ;
					unset($answerList[$key]["answers"]["organization"]);
				}

				if(!empty($answerList[$key]["answers"]["organizations"])){

					foreach ($answerList[$key]["answers"]["organizations"] as $keyORG => $valueORG) {

						if($keyORG != "orgasLinked"){
							//var_dump($value);
							$org = PHDB::findOneById(Organization::COLLECTION,$valueORG["id"], array("name", "type") );
							// $newList[$key]["Porteurs"][] = array("Nom" => $org["name"],
							// 										"Type" => $org["type"]) ;
							$newList[$key]["Porteurs"] .= "\n".$org["name"]." " ;
						}else{
                            foreach ($valueORG as $keyORG2 => $valueORG2) {
                                if($keyORG2 !== "orgasLinked"){
                                    $org = PHDB::findOneById(Organization::COLLECTION,$valueORG2["id"], array("name", "type") );
                                    if(!empty($org) && !empty($org["name"])){

                                        $newList[$key]["Porteurs"] .= "\n".$org["name"]." " ;
                                    }
                                }
                            }
                        }
					}
					unset($answerList[$key]["answers"]["organizations"]);
				}

				if(!empty($answerList[$key]["priorisation"])){
					$newList[$key]["Statut Action"] = $answerList[$key]["priorisation"];
				}



				$totalF = 0;
				$totalI = 0;
				$totalregion = 0;
				$totaletat = 0;
				$totaldepartement = 0;
				$totalpublic = 0;
				$totalautre = 0;
				$totaleurope = 0;
				$totalprivate = 0;
				$totalacteur = 0;
				$totalcolfinanceur = 0;
				$totalademe = 0;
				$totalcerema = 0;
				$totalbpi = 0;
				$totaleau = 0;
				$totalbio = 0;
				$totalafd = 0;
				$totalvnf = 0;
				$totalfam = 0;
				$totalbdt = 0;
				$totalofficeNatForet = 0;
								

				if( !empty($answerList[$key]["answers"]["murir"]) ) {
					if(!empty($answerList[$key]["answers"]["murir"]["budget"])){
						foreach ($answerList[$key]["answers"]["murir"]["budget"] as $keyB => $valB) {
							$fin = array("Libellé" => $valB["poste"],
												"2019" => $valB["amount2019"],
												"2020" => $valB["amount2020"],
												"2021" => $valB["amount2021"],
												"2022" => $valB["amount2022"] ) ;
							if($valB["nature"] == "fonctionnement"){
								//$dependFonction[] = $fin ;
								$totalF += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							} else if($valB["nature"] == "investissement"){
								//$dependInvest[] = $fin ;
								$totalI += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
							}
						}
					}

					if(!empty($answerList[$key]["answers"]["murir"]["planFinancement"])){
						// $newList[$key]["Depense fonctionnement"] = array();
						// $newList[$key]["Depense Investissement"] = array();

						
						foreach ($answerList[$key]["answers"]["murir"]["planFinancement"] as $keyB => $valB) {
							if(!empty($valB["valid"]) && $valB["valid"]=="validated"){


								$fin = array( "Libellé" => $valB["title"],
												"2019" => $valB["amount2019"],
												"2020" => $valB["amount2020"],
												"2021" => $valB["amount2021"],
												"2022" => $valB["amount2022"],
												"validation" => "") ;
								
									$fin["validation"] = $valB["valid"];

								if($valB["financerType"] == "acteursocioeco"){
									//$acteur[] = $fin ;
									$totalacteur += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								}else if($valB["financerType"] == "region"){
									//$region[] = $fin ;
									$totalregion += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								} else if($valB["financerType"] == "etat"){
									//$etat[] = $fin ;
									$totaletat += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								} else if($valB["financerType"] == "departement"){
									//$departement[] = $fin ;
									$totaldepartement += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								} else if($valB["financerType"] == "autre"){
									$totalautre += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								} else if($valB["financerType"] == "public"){
									//$public[] = $fin ;
									$totalpublic += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								} else if($valB["financerType"] == "europe"){
									//$europe[] = $fin ;
									$totaleurope += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								}else if($valB["financerType"] == "private"){
									//$private[] = $fin ;
									$totalprivate += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								}else if($valB["financerType"] == "ademe"){
									//$ademe[] = $fin ;
									$totalademe += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								}else if($valB["financerType"] == "cerema"){
									//$cerema[] = $fin ;
									$totalcerema += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								}else if($valB["financerType"] == "bpi"){
									//$bpi[] = $fin ;
									$totalbpi += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								}else if($valB["financerType"] == "bdt"){
									//$bdt[] = $fin ;
									$totalbdt += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
									
								}else if($valB["financerType"] == "agenceLeau"){
									//$eau[] = $fin ;
									$totaleau += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								}else if($valB["financerType"] == "agenceBiodiv"){
									//$bio[] = $fin ;
									$totalbio += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								}else if($valB["financerType"] == "afd"){
									//$afd[] = $fin ;
									$totalafd += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								}else if($valB["financerType"] == "vn2f"){
									//$vnf[] = $fin ;
									$totalvnf += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								}else if($valB["financerType"] == "franceAgirMer"){
									//$fam[] = $fin ;
									$totalfam += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								}else if($valB["financerType"] == "colfinanceur"){
									//$totalcolfinanceur[] = $fin ;
									$totalcolfinanceur += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								}else if($valB["financerType"] == "officeNatForet"){
									//$totalcolfinanceur[] = $fin ;
									$totalofficeNatForet += $valB["amount2019"]+$valB["amount2020"]+$valB["amount2021"]+$valB["amount2022"];
								}
							}
						}
					}
				}



				$newList[$key]["Somme des dépenses"] = $totalF + $totalI ;
				$newList[$key]["En Fonctionnement"] = $totalF ;
				$newList[$key]["En Investissement"] = $totalI ;
				
				$newList[$key]["Somme Acteur socio-économique"] = $totalacteur;
				$newList[$key]["Somme Communes - intercommunalité-syndicat"] = $totalcolfinanceur;
				$newList[$key]["Somme Département"] = $totaldepartement;
				$newList[$key]["Somme Region"] = $totalregion;
				$newList[$key]["Somme Europe"] = $totaleurope;
				//$newList[$key]["Somme Autre"] = $totalautre;
				$newList[$key]["Somme Etat"] = $totaletat + $totalademe + $totalcerema + $totalbdt + $totalbpi + $totaleau + $totalbio + $totalafd + $totalvnf + $totalfam + $totalautre + $totalofficeNatForet;
				$newList[$key]["Somme Etat - Services déconcentrés / préfecture"] = $totaletat;
				$newList[$key]["Somme Etat - ADEME"] = $totalademe;
				$newList[$key]["Somme Etat - CEREMA"] = $totalcerema;
				$newList[$key]["Somme Etat - Banque des territoires"] = $totalbdt;
				$newList[$key]["Somme Etat - BPI"] = $totalbpi;
				$newList[$key]["Somme Etat - Agence / office de l’eau"] = $totaleau;
				$newList[$key]["Somme Etat - Office français de la biodiversité"] = $totalbio;
				$newList[$key]["Somme Etat - Office national des forêts"] = $totalofficeNatForet;
				$newList[$key]["Somme Etat - Agence française de développement"] = $totalafd;
				$newList[$key]["Somme Etat - Voies navigables de France"] = $totalvnf;
				$newList[$key]["Somme Etat - FranceAgriMer"] = $totalfam;
				$newList[$key]["Somme Etat - Autre"] = $totalautre;

				$newList[$key]["Somme de tous les financements validés"] = $totalacteur + $totalcolfinanceur + $totaldepartement + $totalregion + $totaleurope + $newList[$key]["Somme Etat"] ;

				
				//$newList[$key]["Somme Public"] = $public;
				
				//$newList[$key]["Somme Private"] = $private;

				// $newList[$key]["Plan Financement Total - Etat"] = $totaletat;
				// $newList[$key]["Plan Financement Total - Département"] = $totaldepartement;
				// $newList[$key]["Plan Financement Total - Region"] = $totalregion;
				// $newList[$key]["Plan Financement Total - Autre"] = $totalautre;
				// $newList[$key]["Plan Financement Total - Europe"] = $totaleurope;
				// $newList[$key]["Plan Financement Total - Private"] = $totalprivate;
			}
		}

		$order = array(
			"Nom Action",
			"Statut Action",
			"Porteurs",
			"Somme des dépenses",
			"En Fonctionnement",
			"En Investissement",
			"Somme de tous les financements validés",
			"Somme Acteur socio-économique",
			"Somme Communes - intercommunalité-syndicat",
			"Somme Département",
			"Somme Region",
			"Somme Europe",
			//"Somme Autre",
			"Somme Etat",
			"Somme Etat - Services déconcentrés / préfecture",
			"Somme Etat - ADEME",
			"Somme Etat - CEREMA",
			"Somme Etat - Banque des territoires",
			"Somme Etat - BPI",
			"Somme Etat - Agence / office de l’eau",
			"Somme Etat - Office français de la biodiversité",
			"Somme Etat - Office national des forêts",
			"Somme Etat - Agence française de développement",
			"Somme Etat - Voies navigables de France",
			"Somme Etat - FranceAgriMer",
			"Somme Etat - Autre"
		);

		//Rest::json($newList); exit;
		Rest::csv($newList, false, false, null, $order);

		Yii::app()->end();

	}
}