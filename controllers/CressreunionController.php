<?php
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CressreunionController extends CommunecterController {


    protected function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'importsiret'  		=> 'costum.controllers.actions.cressReunion.admin.ImportsiretAction',
	        'importorga'  		=> 'costum.controllers.actions.cressReunion.admin.ImportorgaAction',
	        'savesiren'  		=> 'costum.controllers.actions.cressReunion.admin.SaveSirenAction',
	        'saveorga'  		=> 'costum.controllers.actions.cressReunion.admin.SaveOrgaAction',
	    );
	}
}
