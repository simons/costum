<?php

class Meusecampagnes {
    public static function saveOrga($params){
        $params = Import::newStart($params);
        $res = Import::previewData($params, true, true, true);
        $resultImport = array();
        if(!empty($res) && !empty($res["elementsObj"])){
           foreach ($res["elementsObj"] as $key => $value) {
                if(!empty($value["name"])){

                    $where = array( "name" => $value["name"],
                                    "address.localityId" => @$value["address"]["localityId"],
                                    "source.key" => "meuseCampagnes"
                                );
                    $org = PHDB::findOne(Organization::COLLECTION, $where);
                    if(empty($org)){
                        PHDB::insert(Organization::COLLECTION, $value );
                        $resElt = array(
                            "update" => false,
                            "id" => false,
                            "name" => @$value["name"]
                        );
                        $resultImport[] = $resElt; 
                    }
                    
                }   
            } 
        }
        $res["resultImport"] = $resultImport;
        
        Rest::json($res); exit;
    }
    public static function saveEvent($params){
        $params = Import::newStart($params);
        $res = Import::previewData($params, true, true, true);
        $resultImport = array();
        if(!empty($res) && !empty($res["elementsObj"])){
           foreach ($res["elementsObj"] as $key => $value) {
                if(!empty($value["name"])){

                    $where = array( "name" => $value["name"],
                                    "address.localityId" => @$value["address"]["localityId"],
                                    "source.key" => "meuseCampagnes"
                                );
                    $event = PHDB::findOne(Event::COLLECTION, $where);
                    if(!empty($org)){
                        PHDB::insert(Event::COLLECTION, $value );
                        $resElt = array(
                            "update" => false,
                            "id" => false,
                            "name" => @$value["name"]
                        );
                        $resultImport[] = $resElt;
                    }
                    
                }   
            } 
        }
        $res["resultImport"] = $resultImport;
        
        Rest::json($res); exit;
    }
}
?>