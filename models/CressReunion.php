<?php

class CressReunion {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	
    public static $dataBinding_allOrganization = array(
        "id"     => array("valueOf" => "id"),
        "siren"     => array("valueOf" => "siren"),
        "Nom de l'organisme"      => array("valueOf" => "name"),
        "Type : ( NGO, Group, LocalBusiness ou GovernmentOrganization )"       => array("valueOf" => "type"),

        "Rue"   => array("parentKey"=>"address", 
                             "valueOf" => array(
                                    "streetAddress"     => array("valueOf" => "streetAddress")
                                    )),
        "Code Postal"   => array("parentKey"=>"address", 
                             "valueOf" => array(
                                    "streetAddress"     => array("valueOf" => "postalCode")
                                    )),
        "Ville"   => array("parentKey"=>"address", 
                             "valueOf" => array(
                                    "streetAddress"     => array("valueOf" => "addressLocality")
                                    )),
        "Code Pays"   => array("parentKey"=>"address", 
                             "valueOf" => array(
                                    "streetAddress"     => array("valueOf" => "addressCountry")
                                    )),
        "Email"     => array("valueOf" => "email"),
        "Site web"       => array(   "valueOf" => 'slug',
                        "type"  => "url", 
                        "prefix"   => "/#@",
                        "suffix"   => ""),
        "Description courte"      => array("valueOf" => "shortDescription"),
        "Description longue"       => array("valueOf" => "description"),
        "tags"      => array("valueOf" => "tags"),
    );

    public static function saveSiren($params){
        //var_dump("HelloThere"); exit;

        $params = Import::newStart($params);
        $res = Import::previewData($params, true, true, true);
        $resultImport = array();
        //Rest::json($res); exit;
        if(!empty($res) && !empty($res["elementsObj"])){
           foreach ($res["elementsObj"] as $key => $value) {
                if(!empty($value["name"]) && !empty($value["siren"])){

                    $where = array( "name" => $value["name"],
                                    "source.key" => "cressReunion"
                                    ,"siren" => array('$exists' => 0) 
                                );

                    if(!empty($value["address"])){
                        if(!empty($value["address"]["streetAddress"]))
                            $where["address.streetAddress"] = $value["address"]["streetAddress"];

                        // if(!empty($value["address"]["postalCode"]))
                        //     $where["address.postalCode"] = $value["address"]["postalCode"];
                    }
                    //Rest::json($where); exit;
                    $org = PHDB::findOne(Organization::COLLECTION, $where);
                    if(!empty($org)){
                        $org["siren"] = $value["siren"];
                        $set["siren"] = $value["siren"];

                        if( !empty($value["sigle"]) ){
                            $set["siren"] = $value["siren"];
                            $set["sigle"] = $value["sigle"];
                        }

                        PHDB::update(Organization::COLLECTION,
                            array("_id"=>new MongoId($org["_id"]) ) , 
                            array('$set' => $set)
                        );
                        $resultImport[] = $org; 
                    }
                    
                }   
            } 
        }
        //Rest::json($resultImport); exit;
        $res["resultImport"] = $resultImport;
        
        Rest::json($res); exit;
    }

    public static function saveOrga($params){
        //var_dump("HelloThere"); exit;
        //var_dump("HERE"); exit;
        $params = Import::newStart($params);
        $res = Import::previewData($params, true, true, true);
        $resultImport = array();
        //Rest::json($res); exit;
        if(!empty($res) && !empty($res["elementsObj"])){
           foreach ($res["elementsObj"] as $key => $value) {
                if(!empty($value["name"]) && !empty($value["siren"])){

                    $where = array( "id" => $value["id"],
                                    "source.key" => "cressReunion"
                                );

                    // if(!empty($value["address"])){
                    //     if(!empty($value["address"]["streetAddress"]))
                    //         $where["address.streetAddress"] = $value["address"]["streetAddress"];

                    //     // if(!empty($value["address"]["postalCode"]))
                    //     //     $where["address.postalCode"] = $value["address"]["postalCode"];
                    // }
                    //Rest::json($where); exit;
                    $org = PHDB::findOne(Organization::COLLECTION, $where);
                    if(!empty($org)){
                        // Code a modifier pour prendre en compte les colonne de la cress 
                        // $org["siren"] = $value["siren"];
                        // $set["siren"] = $value["siren"];

                        // if( !empty($value["sigle"]) ){
                        //     $set["siren"] = $value["siren"];
                        //     $set["sigle"] = $value["sigle"];
                        // }

                        PHDB::update(Organization::COLLECTION,
                            array("_id"=>new MongoId($org["_id"]) ) , 
                            array('$set' => $set)
                        );
                        $resultImport[] = $org; 
                    }
                    
                }   
            } 
        }
        //Rest::json($resultImport); exit;
        $res["resultImport"] = $resultImport;
        
        Rest::json($res); exit;
    }
}
?>