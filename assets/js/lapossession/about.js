function bindDynFormEditable(){ 

		$(".btn-update-descriptions").off().on( "click", function(){

			var form = {
				saveUrl : baseUrl+"/costum/lapossession/updateblock/",
				dynForm : {
					jsonSchema : {
						title : trad["Update description"],
						icon : "fa-key",
						onLoads : {
							
							markdown : function(){
								dataHelper.activateMarkdown("#ajaxFormModal #description");
								$("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
											  					  .addClass("bg-dark");
							}
						},
						afterSave : function(data){
							mylog.dir(data);
							if(data.result&& data.resultGoods && data.resultGoods.result){
								if(data.resultGoods.values.shortDescription=="")
									$(".contentInformation #shortDescriptionAbout").html('<i>'+trad["notSpecified"]+'</i>');
								else
									$(".contentInformation #shortDescriptionAbout").html(data.resultGoods.values.shortDescription);
								$(".contentInformation #shortDescriptionAboutEdit").html(data.resultGoods.values.shortDescription);
								$("#shortDescriptionHeader").html(data.resultGoods.values.shortDescription);
								if(data.resultGoods.values.description=="")
									$(".contentInformation #descriptionAbout").html(dataHelper.markdownToHtml('<i>'+trad["notSpecified"]+'</i>'));
								else
									$(".contentInformation #descriptionAbout").html(dataHelper.markdownToHtml(data.resultGoods.values.description));
								$("#descriptionMarkdown").html(data.resultGoods.values.description);

								if(data.resultGoods.values.category=="")
									$(".contentInformation #categoryAbout").html('<i>'+trad["notSpecified"]+'</i>');
								else
									$(".contentInformation #categoryAbout").html(data.resultGoods.values.category);
								$(".contentInformation #categoryAboutEdit").html(data.resultGoods.values.category);
							
							}
							dyFObj.closeForm();
							changeHiddenFields();
						},
						properties : {
							block : dyFInputs.inputHidden(),
							typeElement : dyFInputs.inputHidden(),
							descMentions : dyFInputs.inputHidden(),
							isUpdate : dyFInputs.inputHidden(true),
							shortDescription : 	dyFInputs.textarea(tradDynForm["shortDescription"], "...",{ maxlength: 140 }),
							description : dyFInputs.textarea(tradDynForm["longDescription"], "..."),
							category : 	dyFInputs.inputSelect("Catégorie", "...", {
                                    "citoyennete" : "citoyennete",
                                    "energie" : "energie",
                                    "construction" : "construction",
                                    "communs" : "communs",
                                    "dechets" : "dechets",
                                    "education" : "education",
                                    "economie" : "economie",
                                    "alimentation/sante" : "alimentation/sante"
                                }),
						}
					}
				}
			};

			var dataUpdate = {
				block : "descriptions",
		        id : contextData.id,
		        typeElement : contextData.type,
		        name : contextData.name,
		        shortDescription : $(".contentInformation #shortDescriptionAboutEdit").html(),
				description : $("#descriptionMarkdown").html(),
				category : $(".contentInformation #categoryAboutEdit").html(),
			};

			dyFObj.openForm(form, "markdown", dataUpdate);
		});
	}