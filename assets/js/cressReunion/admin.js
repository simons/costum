adminPanel.views.importsiret = function(){
	ajaxPost('#content-view-admin', baseUrl+'/costum/cressreunion/importsiret/', {}, function(){},"html");
};

adminPanel.views.organizations = function(){
	var data={
		title : "Les organisations",
		types : [ "organizations" ],
		table : {
            name: {
                name : "Nom"
            },
            tags : { name : "Mots clés"}
        },
        actions : {
        	//banUser : true,
        	delete : true
        },
        csv : {
            organization : {
                key : "cressReunion"
            }
        }
	};
	ajaxPost('#content-view-admin', baseUrl+'/costum/default/groupadmin/', data, function(){},"html");
};

adminPanel.views.importorga = function(){
	ajaxPost('#content-view-admin', baseUrl+'/costum/cressreunion/importorga/', {}, function(){},"html");
};