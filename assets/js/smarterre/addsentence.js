
function addSentence(){

    console.log("----------- ADD SENTENCE");
    
    var params = {
        "name" : $("#sentenceField").val(),
        "docType" : "smarterre."+window.location.href.split("#")[1],
        "targetId" : userId,
        "targetType" : "citoyens"
    };
    console.log("params sentence ",params);

    $.ajax({
        type : "POST",
        data : params,
        dataType : "json",
        url : baseUrl + "/co2/folder/crud/action/new",
        async : false,
        success: function(data){
            console.log("success", data);
            toastr.success("Votre phrase a bien été ajouté");
            $("#exampleModalCenter").modal('toggle');
        },
        error : function(e){
            console.log("ERROR ",e);
        }
    });
}
