function stepOneLink(id,mid){

    var params = {
        "parentId" : id,
        "thematique" : window.location.href.split("#")[1]
    }
	console.log("stepOne ",params);

	$.ajax({
		type: 'POST',
		data: params,
		dataType: 'json',
		url : baseUrl+'/costum/smarterre/searchurl/',
		async: false,
		success : function(data){
			console.log("success");
			console.log(data);
			if(data.result == true){
				stepTwoLink(data,mid);
			}
		},
		error: function(e){
			console.log("ERROR :",e);
		}
	});
}

//Permet d'afficher les résultats
function stepTwoLink(data,mid){
	console.log("stepTwo :"+ mid);
	var d = "<ul>";
	$.each(data.urls,function(key,value){
		console.log("name :" + value.name + " url : " + value.url);
		d += "<li><a href='"+value.url+"' target='_blank'>"+value.name+"</a></li>";
	});

	d += "</ul>";
	$(".affiche"+mid).html(d);

	console.log(d);
}
