<?php 
//pourrait aller dans 
$cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
  // SHOWDOWN
  '/plugins/showdown/showdown.min.js',
  // MARKDOWN
  '/plugins/to-markdown/to-markdown.js'
);
$poiList = array();
if(isset(Yii::app()->session["costum"]["contextType"]) && isset(Yii::app()->session["costum"]["contextId"])){
    $el = Element::getByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"] );

    $poiList = PHDB::find(Poi::COLLECTION, 
                    array( "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                           "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                           "type"=>"cms") );
} else {?>
  
  <div class="col-xs-12 text-center margin-top-50">
    <h2>Ce COstum est un template <br/>doit etre associé à un élément pour avoir un context.</h2>
  </div>

<?php exit;}?>