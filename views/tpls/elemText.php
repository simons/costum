<?php 

if(!isset($noRender)){
	echo '<h4 style="text-align:left; color:'.@$color.'">'.( (isset($title)) ? $title : @$field).'</h4>';
	echo "<div class='text-left markdown' id='elemText".$field."'>".@$value."</div>";
}
echo $this->renderPartial("costum.views.tpls.elemOpenFormBtn",
                                array(
                                    'collection' => $collection,
                                    'id' => $id,
                                    'field' => $field,
                                    'dynFormProperty'=>(is_array($dynFormProperty))? "" : $dynFormProperty ,
                                 ),true);

?>


<script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render","/modules/costum/views/tpls/elemText.php",'field : <?php echo $field ?>');
    
    var dynFormProperty = <?php echo (!$dynFormProperty) ? "null" : json_encode($dynFormProperty) ?>;
    mylog.log("editProperty","elemText ",dynFormProperty);
    var thisField = "<?php echo $field ?>";

    if(dyFObj.formProperties == null)
        dyFObj.formProperties = {};

    //if no dynFormProperty means we are using a predefined property
    if(dynFormProperty != null){
        if(dyFObj.dynFormSource != null){
        	mylog.log("editProperty","add dyFObj.formProperties.",thisField,<?php echo json_encode($dynFormProperty) ?>);
        	dyFObj.formProperties[thisField] = dynFormProperty;
        }
    } else  {
        mylog.log("editProperty","dyFInputs.defaultProperties",dyFInputs.defaultProperties);
        dyFObj.formProperties[thisField] = dyFInputs.defaultProperties[thisField];
    }
    
});
</script>