<style type="text/css">
	.blockFontPreview{
		font-size: 14px;
		color: #777;
	}
</style>
<?php 
	// $typeItemHead = $type;
	// if($type == Organization::COLLECTION && @$element["type"]) $typeItemHead = $element["type"];
	// $icon = Element::getFaIcon($typeItemHead) ? Element::getFaIcon($typeItemHead) : "";
	// $iconColor = Element::getColorIcon($typeItemHead) ? Element::getColorIcon($typeItemHead) : "";
?> 
<div id="preview-elt-<?php echo $type ?>-<?php echo (string)$element["_id"] ?>">
<?php 
if( isset(Yii::app()->session['costum']["htmlConstruct"]) 
	&& isset(Yii::app()->session['costum']["htmlConstruct"]["preview"])
	&& isset(Yii::app()->session['costum']["htmlConstruct"]["preview"]["element"])){
	 $params = array("element"=>$element , 
                                    "id" => @$id,
                                    "type" => @$type,
                                    "page" => "page",
                                    "canEdit"=>$canEdit,
                                    "openEdition" => $openEdition,
                                    "linksBtn" => $linksBtn,
                                    "isLinked" => $isLinked,
                                    "controller" => $controller,
                                    "countStrongLinks" => $countStrongLinks,
                                    "countInvitations" => $countInvitations,
                                    "countries" => $countries,
                                    "typeItem" => $typeItem,
                                    "typeItemHead" => $typeItemHead,
                                    "categoryItem" => $categoryItem,
                                    "icon" => $icon,
                                    "iconColor" => $iconColor,
                                    "useBorderElement" => $useBorderElement,
                                    "pageConfig" => $pageConfig,
                                    "addConfig" => $addConfig  );
        $this->renderPartial( Yii::app()->session["costum"]["htmlConstruct"]["preview"]["element"], $params ); 
}else{ ?>
	<div class="col-xs-12 padding-10">
	<button class="btn btn-default pull-right btn-close-preview">
		<i class="fa fa-times"></i>
	</button>
	<a href="#@<?php echo $element["slug"] ?>" class="lbh btn btn-primary pull-right margin-right-10"><?php echo Yii::t("common", "Go to the item") ?></a>
	</div>

<?php 
	if (!@$element["profilBannereUrl"] || (@$element["profilBannereUrl"] && empty($element["profilBannereUrl"]))){ 
		if(!empty(Yii::app()->session["costum"]) && isset(Yii::app()->session["costum"]["htmlConstruct"]) 
			&& isset(Yii::app()->session["costum"]["htmlConstruct"]["element"])
				&& isset(Yii::app()->session["costum"]["htmlConstruct"]["element"]["banner"]["img"]))
			$url=Yii::app()->getModule( "costum" )->assetsUrl.Yii::app()->session["costum"]["htmlConstruct"]["element"]["banner"]["img"];
		else
			$url=Yii::app()->theme->baseUrl.'/assets/img/background-onepage/connexion-lines.jpg';
	}else
		$url=Yii::app()->createUrl('/'.$element["profilBannerUrl"]); 
	?> 
    	
	<div class="col-xs-12 no-padding" style="border-top: 1px solid #e7e7e7;border-bottom: 1px solid #e7e7e7;">
		<?php 
			$imgHtml='<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" 
				alt="'.Yii::t("common","Banner").'" 
				src="'.$url.'">';
			if (@$element["profilRealBannerUrl"] && !empty($element["profilRealBannerUrl"])){
				$imgHtml='<a href="'.Yii::app()->createUrl('/'.$element["profilRealBannerUrl"]).'"
							class="thumb-info"  
							data-title="'.Yii::t("common","Cover image of")." ".$element["name"].'"
							data-lightbox="all">'.
							$imgHtml.
						'</a>';
			}
			echo $imgHtml;
		?>		
	</div>
	<div class="content-img-profil-preview col-xs-8 col-xs-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
		<?php if(isset($element["profilMediumImageUrl"])){ ?> 
		<a href="<?php echo Yii::app()->createUrl('/'.$element["profilImageUrl"]) ?>"
		class="thumb-info"  
		data-title="<?php echo Yii::t("common","Profil image of")." ".$element["name"] ?>"
		data-lightbox="all">
			<img class="img-responsive" onload='eltImgPreview(this)' style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo Yii::app()->createUrl('/'.$element["profilMediumImageUrl"]) ?>" />
		</a>
		<?php }else{ ?>
			<img class="img-responsive shadow2 thumbnail" onload='eltImgPreview(this)' style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo $this->module->assetsUrl ?>/images/thumbnail-default.jpg"/>
		<?php } ?>
	</div>
	<div class="preview-element-info col-xs-12">
		<h3 class="text-center"><?php echo $element["name"] ?></h3>
		<span class="col-xs-12 text-center blockFontPreview"> 
			<span class="text-<?php echo $iconColor; ?>"><?php echo strtoupper(Yii::t("common", Element::getControlerByCollection($type))); ?></span>
			<?php if(($type==Organization::COLLECTION || $type==Event::COLLECTION) && @$element["type"]){ 
				if($type==Organization::COLLECTION)
					$typesList=Organization::$types;
				else
					$typesList=Event::$types;
				?>
					<i class="fa fa-x fa-angle-right margin-left-10"></i>
					<span class="margin-left-10"><?php echo Yii::t("category", $typesList[$element["type"]]) ?></span>
			<?php } ?>
			
		</span>
		
		<?php 
			if(!empty($element["address"]["addressLocality"])){ ?>
				<div class="header-address col-xs-12 text-center blockFontPreview">
					<i class="fa fa-map-marker"></i> 
					<?php
						echo !empty($element["address"]["streetAddress"]) ? "<i class='fa fa-map-marker'></i> ".$element["address"]["streetAddress"].", " : "";
						echo !empty($element["address"]["postalCode"]) ? 
								$element["address"]["postalCode"].", " : "";
						echo $element["address"]["addressLocality"];
					?>
				</div>
			<?php } ?>
			<div class="header-tags col-xs-12 text-center blockFontPreview">
				<?php 
				if(@$element["tags"]){ 
					foreach ($element["tags"] as $key => $tag) { ?>
						<a  href="javascript:;"  class="letter-red" style="vertical-align: top;">#<?php echo $tag; ?></a>
					<?php } 
				} ?>
				</div>
			</div>
		<?php if($type==Event::COLLECTION){ ?>
			<div class="event-infos-header text-center margin-top-10 col-xs-12 blockFontPreview"  style="font-size: 14px;font-weight: none;"></div>
		<?php } ?>
		
		<div class="col-xs-12 col-sm-9 col-md-9 col-lg-10 pull-right">
			<span class="pull-left text-white" id="shortDescriptionHeader"><?php echo ucfirst(substr(trim(@$element["shortDescription"]), 0, 180)); ?>
			</span>	
		</div>
	</div>
	<?php } ?>
</div>

<script type="text/javascript">

	var eltPreview=<?php echo json_encode($element); ?>;

	jQuery(document).ready(function() {	
		var str = directory.getDateFormated(eltPreview);
		$(".event-infos-header").html(str);

		coInterface.bindLBHLinks();
	});
	function eltImgPreview($this){
		hImg=$($this).outerHeight();
		$(".content-img-profil-preview").css({"margin-top": -(hImg/2)+"px"});
	}
</script>