<?php 
$cssAnsScriptFilesTheme = array(
       '/plugins/jsonview/jquery.jsonview.js',
		'/plugins/jsonview/jquery.jsonview.css',
		'/plugins/jQuery/jquery-2.1.1.min.js',
		'/plugins/jQuery/jquery-1.11.1.min.js',
		'/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
		'/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js'             
    );
?>
<style type="text/css">
@font-face {
    font-family : "SharpSansMedium", sans-serif;
    src : url("<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/font/coeurNumerique/SharpSansNo1-Medium.ttf") format("ttf");
}
@font-face {
    font-family : "SharpSansBold", sans-serif;
    src : url("<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/font/coeurNumerique/SharpSansNo1-Semibold.ttf") format("ttf");
}
#header{
    margin-top: -38%;
    z-index: 1;
}
.himage{
    margin-top : 15%;
}
#maps{
    margin-top : -42%;
    z-index : 1;
}
.description-carto{
    text-align : right;
    color : white;
    font-size : 2.5rem;
    margin-right: 10%;
}
.mimage{
    margin-top : -10%;
}
.actuimage{
    margin-top : -18%;
}
.actucard {
    margin-top : -10%;
}
.hfiliere{
    margin-top : 3%;
}
.bloc3{
    margin-top : 4%;
}
#agenda{
    background-image : url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/bloc/bloc4.png);
    background-size:cover;
    background-repeat: no-repeat;
    height : 100%;
}
#stastique{
    background-image : url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/bloc/bloc5.png);
    background-size : cover;
    background-repeat: no-repeat;
    height : 100%;
}
.himage{
    text-align: center;
}
.levent{
    color : white;
}
#caroussel{
    background-color: rgba(0,0,0,0.5);
    opacity: 1;
    width: 30%;
    z-index:1;
}
.resume {
    text-align: left;
    z-index: 10;
    position : absolute;
    margin-top : 3%;
}
.resume > h2{
    font-size : 2vw;
    color : white;
}
.resume > p{
   font-size : 1.3vw;
   color : white;
}
#arrow-caroussel{
    margin-top : 100%;
    font-size:3vw;
    color: white;
}
#arrow-caroussel > a {
    color : white;
    text-decoration : none;
    margin: 3%;
}
.carousel-border{
  	box-shadow: 0px 0px 16px 3px black;
}
.card{
/* border-width: 1px;
border-style : solid; */
background: #34393a;
margin-left: 3%;
margin-right: 5%;
color: white;
font-size : 1.5vw;
}
.card-info{
    background : #c7d407;
    padding : 10px;
    font-size : 1.5rem;
}
.card-c{
    right: 3.71%;
    margin-top: -35%;
}
.ctr-img{
    width : 100%; 
    height:auto;
}
.carousel-inner{
    position: relative;
    width: 100%;
    overflow: hidden;
    height: 550px;
}
.ctr-img {
    z-index : 1;
}
.logofn{
    width: 85%;
    margin-left: 10%;
}
.pheader{
    text-align : center; color : white; font-size: 1.7vw; margin-top: 11%;
}

@media (max-width: 768px){
    .logofn{
        width:110%;
    }
    .pheader{
        font-size: 2.4vw;
        margin-left:15%;
    }
    .himage > img {
        width: 130%;
    }
    .btn-choose{
        width:200%;
    }
    .description-carto{
        font-size : 2.5vw;
        margin-top: 20%;
    }
}
</style>

<!-- Image de fond --> 
<div class="content-img bloc1">
    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/bloc/bloc1.png" class="img-responsive">
</div>

<div id="header" class="row">
    <!-- Description de la filière numérique -->
    <div class="col-xs-6 col-sm-6 col-md-6 hfiliere">
    <div class="logofn">
    <center>
    <!--Début svg logo --> 
    <?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 23.0.4, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 320 595.28 200.89" style="enable-background:new 0 0 595.28 841.89;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#FFFFFF;}
	.st1{font-family:'SharpSansMedium';}
	.st2{font-size:43.3313px;}
	.st3{font-family:'SharpSansBold';}
	.st4{fill:#36B4A0;}
	.st5{font-family:'SharpSansMedium';}
	.st6{font-size:26.4145px;}
	.st7{fill:#6BBFA3;}
	.st8{fill:#AFCA0B;}
	.st9{fill:#00ACA9;}
	.st10{fill:#95C11F;}
</style>
<g>
	<g>
		<g>
			<text transform="matrix(1 0 0 1 271.2626 403.6614)"><tspan x="0" y="0" class="st0 st1 st2">LA</tspan><tspan x="0" y="44.99" class="st0 st3 st2">RÉUNION</tspan></text>
			<g>
				<text transform="matrix(1 0 0 1 360.971 480.1658)" class="st4 st5 st6">100% NUMÉRIQUE</text>
			</g>
		</g>
	</g>
	<g>
		<g>
			<polygon class="st7" points="52.11,420.94 104.81,512.22 210.21,512.22 137.1,420.94 			"/>
			<polygon class="st8" points="210.21,329.67 210.21,512.22 262.91,420.94 			"/>
			<polygon class="st9" points="210.21,329.67 104.81,329.67 52.11,420.94 140.14,420.94 			"/>
			<polygon class="st10" points="137.1,420.94 210.21,512.22 210.21,329.67 			"/>
		</g>
		<g>
			<path id="share_1_" class="st0" d="M195.31,406.78c-4.78,0-8.99-2.28-11.74-5.77c-9.83,4.92-19.68,9.83-29.52,14.76
				c0.34,1.24,0.57,2.52,0.57,3.86c0,1.35-0.23,2.63-0.57,3.87c9.83,4.92,19.67,9.83,29.51,14.75c2.75-3.48,6.96-5.76,11.74-5.76
				c8.28,0,14.99,6.71,14.99,14.98c0,8.28-6.71,15-14.99,15c-8.27,0-14.98-6.72-14.98-15c0-1.34,0.23-2.62,0.57-3.86
				c-9.83-4.93-19.67-9.83-29.52-14.76c-2.75,3.49-6.96,5.78-11.74,5.78c-8.27,0-14.99-6.7-14.99-14.99
				c0-8.27,6.72-14.98,14.99-14.98c4.78,0,8.99,2.28,11.74,5.77c9.83-4.91,19.67-9.83,29.52-14.74c-0.34-1.25-0.57-2.53-0.57-3.88
				c0-8.27,6.71-14.99,14.98-14.99c8.28,0,14.99,6.72,14.99,14.99C210.3,400.06,203.59,406.78,195.31,406.78z"/>
			<path id="share_3_" class="st0" d="M128.53,459.39c1.74-2.95,4.69-4.71,7.85-5.13c0.55-7.86,1.12-15.72,1.68-23.59
				c-0.89-0.24-1.77-0.57-2.59-1.06c-0.84-0.49-1.54-1.1-2.18-1.76c-6.62,4.27-13.25,8.54-19.87,12.81c1.15,2.97,1.01,6.4-0.73,9.34
				c-3.02,5.11-9.61,6.79-14.71,3.78c-5.11-3.02-6.8-9.62-3.78-14.72c3.02-5.1,9.61-6.78,14.72-3.76c0.83,0.49,1.53,1.1,2.17,1.76
				c6.63-4.27,13.25-8.54,19.88-12.81c-1.15-2.97-1.02-6.4,0.72-9.35c3.02-5.1,9.61-6.8,14.72-3.77c5.1,3.02,6.79,9.61,3.77,14.72
				c-1.75,2.95-4.69,4.71-7.84,5.13c-0.56,7.86-1.12,15.72-1.69,23.59c0.89,0.25,1.77,0.57,2.6,1.07c5.1,3.02,6.8,9.61,3.78,14.71
				c-3.02,5.11-9.62,6.79-14.72,3.77C127.2,471.09,125.51,464.5,128.53,459.39z"/>
			<path id="share_2_" class="st0" d="M117.72,396.82c1.29,2.5,1.22,5.33,0.13,7.71c5.23,3.83,10.45,7.67,15.69,11.5
				c0.56-0.51,1.17-0.98,1.87-1.34c0.71-0.36,1.44-0.59,2.18-0.75c-0.07-6.48-0.14-12.96-0.21-19.44c-2.57-0.5-4.9-2.1-6.18-4.6
				c-2.23-4.34-0.52-9.67,3.82-11.89c4.34-2.23,9.67-0.52,11.9,3.82c2.23,4.34,0.51,9.66-3.83,11.89c-0.7,0.36-1.44,0.59-2.18,0.74
				c0.07,6.48,0.14,12.96,0.21,19.45c2.57,0.5,4.9,2.09,6.19,4.6c2.23,4.34,0.52,9.66-3.82,11.9c-4.34,2.23-9.66,0.51-11.89-3.82
				c-1.29-2.51-1.22-5.33-0.14-7.71c-5.22-3.83-10.45-7.67-15.68-11.51c-0.57,0.51-1.17,0.98-1.88,1.34
				c-4.34,2.23-9.67,0.52-11.89-3.82c-2.23-4.34-0.51-9.67,3.82-11.9C110.16,390.77,115.49,392.48,117.72,396.82z"/>
		</g>
	</g>
</g>
</svg>

    <!-- Fin svg -->
    </center>
    </div>
        <!-- <center><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/logofn.svg" class="img-responsive" width="90%" style="margin-left: 10%;"></center> -->
       <!-- Importer le svg -->
        <p class="pheader">Restons connectés pour faire avancer notre filière numérique</p>
    </div>
    <!-- Image + btn -->
    <div class="col-xs-6 col-sm-6 col-md-6 himage">
        <img  src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/home/schemanum.png" class="img-responsive"><br>
        <span class="col-xs-12 col-sm-12 col-md-12 btn-choose">

        <!-- <a onclick="dyFObj.openForm('organization')" href="#"> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/barre/jesouhaite.svg" class="img-responsive"> </a> -->
        
        <!-- Debut svg --> 
        <?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 23.0.4, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 300 595.28 70.89" style="enable-background:new 0 0 595.28 841.89;" xml:space="preserve">
<style type="text/css">
	.st12{fill:url(#SVGID_1_); z-index: 1;}
    .txt0{z-index : 10; fill : white; font-size : 0.9vw; font-family: "customFont" !important;}
</style>
<g>
	<g>
		<g>
			<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="182.7465" y1="330.3033" x2="406.2793" y2="330.3033">
				<stop  offset="0" style="stop-color:#6BBFA2"/>
				<stop  offset="0.3451" style="stop-color:#67BDA1"/>
				<stop  offset="0.6027" style="stop-color:#5CB59C"/>
				<stop  offset="0.8311" style="stop-color:#48A995"/>
				<stop  offset="1" style="stop-color:#309D8C"/>
			</linearGradient>
			<path class="st12" d="M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25c0.01-0.01,0.03-0.02,0.04-0.03
				C206.03,329.7,206,329.63,205.98,329.55z M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25c0.01-0.01,0.03-0.02,0.04-0.03
				C206.03,329.7,206,329.63,205.98,329.55z M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25c0.01-0.01,0.03-0.02,0.04-0.03
				C206.03,329.7,206,329.63,205.98,329.55z M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25c0.01-0.01,0.03-0.02,0.04-0.03
				C206.03,329.7,206,329.63,205.98,329.55z M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25c0.01-0.01,0.03-0.02,0.04-0.03
				C206.03,329.7,206,329.63,205.98,329.55z M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25c0.01-0.01,0.03-0.02,0.04-0.03
				C206.03,329.7,206,329.63,205.98,329.55z M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25c0.01-0.01,0.03-0.02,0.04-0.03
				C206.03,329.7,206,329.63,205.98,329.55z M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25c0.01-0.01,0.03-0.02,0.04-0.03
				C206.03,329.7,206,329.63,205.98,329.55z M394.48,310.5H194.55l-11.8,19.81l11.8,19.8h199.93l11.8-19.8L394.48,310.5z
				 M212.8,328.5c0,0.37-0.07,0.73-0.16,1.06c2.8,1.35,5.6,2.69,8.4,4.04c0.78-0.95,1.98-1.58,3.35-1.58c2.36,0,4.27,1.84,4.27,4.1
				c0,2.27-1.91,4.11-4.27,4.11c-2.35,0-4.26-1.84-4.26-4.11c0-0.36,0.07-0.72,0.16-1.05c-2.81-1.35-5.6-2.69-8.41-4.04
				c-0.64,0.78-1.58,1.35-2.65,1.52c-0.14,1.85-0.26,3.68-0.41,5.53c0.26,0.07,0.5,0.16,0.74,0.29c1.46,0.83,1.94,2.64,1.08,4.04
				c-0.86,1.4-2.74,1.85-4.2,1.03c-1.46-0.83-1.94-2.63-1.08-4.03c0.5-0.81,1.34-1.29,2.24-1.41c0.13-1.81,0.27-3.61,0.4-5.42
				c-0.94-0.11-1.78-0.51-2.42-1.11c-1.51,0.94-3.01,1.87-4.52,2.8c0.32,0.82,0.28,1.75-0.21,2.56c-0.86,1.4-2.74,1.86-4.2,1.04
				c-1.45-0.83-1.94-2.64-1.07-4.04c0.86-1.4,2.73-1.85,4.19-1.03c0.24,0.13,0.43,0.3,0.62,0.48c1.47-0.92,2.95-1.84,4.42-2.74
				c-0.36-0.6-0.56-1.29-0.56-2.04c0-0.49,0.09-0.95,0.26-1.38c-0.93-0.65-1.86-1.32-2.79-1.97c-0.16,0.15-0.33,0.27-0.54,0.37
				c-1.23,0.61-2.75,0.14-3.38-1.05c-0.63-1.19-0.15-2.65,1.09-3.26c1.24-0.61,2.75-0.15,3.39,1.05c0.36,0.69,0.35,1.46,0.04,2.12
				c0.88,0.62,1.77,1.25,2.65,1.86c0.65-0.95,1.71-1.63,2.94-1.79c-0.01-0.94-0.02-1.88-0.03-2.83c-0.73-0.14-1.4-0.57-1.76-1.25
				c-0.64-1.19-0.15-2.65,1.09-3.26c1.24-0.61,2.75-0.14,3.38,1.05c0.63,1.19,0.15,2.65-1.09,3.26c-0.2,0.1-0.41,0.16-0.62,0.2
				c0.01,0.94,0.02,1.87,0.04,2.81c1.2,0.11,2.24,0.7,2.95,1.56c2.8-1.35,5.6-2.7,8.41-4.04c-0.09-0.34-0.16-0.69-0.16-1.05
				c0-2.27,1.91-4.11,4.26-4.11c2.36,0,4.27,1.84,4.27,4.11c0,2.26-1.91,4.1-4.27,4.1c-1.36,0-2.56-0.62-3.35-1.58
				c-2.8,1.35-5.6,2.7-8.4,4.04C212.73,327.78,212.8,328.14,212.8,328.5z M206.02,329.8c0.01-0.01,0.03-0.02,0.04-0.03
				c-0.03-0.07-0.06-0.15-0.08-0.22C205.98,329.64,205.99,329.72,206.02,329.8z M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25
				c0.01-0.01,0.03-0.02,0.04-0.03C206.03,329.7,206,329.63,205.98,329.55z M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25
				c0.01-0.01,0.03-0.02,0.04-0.03C206.03,329.7,206,329.63,205.98,329.55z M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25
				c0.01-0.01,0.03-0.02,0.04-0.03C206.03,329.7,206,329.63,205.98,329.55z M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25
				c0.01-0.01,0.03-0.02,0.04-0.03C206.03,329.7,206,329.63,205.98,329.55z M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25
				c0.01-0.01,0.03-0.02,0.04-0.03C206.03,329.7,206,329.63,205.98,329.55z M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25
				c0.01-0.01,0.03-0.02,0.04-0.03C206.03,329.7,206,329.63,205.98,329.55z M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25
				c0.01-0.01,0.03-0.02,0.04-0.03C206.03,329.7,206,329.63,205.98,329.55z M205.98,329.55c0,0.08,0.01,0.16,0.04,0.25
				c0.01-0.01,0.03-0.02,0.04-0.03C206.03,329.7,206,329.63,205.98,329.55z"/>
		
        <g>
        <?php if(@Yii::app()->session["userId"]) { ?>
                <text y="335" class="txt0" x="230" style="font-size: 0.85vw;"><a onclick="dyFObj.openForm('organization'); " style="text-decoration : none" href="#">Je souhaites ajouter un projet</a></text>
        <?php } else { ?>
            <text x="230" y="337" class="txt0" style="font-size: 1.3vw;"><a href="javascript:;" data-toggle="modal" data-target="#modalRegister" style="text-decoration : none;"> Je rejoins la filière </a></text>
        <?php } ?>
            </g>

        </g>
        
	</g>
</g>
</svg>
    </div>


    </div>
<!-- Image de fond --> 
<div class="content-img bloc2">
    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/bloc/bloc2.png" class="img-responsive"  style="width: 70%;">
    


</div>

<div id="maps" class="row">
    <!--- Description de la carte -->
    <div class="col-xs-6 col-sm-6 col-md-6 dcarto">

    <div>
            <!-- Début SVG pour dropdown -->
    <?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 23.0.4, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="-78 375 595.28 60.89" style="enable-background:new 0 0 595.28 841.89;" xml:space="preserve" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<style type="text/css">
	.st0{fill:#FFFFFF;}
</style>
<g>
	<g>
		<circle class="st0" cx="220.5" cy="405.78" r="5.07"/>
		<path class="st0" d="M456.1,385.76H207.38l-14.68,25.61l14.68,25.61H456.1l14.68-25.61L456.1,385.76z M231.5,415.64l-3.21,4.64
			l-7.91,11.42l-7.9-11.42l-3.23-4.66c-4.15-5.53-3.55-14.68,1.31-19.53c2.63-2.63,6.12-4.07,9.83-4.07s7.21,1.45,9.83,4.07
			C235.06,400.94,235.66,410.08,231.5,415.64z"/>
        <g>
                <text  y="420" style="font-size:2rem;" x="255">CARTO / ANNUAIRE</text>
        </g>
	</g>
</g>
</svg>

    <!-- FIN SVG pojur dropdown -->

        <div class="dropdown-menu col-xs-12 col-sm-12 col-md-12">
            <!-- <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/barre/acteurscarto.svg" class="dropdown-item img-responsive">  -->
        
        <!-- Début SVG -->
        <?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 23.0.4, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="-140 126 595.28 210.89" style="enable-background:new 0 0 595.28 841.89;" xml:space="preserve">
<style type="text/css">
	.poly0{fill:#41435B;}
	.st1{fill:#FFFFFF;}
    .actor-svg{fill:white; font-size : 1.2vw;}
</style>
<g>
	<polygon class="poly0" points="406.64,151.43 391.96,177.03 143.24,177.03 128.57,151.43 143.24,125.82 391.96,125.82 	"/>
</g>
<g>
	<polygon class="poly0" points="406.64,202.64 391.96,228.24 143.24,228.24 128.57,202.64 143.24,177.03 391.96,177.03 	"/>
</g>
<g>
	<polygon class="poly0" points="406.64,253.85 391.96,279.45 143.24,279.45 128.57,253.85 143.24,228.24 391.96,228.24 	"/>
</g>
<g>
	<polygon class="poly0" points="406.64,305.06 391.96,330.66 143.24,330.66 128.57,305.06 143.24,279.45 391.96,279.45 	"/>
</g>

<g>
    <text y="160" x="210" class="actor-svg"> <a href="javascript:;" data-hash="#productor" class="lbh-menu-app " style="text-decoration : none;">Acteurs producteurs</a></text>
</g>
<g>
	<g>
		<path class="st1" d="M174.19,243.99h-6.11v-1.18c0-0.33-0.26-0.59-0.59-0.59c-0.33,0-0.59,0.27-0.59,0.59v1.18h-6.11
			c-0.87,0-1.58,0.71-1.58,1.58v2.08l-0.82-0.27c-0.21-0.08-2.13-0.74-3.89-0.83l-0.23,0.47h-0.01l0.66,5.17l-0.79,1.38l-0.79-1.38
			l0.66-5.17H154l-0.23-0.47c-1.83,0.09-3.82,0.81-3.91,0.84c-0.03,0.01-0.05,0.04-0.08,0.05c-0.06,0.03-0.12,0.07-0.18,0.11
			c-0.05,0.04-0.09,0.08-0.13,0.12c-0.04,0.05-0.08,0.1-0.11,0.15c-0.03,0.06-0.06,0.12-0.08,0.18c-0.01,0.03-0.04,0.06-0.04,0.1
			l-1.58,7.1c-0.12,0.53,0.22,1.06,0.75,1.18c0.07,0.02,0.14,0.02,0.21,0.02c0.45,0,0.86-0.31,0.96-0.77l1.46-6.55
			c0.1-0.03,0.22-0.07,0.34-0.1v6.73l-0.78,9.76c-0.05,0.65,0.43,1.22,1.08,1.27c0.03,0,0.06,0,0.1,0c0.61,0,1.13-0.47,1.18-1.09
			l0.71-8.82c0.15,0.06,0.31,0.1,0.48,0.1c0.17,0,0.33-0.04,0.48-0.1l0.71,8.82c0.05,0.62,0.57,1.09,1.18,1.09c0.03,0,0.06,0,0.1,0
			c0.65-0.05,1.14-0.62,1.08-1.27l-0.78-9.76v-6.73c0.5,0.14,0.84,0.26,0.85,0.27c0,0,0.01,0,0.01,0c0,0,0.01,0,0.01,0.01l1.45,0.48
			v5.31c0,0.87,0.71,1.58,1.58,1.58h2.87l-2.52,9.4c-0.08,0.32,0.1,0.64,0.42,0.72c0.05,0.01,0.1,0.02,0.15,0.02
			c0.26,0,0.5-0.17,0.57-0.44l2.6-9.71h2.02v6.88c0,0.33,0.26,0.59,0.59,0.59c0.33,0,0.59-0.26,0.59-0.59v-6.88h2.02l2.6,9.71
			c0.07,0.26,0.31,0.44,0.57,0.44c0.05,0,0.1-0.01,0.15-0.02c0.31-0.08,0.5-0.41,0.42-0.72l-2.52-9.4h2.87
			c0.87,0,1.58-0.71,1.58-1.58v-9.46C175.77,244.69,175.06,243.99,174.19,243.99z M174.98,255.03c0,0.43-0.35,0.79-0.79,0.79h-13.4
			c-0.44,0-0.79-0.35-0.79-0.79v-5.05l2.5,0.83c0.1,0.03,0.21,0.05,0.31,0.05c0.41,0,0.8-0.26,0.93-0.67
			c0.02-0.07,0.02-0.14,0.03-0.21l5.93-2.74c0.1-0.05,0.14-0.16,0.1-0.26c-0.05-0.1-0.16-0.14-0.26-0.1l-5.8,2.68
			c-0.09-0.28-0.3-0.52-0.61-0.62L160,247.9v-2.34c0-0.43,0.35-0.79,0.79-0.79h13.41c0.44,0,0.79,0.35,0.79,0.79L174.98,255.03
			L174.98,255.03L174.98,255.03z"/>
		<path class="st1" d="M154.13,246.33c1.37,0,2.49-1.6,2.49-2.97c0-1.38-1.12-2.49-2.49-2.49c-1.38,0-2.49,1.12-2.49,2.49
			C151.64,244.73,152.76,246.33,154.13,246.33z"/>
		<path class="st1" d="M168.66,251l0.41-0.25l-0.4-0.64l-0.41,0.25c-0.19-0.2-0.41-0.36-0.65-0.46l0.11-0.47l-0.74-0.17l-0.11,0.47
			c-0.26-0.01-0.53,0.03-0.79,0.13l-0.26-0.41l-0.64,0.4l0.26,0.41c-0.2,0.19-0.36,0.41-0.46,0.65l-0.47-0.11l-0.17,0.74l0.47,0.11
			c-0.01,0.26,0.03,0.53,0.13,0.79l-0.41,0.25l0.4,0.64l0.41-0.26c0.18,0.2,0.41,0.36,0.65,0.46L165.9,254l0.73,0.17l0.11-0.47
			c0.26,0.01,0.53-0.03,0.79-0.13l0.25,0.41l0.64-0.4l-0.25-0.41c0.2-0.19,0.36-0.41,0.46-0.65l0.47,0.11l0.17-0.74l-0.47-0.11
			C168.8,251.52,168.76,251.25,168.66,251z M167.53,252.86c-0.63,0.4-1.47,0.2-1.87-0.43c-0.4-0.63-0.2-1.47,0.43-1.87
			c0.64-0.4,1.47-0.2,1.87,0.43C168.35,251.63,168.16,252.47,167.53,252.86z"/>
		<path class="st1" d="M172.75,249.8l-0.36,0.12c-0.11-0.19-0.25-0.35-0.42-0.48l0.17-0.34l-0.54-0.27l-0.17,0.34
			c-0.2-0.06-0.42-0.07-0.63-0.04l-0.12-0.36l-0.57,0.19l0.12,0.36c-0.19,0.11-0.35,0.25-0.48,0.42l-0.34-0.17l-0.27,0.54l0.34,0.17
			c-0.06,0.2-0.07,0.42-0.04,0.63l-0.36,0.12l0.19,0.57l0.36-0.12c0.11,0.19,0.25,0.35,0.42,0.48l-0.17,0.34l0.54,0.27l0.17-0.34
			c0.2,0.06,0.42,0.07,0.63,0.04l0.12,0.36l0.57-0.19l-0.12-0.36c0.19-0.11,0.35-0.25,0.48-0.42l0.34,0.17l0.27-0.54l-0.34-0.17
			c0.06-0.2,0.07-0.42,0.04-0.63l0.36-0.12L172.75,249.8z M171.36,251.73c-0.57,0.19-1.18-0.11-1.37-0.68
			c-0.19-0.56,0.11-1.18,0.68-1.37c0.57-0.19,1.18,0.11,1.37,0.68C172.22,250.93,171.92,251.54,171.36,251.73z"/>
	</g>
</g>

<g>
    <text y="210" x="180" class="actor-svg"><a href="javascript:;" data-hash="#consumme" class="lbh-menu-app " style="text-decoration : none;">Acteurs consommateurs</a></text>
</g>
<g>
	<g>
		<path class="st1" d="M167.24,307.81c-0.19-0.45-0.64-0.73-1.06-0.91l-1.43-0.66c0-1.05-0.01-6.26-0.01-6.81
			c0.01-0.85-0.09-1.62-0.32-2.28l1.78-0.64l0,1.64l-0.12,0c-0.08-0.01-0.16,0.05-0.17,0.14l-0.16,1.36c0,0.04,0.01,0.08,0.03,0.11
			c0.03,0.03,0.06,0.05,0.11,0.06l1.24,0c0.04,0,0.08-0.02,0.11-0.06c0.03-0.03,0.04-0.07,0.03-0.12c-0.03-0.27-0.06-0.53-0.09-0.8
			l-0.06-0.56c-0.01-0.08-0.08-0.14-0.17-0.14l-0.12,0l0-1.87l0.06-0.02l0.21-0.08c0.18-0.06,0.3-0.23,0.3-0.42
			c0-0.19-0.12-0.36-0.3-0.42l-7.43-2.69c-0.1-0.04-0.21-0.04-0.31,0l-7.43,2.69c-0.18,0.06-0.3,0.23-0.3,0.42s0.12,0.36,0.3,0.42
			l2.69,0.97c-0.24,0.68-0.34,1.46-0.33,2.33l0,6.75l-1.43,0.66c-0.43,0.18-0.85,0.44-1.06,0.91c0,0-3.15,8.04-1.44,8.04h18.3
			C170.39,315.84,167.24,307.81,167.24,307.81z M162.39,307.12c0,0-2.39,0.79-2.86,3.58c0,0.01-0.01,0.01-0.01,0
			c-0.46-2.79-2.86-3.58-2.86-3.58l1.57-1.56c-2.21-0.99-2.8-4.37-2.8-4.37c-0.13-0.49-0.14-0.82-0.14-0.82
			c1.11-0.08,4.97-0.41,5.24-1.66c0.36,0.67,2.29,2.37,3.07,2.49c0,0-0.76,3.34-2.83,4.35L162.39,307.12z"/>
	</g>
</g>
<g>
    <text y="260" x="215" class="actor-svg"><a href="javascript:;" data-hash="#former" class="lbh-menu-app " style="text-decoration : none;">Acteurs formateurs</a></text>
</g>
<g>
	<path class="st1" d="M155.88,147.5c-0.08,0.01-0.34,0.23-0.34,0.23C155.68,147.79,155.96,147.5,155.88,147.5z M155.92,147.45
		l-0.02,0.01C155.9,147.46,155.91,147.46,155.92,147.45C155.92,147.46,155.92,147.46,155.92,147.45z M165.67,137.33
		c1.46,0,2.64,1.18,2.64,2.64c0,1.46-1.18,2.64-2.64,2.64c-1.46,0-2.64-1.18-2.64-2.64C163.02,138.51,164.21,137.33,165.67,137.33z
		 M149.79,148.66h0.71v-0.34h-0.71c-0.19,0-0.33-0.04-0.38-0.1c-0.04-0.05-0.02-0.12-0.02-0.12l0.01-2.06h-0.34v1.98
		c-0.02,0.07-0.03,0.24,0.08,0.39C149.25,148.58,149.48,148.66,149.79,148.66z M172.19,153.49l-0.1-0.78
		c-0.38-3.04-0.39-3.16-0.25-8.53c0-0.19-0.14-0.34-0.33-0.35c-0.18-0.01-0.34,0.14-0.35,0.33c-0.15,5.42-0.13,5.54,0.25,8.63
		l0.1,0.78c0.1,0.75,0.01,1.27-0.24,1.47c-0.3,0.23-0.81,0.03-0.85,0.01c-2.5-0.77-5.62,0.82-5.76,0.89
		c-0.17,0.09-0.23,0.29-0.15,0.46c0.06,0.12,0.18,0.18,0.3,0.18c0.05,0,0.11-0.01,0.16-0.04c0.02-0.01,1.41-0.72,2.99-0.95v5.3
		h-1.41c-0.29,0-0.52,0.23-0.52,0.52c0,0.29,0.23,0.52,0.52,0.52h4.02c0.29,0,0.52-0.23,0.52-0.52c0-0.29-0.23-0.52-0.52-0.52h-1.3
		v-5.36c0.31,0.02,0.62,0.07,0.92,0.16c0.04,0.01,0.88,0.36,1.49-0.11C172.15,155.22,172.32,154.52,172.19,153.49z M170.57,151.95
		c-0.06-2.24-0.79-4.39-0.85-6.62c-0.09-3.25-5.06-3.32-5.1-0.15c-1.9,0.32-3.81,0.5-5.74,0.41c-1.76-0.08-1.76,2.64-0.02,2.72
		c2.03,0.1,4.05-0.1,6.06-0.43c0.21,1.19,0.45,2.38,0.53,3.58c-4.14,1.45-6.44,4.96-7.03,9.41c0,0.03-0.01,0.06-0.01,0.1v-9.87h2.61
		v-2.33h-9.37c0.01-0.03,0.01-0.06,0.01-0.11c0-0.03,0-0.05,0-0.09c0.01-0.2,0.05-0.26,0.24-0.3c0.05-0.01,0.11-0.01,0.17-0.01v0.16
		h2.52c0.07,0.01,0.15,0.02,0.21,0.02c0.11,0,0.2-0.02,0.25-0.1c0.03-0.04,0.03-0.09,0.03-0.14c0-0.05,0-0.1,0.02-0.13
		c0.04-0.04,0.11-0.07,0.16-0.08c-0.05,0.1-0.09,0.21-0.09,0.32h2.85c0-0.55-0.64-0.99-1.42-0.99c-0.25,0-0.47,0.05-0.67,0.12
		c0.05,0.47-0.39,0.42-0.57,0.39c-0.02,0.03-0.04,0.05-0.05,0.08c-0.01,0-0.01-0.01-0.02-0.01c-0.08,0-0.19,0.04-0.24,0.11
		c-0.04,0.05-0.04,0.12-0.04,0.18c0,0.04,0,0.08-0.01,0.09c-0.03,0.04-0.09,0.05-0.16,0.06v-0.39h-2.76v0.21
		c-0.07,0-0.14,0.01-0.19,0.02c-0.24,0.05-0.29,0.16-0.31,0.38c0,0.03,0,0.06,0,0.09c0,0.05,0,0.08-0.01,0.11h-5.18v2.33h1.42v11.32
		h10.6v-1.13c0.17,1.74,3.15,1.58,3.41-0.41c0.45-3.45,2.46-5.75,5.85-6.51C169.06,154.57,170.62,153.77,170.57,151.95z
		 M150.22,141.08l0.43,6.11l-1.13,0.08l-0.43-6.11L150.22,141.08z"/>
</g>
<g>
    <text y="310" x="230" class="actor-svg"><a href="javascript:;" data-hash="#student" class="lbh-menu-app " style="text-decoration : none;">Acteurs étudiants</a></text>
</g>
<g>
	<g>
		<path class="st1" d="M151.73,205.74l0.59-0.59h4.33l-2.76-4.99l-2.04,0.42c-0.54,0.11-0.96,0.51-1.1,1.04l-2.43,7.92
			c-0.11,0.43-0.02,0.89,0.25,1.24c0.27,0.35,0.69,0.56,1.14,0.56h2.02L151.73,205.74L151.73,205.74z"/>
		<path class="st1" d="M165.75,209.53l-2.43-7.92c-0.14-0.54-0.58-0.94-1.12-1.05l-2.04-0.39l-2.75,4.97h4.66l0.59,0.59v5.59h1.7
			c0.45,0,0.87-0.21,1.14-0.56C165.77,210.41,165.86,209.96,165.75,209.53z"/>
		<polygon class="st1" points="152.92,211.33 152.75,211.33 152.75,211.33 161.32,211.33 161.32,211.33 161.48,211.33 
			161.48,206.33 152.92,206.33 		"/>
		<rect x="150.55" y="211.87" class="st1" width="12.97" height="1.07"/>
		<path class="st1" d="M152.42,196.66c0.02,0.05,0.05,0.12,0.08,0.19c0.13,0.3,0.23,0.53,0.37,0.66c0.66,2.15,2.36,3.63,4.16,3.63
			c1.8-0.01,3.47-1.51,4.11-3.66c0.14-0.13,0.24-0.36,0.36-0.66c0.03-0.08,0.06-0.15,0.08-0.2c0.24-0.5,0.16-1.05-0.18-1.31
			c0-0.02,0-0.03,0-0.05c-0.01-2.66-0.36-5.51-4.41-5.49c-4.08,0.01-4.41,2.74-4.4,5.52c0,0.02,0,0.03,0,0.05
			C152.25,195.61,152.17,196.16,152.42,196.66z M152.99,195.8l0.01,0c0.07-0.02,0.12-0.07,0.15-0.13l0.71,0.11
			c0.07,0.01,0.13-0.03,0.16-0.09l0.45-1.03c0.51,0.12,2.42,0.55,3.92,0.55c0.46,0,0.85-0.05,1.15-0.13l0.28,0.68
			c0.02,0.06,0.08,0.09,0.14,0.09c0.01,0,0.03,0,0.04-0.01l0.82-0.23c0.03,0.07,0.09,0.13,0.17,0.16l0.01,0
			c0.12,0.06,0.16,0.33,0.03,0.59c-0.03,0.06-0.06,0.15-0.1,0.23c-0.05,0.12-0.15,0.38-0.22,0.45c-0.07,0.04-0.12,0.1-0.14,0.18
			c-0.54,1.95-2,3.31-3.56,3.32c-1.56,0-3.05-1.35-3.61-3.29c-0.02-0.08-0.07-0.14-0.14-0.18c-0.06-0.07-0.17-0.32-0.22-0.45
			c-0.03-0.08-0.07-0.16-0.1-0.23C152.83,196.13,152.86,195.86,152.99,195.8z"/>
	</g>
</g>
</svg>


        <!-- Fin SVG -->
        </div>
        
    </div>

  
    </div>

    <!-- Image de la réunion --> 
    <div class="col-xs-6  col-sm-6 col-md-6 mimage" >
        <!-- En attente d'une image -->
       
       <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/home/carterun.png" class="img-responsive" style="margin-left: -11%; width: 90%">
    </div>

    <div class="col-xs-6 col-sm-6 col-md-6 " style="margin-top : -28%;">
    <p class="description-carto">Lorem ipsum dolor sit <br>
                                        amet, consectetuer <br>
                                    adpiscing elit, sed diam <br>
                                                    nonummy <br>
        <a href="javascript:;" data-hash="#map" class="lbh-menu-app" style="text-decoration : none;">
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/home/plus-carto.svg" class="img-responsive">
        </a>
        </p>
    </div>
</div>

<!-- Image de fond --> 
<div class="content-img bloc3" style="margin-top: 10%;">
    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/bloc/bloc3.png" class="img-responsive" style="width:50%; margin-left: 50%;">
</div>
<div id="actualite" class="row">
    <div class="col-xs-6 col-sm-6 col-md-6">
    <!-- Voir comment on récupère l'actualité --> 
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 actuimage">
    <!-- <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/barre/actu.svg" class="img-responsive"> -->
    <!-- debut svg -->
    <?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 23.0.4, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="40 415 595.28 75.89" style="enable-background:new 0 0 595.28 841.89;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#FFFFFF;}
</style>
<g>
	<path class="st0" d="M263.1,465.26v-30.52h-36.63v30.52c0,0,0,3.05,3.05,3.05h34.61C263.54,467.64,263.1,466.67,263.1,465.26z
		 M243.26,462.2h-13.73v-15.26h13.73V462.2z M257,462.2h-10.69v-3.05H257V462.2z M260.05,456.1h-13.73v-3.05h13.73V456.1z
		 M260.05,450h-13.73v-3.05h13.73V450z M260.05,443.89h-30.52v-3.05h30.52V443.89z"/>
	<path class="st0" d="M472.94,425.91H224.22l-14.68,25.61l14.68,25.61h248.72l14.68-25.61L472.94,425.91z M272.26,465.26
		c0,6.01-4.58,6.11-4.58,6.11h-38.16c-6.11,0-6.11-6.11-6.11-6.11v-33.58h42.74v6.1h6.11V465.26z"/>
    <g>
        <text style="font-size:2rem; fill:#41435b;" y="460"  x="335">ACTUALITÉS</text>
    </g>
</g>
</svg>
    <!-- fin svg -->

    </div>


<!-- affiche les actu -->
<div class="row">
    <div class="actucard col-xs-12 col-sm-12 col-md-12">
       <div id="newsstream" style="margin: 1%;">
       </div>
    </div>
</div>
    <div style="margin-top: 2%">
        <p align="center">
        <a href="javascript:;" data-hash="#live" class="lbh-menu-app " style="text-decoration : none; font-size:2rem;">
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/plus.svg" class="img-responsive" style="width: 50%;"><br>
        Voir plus <br>
        d'actualités</p>
        </a>
        </p>
    </div>

</div>

<!-- image de fond
<div class="content-image bloc4">
    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/bloc/bloc4.png" class="img-responsive">
</div> -->


<!-- div de l'agenda -->
<div id="agenda" class="row" style="margin-top: 1%;">

    <div style="margin-top: 11%; margin-bottom : 100%;">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <!-- <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/barre/agenda.svg" class="img-responsive"> -->
            <?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 23.0.4, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="-120 310 595.28 75.89" style="enable-background:new 0 0 595.28 841.89;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#FFFFFF;}
</style>
<path class="st0" d="M230.52,342.34h-25.76c-0.82,0-1.49,0.69-1.49,1.53v14.54c0,0.85,0.67,1.54,1.49,1.54h25.76
	c0.82,0,1.49-0.69,1.49-1.54v-14.54C232.01,343.03,231.34,342.34,230.52,342.34z M212.46,356.96c0,0.5-0.39,0.9-0.87,0.9h-3.05
	c-0.48,0-0.87-0.4-0.87-0.9v-3.15c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.4,0.87,0.9V356.96z M212.46,349.1
	c0,0.5-0.39,0.9-0.87,0.9h-3.05c-0.48,0-0.87-0.41-0.87-0.9v-3.14c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.41,0.87,0.9V349.1z
	 M220.07,356.96c0,0.5-0.39,0.9-0.87,0.9h-3.04c-0.48,0-0.87-0.4-0.87-0.9v-3.15c0-0.5,0.39-0.9,0.87-0.9h3.04
	c0.48,0,0.87,0.4,0.87,0.9V356.96z M220.07,349.1c0,0.5-0.39,0.9-0.87,0.9h-3.04c-0.48,0-0.87-0.41-0.87-0.9v-3.14
	c0-0.5,0.39-0.9,0.87-0.9h3.04c0.48,0,0.87,0.41,0.87,0.9V349.1z M227.68,356.96c0,0.5-0.39,0.9-0.87,0.9h-3.05
	c-0.48,0-0.87-0.4-0.87-0.9v-3.15c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.4,0.87,0.9V356.96z M227.68,349.1
	c0,0.5-0.39,0.9-0.87,0.9h-3.05c-0.48,0-0.87-0.41-0.87-0.9v-3.14c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.41,0.87,0.9V349.1z
	 M230.52,342.34h-25.76c-0.82,0-1.49,0.69-1.49,1.53v14.54c0,0.85,0.67,1.54,1.49,1.54h25.76c0.82,0,1.49-0.69,1.49-1.54v-14.54
	C232.01,343.03,231.34,342.34,230.52,342.34z M212.46,356.96c0,0.5-0.39,0.9-0.87,0.9h-3.05c-0.48,0-0.87-0.4-0.87-0.9v-3.15
	c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.4,0.87,0.9V356.96z M212.46,349.1c0,0.5-0.39,0.9-0.87,0.9h-3.05
	c-0.48,0-0.87-0.41-0.87-0.9v-3.14c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.41,0.87,0.9V349.1z M220.07,356.96
	c0,0.5-0.39,0.9-0.87,0.9h-3.04c-0.48,0-0.87-0.4-0.87-0.9v-3.15c0-0.5,0.39-0.9,0.87-0.9h3.04c0.48,0,0.87,0.4,0.87,0.9V356.96z
	 M220.07,349.1c0,0.5-0.39,0.9-0.87,0.9h-3.04c-0.48,0-0.87-0.41-0.87-0.9v-3.14c0-0.5,0.39-0.9,0.87-0.9h3.04
	c0.48,0,0.87,0.41,0.87,0.9V349.1z M227.68,356.96c0,0.5-0.39,0.9-0.87,0.9h-3.05c-0.48,0-0.87-0.4-0.87-0.9v-3.15
	c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.4,0.87,0.9V356.96z M227.68,349.1c0,0.5-0.39,0.9-0.87,0.9h-3.05
	c-0.48,0-0.87-0.41-0.87-0.9v-3.14c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.41,0.87,0.9V349.1z M445.03,318.29H196.31
	l-14.68,25.61l14.68,25.61h248.72l14.68-25.61L445.03,318.29z M225.57,325.39c0-0.74,0.59-1.35,1.31-1.35h2.37
	c0.72,0,1.3,0.61,1.3,1.35v7.88c0,0.74-0.59,1.34-1.3,1.34h-2.37c-0.73,0-1.31-0.6-1.31-1.34V325.39z M204.72,325.39
	c0-0.74,0.59-1.35,1.3-1.35h2.38c0.72,0,1.3,0.61,1.3,1.35v7.88c0,0.74-0.59,1.34-1.3,1.34h-2.38c-0.72,0-1.3-0.6-1.3-1.34V325.39z
	 M236.47,360.17c0,1.96-1.55,3.58-3.45,3.58h-30.7c-1.9,0-3.45-1.61-3.45-3.58v-28.14c0-1.93,1.51-3.5,3.35-3.56v4.8
	c0,2.17,1.7,3.92,3.8,3.92h2.4c2.11,0,3.83-1.75,3.83-3.92v-4.82h10.82v4.82c0,2.17,1.73,3.92,3.83,3.92h2.4
	c2.1,0,3.8-1.75,3.8-3.92v-4.8c1.84,0.06,3.35,1.62,3.35,3.56V360.17z M230.52,342.34h-25.76c-0.82,0-1.49,0.69-1.49,1.53v14.54
	c0,0.85,0.67,1.54,1.49,1.54h25.76c0.82,0,1.49-0.69,1.49-1.54v-14.54C232.01,343.03,231.34,342.34,230.52,342.34z M212.46,356.96
	c0,0.5-0.39,0.9-0.87,0.9h-3.05c-0.48,0-0.87-0.4-0.87-0.9v-3.15c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.4,0.87,0.9V356.96z
	 M212.46,349.1c0,0.5-0.39,0.9-0.87,0.9h-3.05c-0.48,0-0.87-0.41-0.87-0.9v-3.14c0-0.5,0.39-0.9,0.87-0.9h3.05
	c0.48,0,0.87,0.41,0.87,0.9V349.1z M220.07,356.96c0,0.5-0.39,0.9-0.87,0.9h-3.04c-0.48,0-0.87-0.4-0.87-0.9v-3.15
	c0-0.5,0.39-0.9,0.87-0.9h3.04c0.48,0,0.87,0.4,0.87,0.9V356.96z M220.07,349.1c0,0.5-0.39,0.9-0.87,0.9h-3.04
	c-0.48,0-0.87-0.41-0.87-0.9v-3.14c0-0.5,0.39-0.9,0.87-0.9h3.04c0.48,0,0.87,0.41,0.87,0.9V349.1z M227.68,356.96
	c0,0.5-0.39,0.9-0.87,0.9h-3.05c-0.48,0-0.87-0.4-0.87-0.9v-3.15c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.4,0.87,0.9V356.96z
	 M227.68,349.1c0,0.5-0.39,0.9-0.87,0.9h-3.05c-0.48,0-0.87-0.41-0.87-0.9v-3.14c0-0.5,0.39-0.9,0.87-0.9h3.05
	c0.48,0,0.87,0.41,0.87,0.9V349.1z M230.52,342.34h-25.76c-0.82,0-1.49,0.69-1.49,1.53v14.54c0,0.85,0.67,1.54,1.49,1.54h25.76
	c0.82,0,1.49-0.69,1.49-1.54v-14.54C232.01,343.03,231.34,342.34,230.52,342.34z M212.46,356.96c0,0.5-0.39,0.9-0.87,0.9h-3.05
	c-0.48,0-0.87-0.4-0.87-0.9v-3.15c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.4,0.87,0.9V356.96z M212.46,349.1
	c0,0.5-0.39,0.9-0.87,0.9h-3.05c-0.48,0-0.87-0.41-0.87-0.9v-3.14c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.41,0.87,0.9V349.1z
	 M220.07,356.96c0,0.5-0.39,0.9-0.87,0.9h-3.04c-0.48,0-0.87-0.4-0.87-0.9v-3.15c0-0.5,0.39-0.9,0.87-0.9h3.04
	c0.48,0,0.87,0.4,0.87,0.9V356.96z M220.07,349.1c0,0.5-0.39,0.9-0.87,0.9h-3.04c-0.48,0-0.87-0.41-0.87-0.9v-3.14
	c0-0.5,0.39-0.9,0.87-0.9h3.04c0.48,0,0.87,0.41,0.87,0.9V349.1z M227.68,356.96c0,0.5-0.39,0.9-0.87,0.9h-3.05
	c-0.48,0-0.87-0.4-0.87-0.9v-3.15c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.4,0.87,0.9V356.96z M227.68,349.1
	c0,0.5-0.39,0.9-0.87,0.9h-3.05c-0.48,0-0.87-0.41-0.87-0.9v-3.14c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.41,0.87,0.9V349.1z
	 M230.52,342.34h-25.76c-0.82,0-1.49,0.69-1.49,1.53v14.54c0,0.85,0.67,1.54,1.49,1.54h25.76c0.82,0,1.49-0.69,1.49-1.54v-14.54
	C232.01,343.03,231.34,342.34,230.52,342.34z M212.46,356.96c0,0.5-0.39,0.9-0.87,0.9h-3.05c-0.48,0-0.87-0.4-0.87-0.9v-3.15
	c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.4,0.87,0.9V356.96z M212.46,349.1c0,0.5-0.39,0.9-0.87,0.9h-3.05
	c-0.48,0-0.87-0.41-0.87-0.9v-3.14c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.41,0.87,0.9V349.1z M220.07,356.96
	c0,0.5-0.39,0.9-0.87,0.9h-3.04c-0.48,0-0.87-0.4-0.87-0.9v-3.15c0-0.5,0.39-0.9,0.87-0.9h3.04c0.48,0,0.87,0.4,0.87,0.9V356.96z
	 M220.07,349.1c0,0.5-0.39,0.9-0.87,0.9h-3.04c-0.48,0-0.87-0.41-0.87-0.9v-3.14c0-0.5,0.39-0.9,0.87-0.9h3.04
	c0.48,0,0.87,0.41,0.87,0.9V349.1z M227.68,356.96c0,0.5-0.39,0.9-0.87,0.9h-3.05c-0.48,0-0.87-0.4-0.87-0.9v-3.15
	c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.4,0.87,0.9V356.96z M227.68,349.1c0,0.5-0.39,0.9-0.87,0.9h-3.05
	c-0.48,0-0.87-0.41-0.87-0.9v-3.14c0-0.5,0.39-0.9,0.87-0.9h3.05c0.48,0,0.87,0.41,0.87,0.9V349.1z"/>
    <g>
        <text style="font-size:2rem; fill:#41435b;" y="350"  x="335">AGENDA</text>
    </g>
</svg>

        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
          <p style="font-size : 2rem; color:white; margin-top: 4%;">Évènement à venir !</p>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12" style="margin-top : 1%; align-items: flex-end; flex-flow: row;">
        <!-- Debut des trois events -->
        <div id="event-affiche"></div>
<!-- 
            <div class="col-xs-4">
                <div class="card">
                Truc lalala
                <div class="col-xs-12 card-c">
                    <div class="card-info col-xs-6">
                    12 juillet <br>
                    Culture
                    </div>
                </div>
                </div>

            </div>
            <div class="col-xs-4">
                <div class="card">
                    triple kill ez
                    <div class="col-xs-12 card-c">
                        <div class="card-info col-xs-6">
                        12 juillet <br>
                        Culture
                        </div>
                    </div>
                </div>
             
            </div>
            <div class="col-xs-4">
                <div class="card">
                    penta kill ez
                    <div class="col-xs-12 card-c">
                        <div class="card-info col-xs-6">
                        12 juillet <br>
                        Culture
                        </div>
                    </div>
                </div>
         
            </div> -->

        <!-- Fin -->
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12" style="margin-top : 3%;">
            <p align="center">
            <a href="javascript:;" data-hash="#agenda" class="lbh-menu-app " style="text-decoration : none;">
                    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/plus.svg" class="img-responsive" style="width: 50%;"><br>
                    <span class="levent" style="font-size: 2rem;">Voir plus <br> d'évènements</span>
                </a>
            </p>
        </div>

        <!-- Carrousel Debut --> 

        <div class="col-xs-12 col-lg-12 col-sm-12 col-md-12 carousel-border" style="margin-top : 3%;">
    	<div id="docCarousel" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner itemctr">
            </div>

            <div id="caroussel" class="carousel-control col-xs-6 col-sm-6 col-md-6">
                <div id="arrow-caroussel">
                <a href="#docCarousel" data-slide="prev"><i class="fa fa-arrow-left"></i></a> <a href="#docCarousel" data-slide="next"><i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
    			<!-- <a class="left carousel-control" href="#docCarousel" data-slide="prev">
    				<span class="glyphicon glyphicon-chevron-left"></span>
    				<span class="sr-only">Previous</span>
  				</a>
  				<a class="right carousel-control" href="#docCarousel" data-slide="next">
    				<span class="glyphicon glyphicon-chevron-right"></span>
    				<span class="sr-only">Next</span>
  				</a> -->
    	</div>
  	</div> 

  
        <!-- Fin -->
    </div>
    
</div>

<!-- Survey désactivé --> 
<script type="text/javascript">

$(document).ready(function(){
    console.log("---------------- Document ready");
    afficheEvent();


    urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false";
	// console.log("C'EST ICIIIIIIIIIII",urlNews);

    ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false}, function(news){}, "html");
});


function afficheEvent(){
    console.log("----------------- Affichage évènement");
    $.ajax({
        type : "GET",
        url : baseUrl + "/costum/coeurnumerique/geteventaction",
        dataType : "json",
        async : false,
        success : function(data){
            console.log("success : ",data);
            var str = "";
            var ctr = "";
            var itr = "";
            var resume = "";
            var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + costum.htmlConstruct.directory.results.events.defaultImg;
            console.log(url);
            var i = 1;
            if(data.result == true){
                $(data.element).each(function(key,value){

                    var startDate = (typeof value.startDate != "undefined") ? "Du "+dateToStr(value.startDate, "fr", false, true) : null;

                    var imgMedium = (value.imgMedium != "none") ? '/ph/'+value.imgMedium : url;
                    var img = (value.img != "none") ? '/ph/'+value.img : url;

                    str += "<div class='col-xs-4 col-sm-4 col-md-4'>";
                    str += "<div class='card'>";
                    str += "<a class='lbh-preview-element' data-hash='#page.type.events.id."+value.id+"' href='@"+value.slug+"' style='text-decoration : none;'>";
                    str += "<img src='"+imgMedium+"' class='img-responsive'>";
                    str += "<div class='col-xs-12 col-sm-12 col-md-12 card-c'>";
                    str += "<div class='col-xs-6 col-sm-6 col-md-6 card-info'>"+startDate+"<br>"+value.type;
                    str += "</a></div></div></div></div>";

                    if(i == 1) ctr += "<div class='item active'>";
                    else ctr += "<div class='item'>";

                    ctr += "<div class='resume col-xs-3 col-sm-3 col-md-3'>";
                    ctr += "<h2>"+startDate+"</h2>";
                    ctr += "<h2>"+value.name+"</h2>";
                    ctr += "<p>"+value.resume+"<p>";
                    ctr += "</div>";

                    ctr += "<img src='"+img+"' class='ctr-img img-responsive'>";
                    ctr += "</div>";

                    i++;
                });
                console.log(ctr);
                $(".itemctr").html(ctr);
                // $("#tclass").html(ctr);
                // $(".resume").html(resume);
                console.log(ctr);
                console.log(resume);
              
            }
            else{
                str += "<div class='col-xs-12 col-sm-12 col-md-12'><b>Aucun évènement n'est prévu</b></div>";
            }
            $("#event-affiche").html(str);
            console.log(str);
        },
        error : function(e){
            console.log("error : ",e);
        }
    });
}

</script>
