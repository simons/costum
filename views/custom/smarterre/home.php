<?php
$cssAnsScriptFilesTheme = array(
       '/plugins/jsonview/jquery.jsonview.js',
    '/plugins/jsonview/jquery.jsonview.css',
    '/plugins/jQuery/jquery-2.1.1.min.js',
    '/plugins/jQuery/jquery-1.11.1.min.js',
    '/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
    '/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    '/plugins/jQuery-Knob/js/jquery.knob.js',
      '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
      //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js',
    );
?>
<style type="text/css">

.mySlides {
	display:none;
}


#head{
	position:absolute;
	z-index:1;
	text-align:center;
	font-weight: bold;
	margin-top: 8.5%;
	color: white;
	font-size: x-large;
	text-shadow: black 0.1em 0.1em 0.2em;
}

.logo{
	width: 19%;
	position: absolute;
	z-index: 40000;
	left: 40%;
	top: 33%;
}

.hexa{
  width: 40%;
  margin-left: 30%;
  margin-top:-5%;
}

.text{
  text-align: center;
  margin-top: 20px;
}


div.item img{
	margin:auto;
}

.hex{
  -webkit-filter:brightness(50%);
}

.off{
  display: none;
  filter: invert(100%);
}

.on{
  display: inline;
  filter: invert(100%);
}

#dessus{
  stroke: white;
  stroke-width:9;
}

text{
  font-family: 'fb';
}
</style>

	<p class="name" hidden>
		
	</p>
	<div style="padding: 0px 1px;margin-top: -8.8%" class="containerCarousel col-xs-12 col-lg-12">
    	<div id="docCarousel" class="carousel slide" data-ride="carousel">

    		<p style="font-family: 'fb';" id="head" class="col-xs-12">EMULATEUR DE LIENS TERRITORIAUX</p>

    		<img class="logo" src="<?php echo Yii::app()->getModule("costum")->assetsUrl;?>/images/smarterre/Logosmarterre.png">

			<div class="carousel-inner">
        		<div class="item active">
        			<img src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/cercle1.jpg'> 
        		</div>

	        	<div class="item">
	        		<img src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/cercle2.jpg'>
	        	</div>

	        	<div class="item">
	        		<img src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/cercle3.jpg'>
	        	</div>
    		</div>
    			<a class="left carousel-control" href="#docCarousel" data-slide="prev">
            <span style="font-family: 'Glyphicons Halflings' !important;" class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#docCarousel" data-slide="next">
            <span style="font-family: 'Glyphicons Halflings' !important;" class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
  				</a>
    	</div>
  	</div> 


<div style="margin-top: -7%;" class="hexa col-xs-12 ">
  <?php echo $this->renderPartial("costum.views.custom.smarterre.elements.hexa"); ?>
</div>

      


        <div class="text col-xs-12">
          	<p class="textPres" style="font-family: 'ml';font-size: 1.2vw">Apporter de la croissance aux organisations<br>
            locales avec une vision humaine, intégrer<br> 
            un projet de société locale pour les <br> 
            générations futures qui prend en compte <br> 
            l'obsolescence et la finitude des ressources.
        	</p>

        	<a title="En savoir plus sur Smarterre" href="#smarterre" class="lbh"><?php echo Yii::t("home","") ?>
        		<i style="font-size: 2vw;" class="fa fa-plus-circle"></i>
        	</a>
        </div>
        

<div id="bonhomme">
  <?php echo $this->renderPartial("costum.views.custom.smarterre.elements.bonhomme"); ?>
</div>


<script>
console.log("test");
  // var bonhomme = new Vivus($(svg).attr("id"), {type : "sync", duration: "200"});
  // bonhomme.play();

  $("polygon").mouseover(function(){
    console.log($(this).attr('id')+" im"+ $(this).attr('id'));
    $("#im"+$(this).attr('id')).removeClass("off").addClass("on");
  });

  $("polygon").mouseleave(function(){
    console.log($(this).attr('id')+" im"+ $(this).attr('id'));
    $("#im"+$(this).attr('id')).removeClass("on").addClass("off");
  });

  $(".lien").mouseover(function(){
    console.log("PASSE SUR UN LIEN");
  //    blColor(this);

      var a = this.href.animVal;
      var url = a.split("#");

      // console.log(a.animVal);
      blColor(url[1]);
      console.log(url[1]);
});

$(".lien").mouseleave(function(){
    console.log("QUITTE UN LIEN");
    blReset();
});

    //Passage sur humain
    $("#humain").mouseover(function(){
      console.log("PASSAGE SUR HUMAIN");
      bMenuColor("humain");
  });
  $("#humain").mouseleave(function(){
      console.log("QUITTE LES HUMAINS");
      bResetColor("humain");
  });


  //Passage sur territoire
  $("#territoire").mouseover(function(){
      console.log("PASSAGE SUR LES TERRITOIRES");
      bMenuColor("territoire");
  });
  $("#territoire").mouseleave(function(){
      console.log("QUITTE SUR LES TERRITOIRES");
      bResetColor("territoire");
  });

  function bMenuColor(c){
      if(c == "humain"){
          $(".clterritoire").each(function(){
              $(this).children().each(function(){
                  $(this).animate({
                      opacity:"0.2"
                  },200);
              });
          });

      // $(".clhumain").each(function(){
      //     $(this).children().each(function(){
      //         $(this).css({
      //             "transform-origin": "489px 200px",
      //             "transform": "scale(1)",
      //             "-ms-transform": "scale(1)",
      //             "-webkit-transform": "scale(1.5)"
      //         });
      //     });
      // });
          // $(".clhumain").css("fill", "<?php echo Yii::app()->session['costum']['css']['color']['turquoise']; ?>");
          // $(".clterritoire").css("fill","grey");
      }
      else if(c == "territoire"){
          $(".clhumain").each(function(){
              $(this).children().each(function(){
                  $(this).animate({
                      opacity:"0.2"
                  },200);
              });
          });

          // $(".clhumain").css("fill","grey");
          // $(".clterritoire").css("fill", "<?php echo Yii::app()->session['costum']['css']['color']['green']; ?>");
      }
  }

  function bResetColor(c){
      if(c == "humain"){
          $(".clterritoire").each(function(){
              $(this).children().each(function(){
                  $(this).animate({
                      opacity:"1"
                  },200);
              });
          });

          // $(".clhumain").each(function(){
          //     $(this).children().each(function(){
          //         $(this).css("transform-origin", "0px 0px");
          // });
      }
      else if(c == "territoire"){
          console.log("Enter cdt");
          $(".clhumain").each(function(){
              $(this).children().each(function(){
                  $(this).animate({
                      opacity:"1"
                  },200);
              });
          });
      }
  }

  function blColor(url){
      console.log("ENTER blColor");
      $("g").each(function(k,v){
          if(url+"-on" != v.classList[1] && $(v).attr("class") != "st3" && $(v).attr("class")){
            console.log("enter condition");
            $(this).children().each(function(){
              $(this).animate({
                  opacity: "0.2"
              }, 200);
            });
          }
      });
  }

  function blReset(){
          console.log("ENTER IF LEAVE");
          $("g").each(function(){
              $(this).children().each(function(){
                  $(this).animate({
                      opacity: "1"
                  }, 200);
              });
          });
      }

var icones = [];   
var i = 0;
var resultat = [];

$("#menuApp").children("a").each(function(){
	icones.push($(this).children("i").attr('id', i++));
});

// var essai = [
// 	{
// 		"id" : 0,
// 		"name" : "Acces rapide"
// 	},
// 	{
// 		"id" : 1,
// 		"name" : "Live"
// 	},
// 	{
// 		"id" : 2,
// 		"name" : "Agenda"
// 	},
// 	{
// 		"id" : 3,
// 		"name" : "Acteurs"
// 	}
// ];

// $(essai).each(function(key,value){
// 	resultat.push("name : " + value.name);
// });

</script>