<style type="text/css">
        .text-explain{
            font-size: 22px;
        }
    </style>

<div id="sub-doc-page">
    <div class="col-xs-12 support-section section-home col-md-10 col-md-offset-1">
        <div class="col-xs-12 header-section">
            <h3 class="title-section col-sm-8">Le programme de Mayenne Demain</h3>
            <hr>
        </div>
        <div class="col-xs-12">
        
            <span class="col-xs-12 text-left text-explain">
              Voici les premières grandes lignes du programme de Mayenne Demain, écrites en concertation avec tous les membres de la liste, et en tenant compte de vos propositions dans la rubrique <a href="#dda"><?php echo Yii::t("home","Je contribue") ?></a>.<br><br>
              Nous continuons de vous rencontrer et de vous écouter pour étoffer ce programme tous ensemble !
                 <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/mayenneDemain/programme.png'> 
               <br/>
            </span>
        </div> 

    </div>
</div>
