<div class="col-xs-12 margin-top-20">
    <h1 class="text-center">Acteurs socio-économiques</h1>

    <div class="col-xs-12 margin-top-20">
        <?php 
        $params = array(
            "poiList" => PHDB::find(Poi::COLLECTION, 
                        array( "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                               "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                               "type"=>"cms",
                                "structags" => array('$in'=>array("stepsocio1","stepsocio2","stepsocio3","stepsocio4"))
                                )),
            "listSteps" => array("socio1","socio2","socio3","socio4")
        );
        //var_dump($params['poiList']);
        foreach ($params['poiList'] as $id => $value) 
        {
            $documents  = PHDB::find(Document::COLLECTION, array('id' => $id, "type" => Poi::COLLECTION ));
            if( count($documents) )
            {
                $params['poiList'][$id]["documents"] = array();
                foreach ( $documents as $key => $doc ) {
                  $params['poiList'][$id]["documents"][] = array (
                    "name" => $doc['name'], 
                    "doctype" => $doc['doctype'],  
                    "path" => Yii::app()->baseUrl."/".Yii::app()->params['uploadUrl'].$doc["moduleId"]."/".$doc["folder"]."/".$doc['name']
                  );
                }
                //var_dump($params['poiList'][$id]["documents"]);
            }
        }

        //var_dump($params);
        echo $this->renderPartial("costum.views.tpls.wizard",$params,true);
        ?>
    </div>
</div>

