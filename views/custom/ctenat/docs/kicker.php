<style type="text/css">
.img-bd-round{border: 1px solid #ccc; border-radius: 30px;padding : 20px;}
</style>


<div class="col-xs-12 margin-top-20  " id="startSection">
	<h1 class="text-center">Découvrir le CTE</h1>
	<div class="col-sm-6 text-center">
		<a href="javascript:;"  id="userDoc" >
			<img class="img-responsive img-bd-round" width="150" style="margin: auto;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/parcours.png">
			<h3>Suivez votre parcours utilisateur</h3>
		</a>
	</div>
	<div class="col-sm-6 text-center">
		<a href="javascript:;"  id="detailDoc"  >
			<img class="img-responsive img-bd-round" width="150" style="margin: auto;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/docDétaillé.png">
			<h3>Accédez à toute la documentation détaillée</h3>
		</a>
	</div>
</div>

<div id="acteurKick" class="col-xs-12 margin-top-20 hide">
	<h2 class="text-center text-grey">Quel utilisateur êtes vous ?</h2>
	<div class="col-sm-6 text-center">
		<a href="javascript:;"  id="startDoc"  class="link-docs-menu" data-type="public" data-dir="costum.views.custom.ctenat.docs">
			<img class="img-responsive img-bd-round" width="150" style="margin: auto;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/public.png">	
			<h3>Acteur Public</h3>
		</a>
	</div>
	<div class="col-sm-6 text-center">
		<a href="javascript:;"  id="startDoc"  class="link-docs-menu" data-type="socioeco" data-dir="costum.views.custom.ctenat.docs">
			<img class="img-responsive img-bd-round" width="170" style="margin: auto;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/socioeco.png">
			<h3>Acteur socio-éco</h3>
		</a>
	</div>
	
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
	bindLinkDocs();
	$("#userDoc").click(function(e) { 
		$('#acteurKick').removeClass("hide");
		$('#startSection').addClass("hide");
	 })
	
	$("#detailDoc").click(function(e) { 
		$('#container-docs').removeClass('col-xs-12').addClass('col-xs-9')
		$('#menu-left').removeClass("hide");
		
		navInDocs("docDetail", "costum.views.custom.ctenat.docs");
	 })
});
</script>