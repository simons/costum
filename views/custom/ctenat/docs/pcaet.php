<div class="col-xs-12 bg-dark">
	<h1 class="text-white">PCAET : Plan climat-air-énergie territorial</h1>
</div>

<div class="row margin-top-20  padding-20">


	<div class="col-xs-12 bgDark">
		<div class="col-xs-6 padding-20">
		<h1 class="text-center">PLAN D'ACTION</h1>
		
		Le Plan Climat Air-Énergie Territorial (PCAET), comme son prédécesseur le PCET, est un outil de planification qui a pour but d'atténuer le changement climatique, de développer les énergies renouvelables et maîtriser la consommation d'énergie. Outre le fait, qu’il impose également de traiter le volet spécifique de la qualité de l’air (Rajout du « A » dans le signe), sa particularité est sa généralisation obligatoire à l’ensemble des intercommunalités de plus de 20.000 habitants à l’horizon du 1er janvier 2019, et dès 2017 pour les intercommunalités de plus de 50.000 habitants.</div>
		<div class="col-xs-6"><img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/planClimat.png'> </div>
		</div>
	</div>

	<div class="col-xs-12 margin-top-20">
		<div class="col-xs-6"><img class="img-responsive" src='http://www.territoires-energie-positive.fr/var/res-league/storage/images/clu_euro/national_leagues/clu_fran/news/pcaet-l-ademe-publie-un-guide-complet-et-actualise/3461522-1-fre-FR/pcaet-l-ademe-publie-un-guide-complet-et-actualise_articlelarge.png'> </div>
		<div class="col-xs-6 padding-20">
		<h1 class="text-center">GET ANSWERS TO YOUR QUESTIONS</h1>
		
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
	</div>
	
	<div class="col-xs-12">
		<div class="col-xs-6 padding-20">
		<h1 class="text-center">CROWD KNOWLEDGE & COLLECTIVE INTELLIGENCE</h1>
		
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
		<div class="col-xs-6"><img class="img-responsive" src='<?php echo Yii::app()->getModule("survey")->assetsUrl; ?>/images/home/edwin-andrade-153753-unsplash.jpg'> </div>
		</div>
	</div>

	<div class="col-xs-12">
		<div class="col-xs-6"><img class="img-responsive" src='<?php echo Yii::app()->getModule("survey")->assetsUrl; ?>/images/home/glen-noble-18012-unsplash.jpg'> </div>
		<div class="col-xs-6 padding-20">
		<h1 class="text-center">PROJECT MUTUALISATION & EVALUATIONS </h1>
		
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
	</div>

	<div class="col-xs-12">
		
		<div class="col-xs-6 padding-20">
		<h1 class="text-center"> TOOLS FOR DEMOCRACY </h1>
		
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
		<div class="col-xs-6"><img class="img-responsive" src='<?php echo Yii::app()->getModule("survey")->assetsUrl; ?>/images/home/my-life-through-a-lens-110632-unsplash.jpg'> </div>
	</div>

	<div class="col-xs-12">
		<div class="col-xs-6"><img class="img-responsive" src='<?php echo Yii::app()->getModule("survey")->assetsUrl; ?>/images/home/cody-davis-253928-unsplash.jpg'> </div>
		<div class="col-xs-6 padding-20">
		<h1 class="text-center">OPEN SOURCE & LIBRE SOFTWARE </h1>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
	</div>

	<div class="col-xs-12">
		
		<div class="col-xs-6 padding-20">
		<h1 class="text-center"> BUILD PARTICIPATIVE COMMUNITIES </h1>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
		<div class="col-xs-6"><img class="img-responsive" src='<?php echo Yii::app()->getModule("survey")->assetsUrl; ?>/images/home/tim-marshall-114623-unsplash.jpg'> </div>
	</div>

	<div class="col-xs-12">
		<div class="col-xs-6"><img class="img-responsive" src='<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/logoBtn.png'> </div>
		<div class="col-xs-6 padding-20">
			<h1 class="text-center">LOVE & CO </h1>
			
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</div>
	</div>


	</div>

<!-- TODO 

//reload when on a link 
//


-->


	

</div>