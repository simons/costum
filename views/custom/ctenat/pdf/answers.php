<?php

Yii::import("parsedown.Parsedown", true);
$Parsedown = new Parsedown();
$cter = PHDB::findOne(Project::COLLECTION, array("slug" => $answer["formId"], "category" => "cteR"), array("name"));

// if( !empty($answer["answers"]) &&
// 				!empty($answer["formId"]) &&
// 				!empty($answer["answers"][$answer["formId"]]) &&
// 				!empty($answer["answers"][$answer["formId"]]["answers"]) ) {
// 				$answers = $answer["answers"][$answer["formId"]]["answers"];
			
// 	if(!empty($answers["organization"]) && !empty($answers["organization"]["id"]) ){
// 		$organization = PHDB::findOneById(Project::COLLECTION, $answers["organization"]["id"], array("name"));
// 	}
// 	if(!empty($answers["project"]) && !empty($answers["project"]["id"])  ){
// 		$project = PHDB::findOneById(Project::COLLECTION, $answers["project"]["id"], array("name"));
// 	}
// }
?>
<style type="text/css">
	
h1 {
	font-size: 24px;
}

.blue{
	color : #195391;
}

.lightgreen{
	color : #a9ce3f;
}

.darkgreen{
	color : #4db88c;
}

.body { 
	font-family: Arial,Helvetica Neue,Helvetica,sans-serif; 
}

table.first {
        color: #003300;
        font-family: helvetica;
        border: 3px solid black;
    }

table.first td {
        border: 1px solid black;
        font-size: 8pt;
    }

table.first td .entete {
         font-size: 10pt;
         font-weight: bold;
    }

</style>
<?php 

	$server = ((isset($_SERVER['HTTPS']) AND (!empty($_SERVER['HTTPS'])) AND strtolower($_SERVER['HTTPS'])!='off') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'];
	//$imgHead =  Yii::app()->createUrl(Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/city.png") ;
	$imgHead =  $server.Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/city.png" ;
?>
<div class="col-xs-12 img-band">
	<img class="img-responsive" src="<?php echo $imgHead ?>"/>
</div>
<div class="body">
	<span style="text-align: center;">
		<h1 class="blue"> CONTRAT DE TRANSITION ECOLOGIQUE</h1>
		<span class="darkgreen">Territoire de <?php echo $cter["name"]; ?></span>
		<span class="lightgreen"></span>
		<br/>
		<?php
			if(!empty($answer["project"])) { ?>
				<span class="">
					<?php 
					//echo "Action N°".$answer["project"]["id"]." : ";
					echo $answer["project"]["name"]; ?> 
				</span>
		<?php	} ?>
	</span>
	<br/><br/>
	<span style="text-align: left; font-size: 14px;">
		Rattachée à l’orientation : <?php 
			if(!empty($answer["caracter"])){
				if(gettype($answer["caracter"]["cibleDDPrincipal"]) == "array")
					echo $answer["caracter"]["cibleDDPrincipal"][0];
				else
					echo $answer["caracter"]["cibleDDPrincipal"];
			}
		?>
		<br/>
		<?php //echo "Dernière date de mise à jour : ".date("d/m/Y", $answer["created"]); ?>
	</span>
	<div class='col-xs-12'>
		<?php
		$str = "";
		$str .= '<h4 class="padding-20 blue" style="">'."Structures Porteuses".'</h4>';
		if(!empty($answer["answers"]["organizations"])){
			$str .= '<span>'.$answer["answers"]["organizations"].'</span>';
		}

		if(!empty($answer["project"]["shortDescription"])){
			
			$str .= '<h4 class="padding-20 blue" style="">'."Description courte".'</h4>';
			$str .= '<span>'.$answer["project"]["shortDescription"].'</span>';
		}
		
		if(!empty($answer["project"]["description"])){
			$str .= '<h4 class="padding-20 blue" style="">'."Description longue".'</h4>';
			$str .= '<span>'.$Parsedown->text($answer["project"]["description"]).'</span>';
		}
		
		if(!empty($answer["project"]["tags"])){
			$str .= '<h4 class="padding-20 blue" style="">'."Tags".'</h4>';
			$str .= '<span>';

			foreach ($answer["project"]["tags"] as $keyT => $valT) {
				$str.= '<span style="color :red">#'.$valT.'</span> ';
			}
			'</span>';
		}

		$str .= '<h4 class="padding-20 blue" style="">'."Attentes vis à vis du CTE".'</h4>';
		
		if(!empty($answer["project"]["expected"])){
			$str .= '<span>'.$Parsedown->text($answer["project"]["expected"]).'</span>';
		}


		echo $str ;


		$params = array(
			//"author" => @$answer["name"],
			"idAnswer"=>$id,
			"answer" => $answer,
			//"saveOption"=>"F",
			//"urlPath"=>$res["uploadDir"],
			"title" => "TEST",
			"participants" =>"partici",
			"dateComment" => "dateComment",
			"subject" => "CTE",
			//"custom" => $form["custom"],
			"footer" => true,
			"tplData" => "cteDossier",
			"form" => $form,
			"forms" => $forms,
			"canAdmin"=>null
		);
		if(isset($answer['answers'])){
			$params["answers"] = $answer['answers'];
		}
		
		echo $this->renderPartial('costum.views.custom.ctenat.pdf.murir', $params, true);

		?>
	</div>
</div>