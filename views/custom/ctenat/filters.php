<?php
//$cssAnsScriptFilesModule = array(
//	'/css/ctenat/filters.css',
//);
//HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl() );
?>
<!-- <div id="filterContainer" class="col-xs-12 col-md-10 col-md-offset-1">
</div> -->

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	filters : {
	 		domainAction : {
	 			view : "selectList",
	 			type : "tags",
	 			name : "Domaine d'action",
	 			action : "tags",
	 			list : costum.lists.domainAction
	 		},
	 		cibleDD:{
	 			view : "selectList",
	 			type : "tags",
	 			name : "Objectif",
	 			action : "tags",
	 			list : costum.lists.cibleDD
	 		},
	 		scope : {
	 			view : "scope",
	 			type : "scope",
	 			action : "scope"
	 		}
	 		/*status : {
	 			view : "selectList",
	 			type : "filters",
	 			field : "source.status.ctenat",
	 			name : "Status",
	 			action : "filters",
	 			list : [ 
                    "Territoire Candidat",
                    "Territoire Candidat refusé", 
                    "Territoire lauréat", 
                    "Dossier Territoire Complet",
                    "CTE Signé"
                ]
	 		},*/

	 	}
	 };
	 if(pageApp=="territoires"){
	 	paramsFilter.filters.status={
	 		view : "selectList",
 			type : "filters",
 			field : "source.status.ctenat",
 			name : "Statuts",
 			action : "filters",
 			list : [ 
                /*"Territoire Candidat",
                "Territoire Candidat refusé",*/ 
                "<?php echo Ctenat::STATUT_CTER_LAUREAT; ?>", 
                //"Dossier Territoire Complet",
                "<?php echo Ctenat::STATUT_CTER_SIGNE; ?>"
            ]
	 	};
	}
	function lazyFilters(time){
	  if(typeof filterObj != "undefined" )
	    filterGroup = filterObj.init(paramsFilter);
	  else
	    setTimeout(function(){
	      lazyFilters(time+200)
	    }, time);
	}

	jQuery(document).ready(function() {
		lazyFilters(0);
	});
</script>