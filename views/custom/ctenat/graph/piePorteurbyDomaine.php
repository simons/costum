<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

?>

<script type="text/javascript">
window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};


	var MONTHS = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];

	var COLORS = [
	"#0A2F62",
	"#0B2D66",
	"#064191",
	"#2C97D0",
	"#16A9B1",
	"#0AA178",
	"#74B976",
	"#0AA178",
	"#16A9B1",
	"#2A99D1",
	"#064191",
	"#0B2E68"
	];
</script>


		<div id="canvas-holder" style="margin:0px auto;width:100%">
				<canvas id="chart-area"></canvas>
		</div>
		<?php if($size!="S") { ?>
		<div style="margin:0px auto;width:80%">
			<button id="randomizeData">Randomize Data</button>
			<button id="addDataset">Add Dataset</button>
			<button id="removeDataset">Remove Dataset</button>
		</div>
		<?php } ?>
	<script>
	
		var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};
jQuery(document).ready(function() {
	mylog.log("render","/modules/graph/views/co/piePorteurbyDomaine.php");
		var config = {
			type: 'pie',
    		data: {
				datasets: [{
					data: [78,43,40,29,40,59,103],
					backgroundColor: COLORS,
				}],
				labels: [ "Services aux territoires et citoyens",
						"Economie verte et circulaire",	
						"Eau, nature et biodiversité",
						"Aménagement",	
						"Mobilités",	
						"Agriculture et alimentation",	
						"Energie"]
			},
			options: {
				responsive: true,
				
			}
		};

			var ctx = document.getElementById('chart-area').getContext('2d');
			window.myPie = new Chart(ctx, config);
		
	});
	</script>