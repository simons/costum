<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

?>



<div id="canvas-holder" style="margin:0px auto;width:100%;">
	<canvas id="chart-area"></canvas>
</div>
		
<script type="text/javascript">

var COLORS = ["#0A2F62","#0B2D66","#064191","#2C97D0","#16A9B1","#0AA178","#74B976","#0AA178","#16A9B1","#2A99D1","#064191","#0B2E68"];

jQuery(document).ready(function() {
	if(typeof pieMillionCTErrData == "undefined")
		alert("pieMillionCTErrData is undefined");
	mylog.log("render","/modules/costum/views/custom/ctenat/graph/pieMillionCTErr.php",pieMillionCTErrData);
	
	var config = {
		type: 'pie',
		data: pieMillionCTErrData
	};

	
	var ctxContainer = document.getElementById('chart-area');
	var ctx = ctxContainer.getContext('2d');
	window.openPie = new Chart(ctx, config);

	ctxContainer.onclick = function(evt) {
      var activePoints = openPie.getElementsAtEvent(evt);
      if (activePoints[0]) {
        var chartData = activePoints[0]['_chart'].config.data;
        var idx = activePoints[0]['_index'];

        var label = chartData.labels[idx];
        var value = chartData.datasets[0].data[idx];

        var classSel = slugify( label ).toLowerCase();
        $(".dashElem").removeClass("hide");
        $(".dashElem:not(."+classSel+")").addClass("hide");
        $("#dashElemTitle").html("Projets "+label +" ("+$("."+classSel).length+")")
        //alert(classSel);
      }
    };
});

</script>