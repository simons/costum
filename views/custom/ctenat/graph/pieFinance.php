<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);


?>

<div id="canvas-holder" style="margin:20px auto;width:100%">
	<canvas id="chart-area-finance"></canvas>
</div>
<?php if($size!="S") { ?>
	<div style="margin:0px auto;width:80%">
	</div>
<?php } ?>

<script>
var randomScalingFactor = function() {
	return Math.round(Math.random() * 100);
};

<?php 
	
	$cters = PHDB::find( Project::COLLECTION, ["category"=>"cteR"], ["slug"] );
	
	$financeDataAll = [];
	$financeLblsAll = [];
	$financeKeyValue = [];
	$financeTotal = 0;
	foreach ($cters as $i => $cter) 
	{
		$financeData = [];
		$financeLbls = [];
		$answersList = PHDB::find(Form::ANSWER_COLLECTION,[
			"formId"=>$cter["slug"],
			"priorisation" => ['$in'=>[ Ctenat::STATUT_ACTION_VALID,
										Ctenat::STATUT_ACTION_COMPLETED,
										Ctenat::STATUT_ACTION_CONTRACT ]]
			], ["_id","answers.".$cter["slug"].".answers.murir.planFinancement"] );
		$finance = Ctenat::chiffreFinancementByType($answersList,$cter["slug"],false);
		foreach ( $finance as $k => $v) {
			if(!isset($financeKeyValue[$k])){
                $financeKeyValue[ $k ] = $v;
            }
            else 
                $financeKeyValue[$k] += $v;
		}
	}

	foreach ($financeKeyValue as $k => $v) 
	{
		if(round(intval($v)/1000000) != 0){
	        $financeDataAll[] = round(intval($v)/1000000);
	        $financeTotal += intval($v);
	        $financeLblsAll[] = (isset(Ctenat::$financerTypeList[$k])) ? Ctenat::$financerTypeList[$k] : $k ;
	    }
    }

?>

jQuery(document).ready(function() {
	//alert("<?php echo round($financeTotal/1000000,1) ?>");
	mylog.log("render","/modules/costum/views/custom/ctenat/graph/pieFinance.php",<?php echo json_encode( $financeDataAll ) ?>,<?php echo json_encode( $financeLblsAll ) ?>);
		var config = {
			type: 'pie',
    		data: {
				datasets: [{
					data: <?php echo json_encode( $financeDataAll ) ?>,
					backgroundColor: <?php echo json_encode( Ctenat::$COLORS ) ?>,
				}],
				labels: <?php echo json_encode( $financeLblsAll ) ?>
			},
			options: {
				responsive: true,
				
			}
		};

			var ctxContainer = document.getElementById('chart-area-finance');
			var ctx = ctxContainer.getContext('2d');
			window.myPie = new Chart(ctx, config);

			ctxContainer.onclick = function(evt) {
		      var activePoints = myPie.getElementsAtEvent(evt);
		      if (activePoints[0]) {
		        var chartData = activePoints[0]['_chart'].config.data;
		        var idx = activePoints[0]['_index'];

		        var label = chartData.labels[idx];
		        var value = chartData.datasets[0].data[idx];

		        var url = "label=" + label + "&value=" + value;
		        //alert("todo sub graphs"+url);
		        smallMenu.openAjaxHTML( baseUrl+'/costum/ctenat/dashboard/slug/terrinarchy');
		      }
		    };
		
	});
	</script>