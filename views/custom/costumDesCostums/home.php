<?php 
$cssAnsScriptFilesTheme = array(
		'/plugins/jQuery/jquery-2.1.1.min.js',
		'/plugins/jQuery/jquery-1.11.1.min.js',
		'/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
        '/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js',
        '/plugins/jQuery-Knob/js/jquery.knob.js',
        '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
        //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js'            
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 
    $poiList = array();
    if(isset(Yii::app()->session["costum"]["contextType"]) && isset(Yii::app()->session["costum"]["contextId"])){
        $el = Element::getByTypeAndId(Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"] );
    
        $poiList = PHDB::find(Poi::COLLECTION, 
                        array( "parent.".Yii::app()->session["costum"]["contextId"] => array('$exists'=>1), 
                               "parent.".Yii::app()->session["costum"]["contextId"].".type"=>Yii::app()->session["costum"]["contextType"],
                               "type"=>"cms") );
    } 

    ?>
    <link href="https://fonts.googleapis.com/css?family=Covered+By+Your+Grace&display=swap" rel="stylesheet"> 
    <link href="//db.onlinewebfonts.com/c/cb18f0f01e1060a50f2a273343bb844e?family=Homestead" rel="stylesheet" type="text/css"/>
<style>
@font-face {
    font-family: "Homestead"; src: url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.eot"); src: url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.eot?#iefix") format("embedded-opentype"), url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.woff2") format("woff2"), url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.woff") format("woff"), url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.ttf") format("truetype"), url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.svg#Homestead") format("svg"); 
    font-family : "Helvetica"; src: url("<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/font/costumDesCostums/Helvetica.ttf") format("ttf");
} 
  .header{
    background-image : url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/photo.jpg);
    background-size: cover;
    padding-bottom: 49%;
}
.intro{
    background-color: #ededed;
    margin-left: 10%;
    margin-right: 10%;
    margin-top: -3%;
    padding-bottom: 3%;
}
.intro-description{
    background: white;
    text-align: center;
    padding-top: 3%;
    padding-bottom: 3%;
    color : <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
    border: solid 1px silver;
}
.projet-title{
    text-align : center;
    border-top: dashed 1.5px <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
    border-bottom:dashed 1.5px <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
    color: <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
}
.projet{
    margin-top:3%;
    margin-left: 10%;  
    margin-right: 10%;
}
.projet-affiche{
    margin-top: 3%;
    background-color:#ededed;
    box-shadow: 0px 0px 20px -2px #878786;
    padding-bottom: 3%;
}
.explication{
    margin-top:3%;
    margin-left: 10%;  
    margin-right: 10%;
}
.explication-title{
    text-align : center;
    border-top: dashed 1.5px <?php echo Yii::app()->session["costum"]["css"]["color"]["bg-green"]; ?>;
    border-bottom:dashed 1.5px <?php echo Yii::app()->session["costum"]["css"]["color"]["bg-green"]; ?>;
    color : <?php echo Yii::app()->session["costum"]["css"]["color"]["bg-green"]; ?>
}

.explication-img {
    text-align: center;
}
.tarif{
    margin-top:3%;
    background-color:#f8f9f9;

}
.tarif-title {
    text-align : center;
    border-top: dashed 1.5px <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
    border-bottom:dashed 1.5px <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
    margin-top: 5%;
    margin-bottom: 5%;
    color : <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
}
.tarif-border{
    background-color: #ededed;
    padding: 2%;
    box-shadow: 0px 0px 20px -2px #878786;
}
.tarif-c{
    border-right : dashed 1.5px black;
    text-align : center;
}
.tarif-cs{
    text-align : center;
}
.bouton{
    margin-top: 2%;
    margin-left: 10%;
    margin-right: 10%;
}
.bouton-img {
    text-align: center;
}
.tarif-c > p {
    margin-top: 5%;
    color : #878786;
    font-size: 2.25rem;
}
.tarif-cs > p {
    margin-top: 5%;
    color : #878786;
    font-size: 2.25rem;
}
.tarif-c > h2{
    color : <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
}
.tarif-cs > h2{
    color: <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>;
}
.bouton-img > span {
    background: #92bf21;
    border-radius: 20px 20px 20px 20px;
    padding: 2.3%;
    padding-left: 5%;
    padding-right: 5%;
}
.tarif-description > p {
    font-style: italic;
    text-align: center;
    margin-left: 10%;
    margin-right: 10%;
    margin-top: 3%;
    margin-bottom: 3%;
    color: #878786;
}
.btn-plus-tarif{
    margin-top: -3%;
    padding: 2%;
}
.img-projet{
    width: 100%;
    height: 75%;
}
@media (max-width:768px){
    .tarif-c > p {
        font-size: 1.25rem;
    }
    .tarif-cs > p {
        font-size: 1.25rem;
    }
    .bouton-img > span{
        font-size: 0.75rem;
        padding: 5%;
    }
    .img-projet {
        width: 110px;
        height: 50px;
    }
    .projet-mobile{
        font-size: 1rem;
        line-height: 9px;
    }
}
</style>

<div class="header">
<center class="col-xs-12 col-sm-12">
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/header.png" class="img-responsive" style="margin-top: 3%; width:80%">
</center>
</div>

<div class="intro row">
    <div style="margin-top: 3%; margin-bottom: 0%; margin-left: 3%; margin-right: 3%;">
        <div class="intro-description col-xs-12 col-sm-12">
            <h1 style="font-family: Homestead !important;">En attente d'une phrase ici</h1>
        </div>
    </div>
</div>

<div class="projet row">
    <div class="projet-title col-xs-12 col-sm-12">
    <h1 style="font-family: 'Covered By Your Grace', cursive !important;">Nos exemples de costum</h1>
    </div>

    <div class="projet-affiche col-xs-12 col-sm-12">
        <div class="col-xs-4 col-sm-4 bouton-img" style="text-align : center; padding: 2%; font-size: 1.5vw;" >
            <span>Nos derniers costums</span>
        </div>
        <div class="col-xs-4 col-sm-4 bouton-img" style="text-align : center; padding: 2%; font-size: 1.5vw;">
            <span>Les best</span>
        </div>
        <div class="col-xs-4 col-sm-4 bouton-img" style="text-align : center; padding: 2%; font-size: 1.5vw;" >
            <select>
                <option value="last">Par Type</option>
                <option value="NGO">Association</option>
                <option value="LocalBusiness">Entreprise</option>
                <option value="Group">Groupe</option>
                <option value="GovernmentOrganization">Service Public</option>
            </select>
        </div>


        <div class="col-xs-12 col-sm-12">
        <?php 
        // $costums = array_merge(PHDB::findAndSortAndLimitAndIndex("costum",array(),array("_id" => -1),1),PHDB::findAndLimitAndIndex("costum",array(),2));
        $costums = PHDB::findAndSortAndLimitAndIndex("costum",array(),array("_id" => -1),3);

        $html="";
        foreach($costums as $key=> $v){
            if(@$v["slug"]){
            $el = Slug::getElementBySlug($v["slug"], ["shortDescription", "profilImageUrl", "profilThumbImageUrl", "profilMediumImageUrl", "name", "tags", "description"]);
            $el=$el["el"];
            $img=$this->module->getParentAssetsUrl()."/images/wave.jpg";
            if (@$v["metaImg"]) 
                $img = Yii::app()->getModule( $this->module->id )->getAssetsUrl().$v["metaImg"];
            else if(!empty($el["profilMediumImageUrl"]))
                $img = Yii::app()->createUrl($el["profilImageUrl"]);
            $html.="<div class='col-xs-4 col-sm-4' style='height:375px;overflow-y:auto; background-color: white;border: solid 3px #ededed;'>".
                    "<img src='".$img."' class='img-responsive img-projet'/>".
                    "<a href='".Yii::app()->createUrl("/costum/co/index/id/".$v["slug"])."' target='_blank'>".
                        "<h3>".$el["name"]."</h3>".
                        "</a>".
                        "<span class='projet-mobile'>".@$el["shortDescription"]."</span>".
                        "<div class='projet-mobile letter-red'>";
                        if(!empty($el["tags"])){
                            foreach($el["tags"] as $v){
                                $html.="#".$v;
                            }
                        }
                        $html.="</div></div>";
                }
        }
        echo $html;
    ?>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12" style="text-align:center; margin-top: 2%;">
    <center>
    <a href="https://www.communecter.org/costum" target="_blank">
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/plus.svg" class="img-responsive" style="width: 5%">
    </a>
    </center>
    </div>
</div>

<div class="explication row">
    <div class="explication-title col-xs-12 col-sm-12">
        <h1 style="font-family: 'Covered By Your Grace', cursive !important;">Explication</h1>
    </div>
    <div class="col-xs-12 margin-top-20">
                    <?php 
                    $params = array(
                        "poiList"=>$poiList,
                        "listSteps" => array("1","2","3","4"),
                        "el" => $el,
                        "color1" => Yii::app()->session["costum"]["css"]["color"]["bg-green"]
                    );
                    echo $this->renderPartial("costum.views.tpls.wizard",$params); ?>
    </div> 
    <div class="explication-img col-xs-12 col-sm-12">
            <center> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/illu.svg" class="img-responsive" style="width: 60%;"></center>
            <h1 style="color : <?php echo Yii::app()->session["costum"]["css"]["color"]["blue"]; ?>; font-family: 'Covered By Your Grace', cursive !important; margin-top: 4%;">
            « Contribuer aux communs avec un contexte personnalisé. »
            </h1>
    </div>
</div>

<div class="tarif row">
    <div style="margin-left: 10%;  margin-right: 10%;">
        <div class="tarif-title col-xs-12 col-sm-12">
        <h1 style="font-family: 'Covered By Your Grace', cursive !important;">Tarifs</h1>
        </div>
    </div>
    <!-- Les tarifs -->
    <div style="margin-left: 2%; margin-right: 2%;">
        <div class="tarif-border col-xs-12 col-sm-12">
                <div class="tarif-c col-xs-3 col-sm-3">
                <h1>2000 €</h1>
                <p> Association - <br> de 50 <br> adhérents </p>
                </div>
                <div class="tarif-c col-xs-3 col-sm-3">
                <h1>2800 €</h1>
                <p> Association + <br> de 50 <br> adhérents </p>
                </div>
                <div class="tarif-c col-xs-3 col-sm-3">
                <h1>3200 €</h1>
                <p> Coopératives <br> et entreprises <br> responsable </p>
                </div>
                <div class="tarif-cs col-xs-3 col-sm-3">
                <h1>Devis</h1>
                <p> Collectivé <br> et autres <br>entreprise </p>
                </div>
        </div>
        </div>
        <div class="btn-plus-tarif col-xs-12 col-sm-12">
                <div class="col-xs-3 col-sm-3">
                    <center>
                        <a href="#" data-toggle="modal" data-target="#descriptionTarif">
                            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/plus.svg" class="img-responsive" style="width: 10%">
                        </a>
                    </center> 
                </div>
                <div class="col-xs-3 col-sm-3">
                    <center>
                        <a href="#" onclick="changeModal('first')" data-toggle="modal" data-target="#descriptionTarif">
                            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/plus.svg" class="img-responsive" style="width: 10%">
                        </a>
                    </center>
                </div>
                <div class="col-xs-3 col-sm-3">
                    <center>
                        <a href="#" data-toggle="modal" data-target="#descriptionTarif">
                            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/plus.svg" class="img-responsive" style="width: 10%">
                        </a>
                    </center>
                </div>
                <div class="col-xs-3 col-sm-3">
                    <center>
                        <a href="#" data-toggle="modal" data-target="#descriptionTarif">
                            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/plus.svg" class="img-responsive" style="width: 10%">
                        </a>
                    </center>
                </div>
        </div>

    <div class="tarif-description col-xs-12 col-sm-12">
   <p style="font-style: italic; text-align: center; margin-left: 10%; margin-right: 10%; margin-top: 3%; margin-bottom: 3%; color: #878786;">
       Nous sommes soucieux de proposer un tarif juste et équitable.
       Nous le fixons en fonction de la taille de votre organisation et vos besoins de personnalisations. <br />
       Nous privilégions le système d’abonnement mensuel. Si ce n’est pas possible merci de nous contacter. <br />
       Le prix fixe correspond à la mise en place du site, et le coût mensuel à la participation au commun.

   </p>
    </div>
</div>

<div class="bouton row">
        <div class="col-xs-12 col-sm-12">
            <div class="bouton-img col-xs-4 col-sm-4">
            <span>Vous avez une idée de costum</span>
            </div>
            <div class="bouton-img col-xs-4 col-sm-4">
            <span>Votre costum en un clic</span>
            </div>
            <div class="bouton-img col-xs-4 col-sm-4">
            <span><a href="https://doc.co.tools/books/2---utiliser-loutil/page/costum" target="_blank" style="text-decoration : none;"> Documentation</a></span>
            </div>
        </div>
</div>


<!-- Modal -->
<div class="modal fade" id="descriptionTarif" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="background-color: rgba(0, 0, 0, 0.5);">
  <div class="modal-dialog modal-lg" role="document" style="margin-top: 8%;">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">Les tarifs</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -4%">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p> La participation mensuelle permet d’améliorer le commun et donne droit à l’assistance mail et chat. </p>
       <p>
Le prix fixe comprend : <br />
<ul>
    <li>Personnalisation d’une de nos maquettes : ajout du logo, couleurs et textes</li>
    <li>Choix des fonctionnalités (<a href="https://doc.co.tools/books/2---utiliser-loutil/page/liste" target="_blank">voir la liste</a>)</li>
    <li>500 Mo d’espace de stockage</li>
    <li><b>Formation de 3 heures à la prise en main de l’outil</b></li>
</ul>
</p>
<p>
    Il est bien sûr possible d’aller plus loin (devis sur simple demande) : <br />
    <ul>
        <li>Importer des données</li>
        <li>Organiser un événement convivial de référencement collectif (cartopartie)</li>
        <li>Créer une maquette personnalisée</li>
        <li>Intégrer votre maquette réalisée chez un autre prestataire</li>
        <li>Créer une nouvelle fonctionnalité</li>
        <li>Organiser un concours</li>
    </ul>
</p>
      </div>
    </div>
  </div>
</div>




<?php
// $allCostum = PHDB::find("costum");
// // var_dump($allCostum); exit;
// $max = array();
// foreach($allCostum as $k => $va){
// if(@$va["slug"]){
// $c = 0;
//             $results=array(
//             "co2-login"=>1, 
//             "co2-web"=>1, 
//             "co2-websearch"=>1,
//             "co2-referencement"=>0,
//             "co2-page"=>0,
//             "co2-dashboard"=>0,
//             "co2-search"=>1,
//             "co2-dda"=>1,
//             "co2-live"=>0,
//             "co2-info"=>0,
//             "co2-annonces"=>0,
//             "co2-agenda"=>0,
//             "co2-welcome"=>1,
//             "co2-admin"=>0
//         );


//         $stat = PHDB::find(CO2Stat::COLLECTION, array("sourceKey"=>$va["slug"]));
//         foreach($stat as $y => $v){
//             //var_dump($v); exit;
//             foreach($v["hash"] as $key => $days ){
//                 foreach($days as $ok=> $kk){
//                     if(@$results[$key]){
//                         $results[$key]=($results[$key]+$kk["nbLoad"]);
//                         $c += $kk["nbLoad"];
//                     }
//                 }

//             }
//         }
//         $allCostum[$k]["stat"] = $c;
//         // arsort($allCostum[$k]["stat"]);
//         // $max[$va["slug"]] = $c;
//     }  
// }
// var_dump($allCostum);
?>

<script>
 jQuery(document).ready(function() {
    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });

    console.log("------------- console", baseUrl+'/<?php  echo $this->module->id;?>/default/doc');
});

 // function changeModal(a){
 //    console.log("--------------------- changeModal");
 //    console.log(costum.tarif.+a);
 // }

</script>