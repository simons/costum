<?php 
$cssAnsScriptFilesTheme = array(

    );
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

$articles_une=Poi::getPoiByWhereSortAndLimit(array("rank"=>"true", "source.key"=>"nicolas"),array("updated"=>-1), 3, 0);
?>




<div id="colorlib-page">
		<div class="container-wrap">
		<a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
		<aside id="colorlib-aside" role="complementary" class="border js-fullheight">
			<div class="text-center">
				<div class="author-img" style="background-image: url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/nicolas/nico.jpg);"></div>
				<h1 id="colorlib-logo"><a href="#">Nicolas Nambinintsoa</a></h1>
			</div>
			<nav id="colorlib-main-menu" role="navigation" class="navbar">
				<div id="navbar" class="collapse">
					<ul>
						<li><a href="#" data-nav-section="contact">Profil</a></li>
						<li><a href="#" data-nav-section="education">Formation</a></li>
						<li><a href="#" data-nav-section="experience">Experience</a></li>
						<li><a href="#" data-nav-section="services">Competence</a></li>
						<li><a href="#" data-nav-section="skills">Langue</a></li>
						<li><a href="#" data-nav-section="about">Loisir</a></li>

					</ul>
				</div>
			</nav>

			<div class="colorlib-footer">
				<!--<p><small>&copy;
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script>  by <a href="#" target="_blank">Nicoss</a>
                </small></p>-->

			</div>

		</aside>

		<div id="colorlib-main">

			<section class="colorlib-contact" data-section="contact">
				<div class="colorlib-narrow-content">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
							<h2 class="colorlib-heading">Profil</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="colorlib-feature colorlib-feature-sm animate-box" data-animate-effect="fadeInLeft">
								<div class="colorlib-icon">
									<i class="fa fa-user"></i>
								</div>
								<div class="colorlib-text">
									<p><strong>Nom et Prénom :</strong> <br>NAMBININTSOA Nicolas</p>
								</div>
							</div>

							<div class="colorlib-feature colorlib-feature-sm animate-box" data-animate-effect="fadeInLeft">
								<div class="colorlib-icon">
									<i class="fa fa-calendar"></i>
								</div>
								<div class="colorlib-text">
									<p><strong>Né le :</strong> <br>21 Novembre 1994</p>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="colorlib-feature colorlib-feature-sm animate-box" data-animate-effect="fadeInLeft">
								<div class="colorlib-icon">
									<i class="fa fa-phone"></i>
								</div>
								<div class="colorlib-text">
									<p><strong>Téléphone :</strong> <br><a href="tel://">+261 34 25 363 35</a></p>
								</div>
						</div>
							<div class="colorlib-feature colorlib-feature-sm animate-box" data-animate-effect="fadeInLeft">
								<div class="colorlib-icon">
									<i class="fa fa-at"></i>
								</div>
								<div class="colorlib-text">
									<p><strong>Email :</strong> <br><a href="#">nambinintsoanicolas21@gmail.com</a></p>
								</div>
							</div>
					</div>

						<div class="col-md-12 text-center">
							<div class="colorlib-feature colorlib-feature-sm animate-box " data-animate-effect="fadeInLeft">
								<div class="colorlib-icon">
									<i class="fa fa-map-marker"></i>
								</div>
								<div class="colorlib-text">
									<p><strong>Adrésse :</strong> Lot 0307/530 Soanierana Fianarantsoa</p>
								</div>
							</div>
						</div>

				</div>
				</div>
			</section>

			<section class="colorlib-education" data-section="education">
				<div class="colorlib-narrow-content">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
							<span class="heading-meta">Education</span>
							<h2 class="colorlib-heading animate-box">FORMATION ET DIPLOMES</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 animate-box" data-animate-effect="fadeInLeft">
							<div class="fancy-collapse-panel">
								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
									<div class="panel panel-default">
										<div class="panel-heading" role="tab" id="headingOne">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Master en Informatique
												</a>
											</h4>
										</div>
										<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
											<div class="panel-body">
												<div class="row">
													<div class="col-md-6">
														<ul>
															<li><strong>2018 - Maintenant : </strong></li>
														</ul>
														<p>Etudier à l’Ecole de Management et d’Innovation
															Technologique (<a href="www.emit.mg" target="_blank">EMIT</a>) de l’Université de Fianarantsoa (UF).
															Preparation du diplôme de master en Informatique. </p>
													</div>
													<div class="col-md-6">
														<p><u>Spécialité</u> : Modélisation et Ingénierie Informatique (M2I).</p>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-heading" role="tab" id="headingTwo">
											<h4 class="panel-title">
												<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Licence en Informatique
												</a>
											</h4>
										</div>
										<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
											<div class="panel-body">
												<li><strong>2015 -2017 : </strong></li>
												<p>Etudier à l’Ecole de Management et d’Innovation Technologique de l’Université de Fianarantsoa. Obtention de diplôme de licence professionnelle en Développement d’Application Internet/Intranet (DA2I).</p>
											</div>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-heading" role="tab" id="headingThree">
											<h4 class="panel-title">
												<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Terminale D
												</a>
											</h4>
										</div>
										<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
											<div class="panel-body">
												<li><strong>2013 – 2014 : </strong></li>
												<p>Terminale au Lycée Raherivelo Ramamonjy - Obtention du Baccalauréat série D.</p>
											</div>
										</div>
									</div>


								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="colorlib-experience" data-section="experience">
				<div class="colorlib-narrow-content">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
							<h2 class="colorlib-heading animate-box">Experience</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="timeline-centered">
								<article class="timeline-entry animate-box" data-animate-effect="fadeInLeft">
									<div class="timeline-entry-inner">

										<div class="timeline-icon color-1">
											<i class="fa fa-pencil"></i>
										</div>

										<div class="timeline-label">
											<h2><a href="#">ITDC</a> <span>2018-2019</span></h2>
											<p>Développement d’une application web de gestion de risque et catastrophe de la Bureau Nationale de Gestion de Risque et Catastrophe (BNGRC) au sein de l’ITDC Mada..</p>
										</div>
									</div>
								</article>


								<article class="timeline-entry animate-box" data-animate-effect="fadeInRight">
									<div class="timeline-entry-inner">
										<div class="timeline-icon color-2">
											<i class="fa fa-pencil"></i>
										</div>
										<div class="timeline-label">
											<h2><a href="#">Preparation du mini-mémoire</a> <span>Février 2019</span></h2>
											<p>Mini-mémoire en vue de passage de 2 eme année de cycle MASTER.</br>
												Durée : 2 mois</br>
												Sujet traité : Réseaux de neurones appliqués à la gestion de l’état civile..
											</p>
										</div>
									</div>
								</article>

								<article class="timeline-entry animate-box" data-animate-effect="fadeInLeft">
									<div class="timeline-entry-inner">
										<div class="timeline-icon color-3">
											<i class="fa fa-pencil"></i>
										</div>
										<div class="timeline-label">
											<h2><a href="#">STAGE</a> <span>Novembre 2017</span></h2>
											<p>Stage au sein de l’ITDC Mada </br>
												Durée de stage : 4 mois </br>
												Sujet traité : Conception et réalisation du front office d’une plateforme de ressources humaines.
											</p>
										</div>
									</div>
								</article>

								<article class="timeline-entry animate-box" data-animate-effect="fadeInTop">
									<div class="timeline-entry-inner">
										<div class="timeline-icon color-4">
											<i class="fa fa-pencil"></i>
										</div>
										<div class="timeline-label">
											<h2><a href="#">STAGE</a> <span>Décembre 2016</span></h2>
											<p>Stage au sein de la direction régionale des impôts, Service regional des entreprise Fianarantsoa.<br>
												Durée de stage : 3 mois<br>
												Sujet traité : Mise en place d’une application de gestion des Pièces Comptables
											</p>
										</div>
									</div>
								</article>



								<article class="timeline-entry begin animate-box" data-animate-effect="fadeInBottom">
									<div class="timeline-entry-inner">
										<div class="timeline-icon color-none">
										</div>
									</div>
								</article>
							</div>
							<p class="animate-box" data-animate-effect="fadeInTop"><strong>AUTRES EXPERIENCES</strong> <br>
								Novembre 2018 : Forum sur le métier du numérique (Université de Fianarantsoa) Concours de développement d’application mobile (Android).
							</p>

						</div>
					</div>
				</div>
			</section>
			
			<section class="colorlib-services" data-section="services">
				<div class="colorlib-narrow-content">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
							<h2 class="colorlib-heading">COMPETENCE</h2>
						</div>
					</div>
					<div class="row row-pt-md">

                        <div class="col-md-4 text-center animate-box">
                            <div class="services color-5">
								<span class="icon">
									<i class="fa fa-briefcase"></i>
								</span>
                                <div class="desc">
                                    <h3>Gestion de projet</h3>
                                    <p>Méthodes agiles <br>.</p>
                                </div>
                            </div>
                        </div>
						<div class="col-md-4 text-center animate-box">
							<div class="services color-2">
								<span class="icon">
									<i class="fa fa-database"></i>
								</span>
								<div class="desc">
									<h3>SGBD</h3>
									<p>MS Access, MySQL, PostgreSQL</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 text-center animate-box">
							<div class="services color-3">
								<span class="icon">
									<i class="fa fa-mobile-phone"></i>
								</span>
								<div class="desc">
									<h3>Mobile</h3>
									<p>Android<br>.</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 text-center animate-box">
							<div class="services color-4">
								<span class="icon">
									<i class="fa fa-code"></i>
								</span>
								<div class="desc">
									<h3>Langages des programmations</h3>
									<p>C, C++, Java, Visual Basic, PHP, JavaScript, JSP, Python</p>
								</div>
							</div>
						</div>
                        <div class="col-md-4 text-center animate-box">
                            <div class="services color-1">
								<span class="icon">
									<i class="fa fa-desktop"></i>
								</span>
                                <div class="desc">
                                    <h3>Outils bureautique</h3>
                                    <p>Word, Excel, Power point, Access<br>.</p>
                                </div>
                            </div>
                        </div>
						<div class="col-md-4 text-center animate-box">
							<div class="services color-6">
								<span class="icon">
									<i class="fa fa-lightbulb-o"></i>
								</span>
								<div class="desc">
									<h3>Autres connaissances</h3>
									<p>Recherche opérationnelle<br>.<br></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			


			<section class="colorlib-skills" data-section="skills">
				<div class="colorlib-narrow-content">
					<div class="row">
						<div class="col-md-7 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
							<span class="heading-meta">Langue</span>
							<h2 class="colorlib-heading animate-box">CONNAISSANCES LINGUISTIQUES</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 animate-box" data-animate-effect="fadeInLeft">
							<!-- <p></p> -->
						</div>
						<div class="col-md-6 animate-box" data-animate-effect="fadeInLeft">
							<div class="progress-wrap">
								<h3>Francais</h3>
								<div class="progress">
								 	<div class="progress-bar color-1" role="progressbar" aria-valuenow="75"
								  	aria-valuemin="0" aria-valuemax="100" style="width:75%">
								    <span>75%</span>
								  	</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 animate-box" data-animate-effect="fadeInRight">
							<div class="progress-wrap">
								<h3>Anglais</h3>
								<div class="progress">
								 	<div class="progress-bar color-2" role="progressbar" aria-valuenow="50"
								  	aria-valuemin="0" aria-valuemax="100" style="width:50%">
								    <span>50%</span>
								  	</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="colorlib-about" data-section="about">
				<div class="colorlib-narrow-content">
					<div class="row">
						<div class="col-md-12">
							<div class="row row-bottom-padded-sm animate-box" data-animate-effect="fadeInLeft">
								<div class="col-md-12">
									<div class="about-desc">
										<h2 class="colorlib-heading">Loisirs/ centres d’intérêts</h2>
										</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 animate-box" data-animate-effect="fadeInLeft">
									<div class="services color-1">
										<span class="icon2"><i class="fa fa-soccer-ball-o"></i></span>
										<h3>Basket Ball</h3>
									</div>
								</div>
								<div class="col-md-4 animate-box" data-animate-effect="fadeInRight">
									<div class="services color-2">
										<span class="icon2"><i class="fa fa-bus"></i></span>
										<h3>Voyage</h3>
									</div>
								</div>
								<div class="col-md-4 animate-box" data-animate-effect="fadeInTop">
									<div class="services color-3">
										<span class="icon2"><i class="fa fa-book"></i></span>
										<h3>Lecture</h3>
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
			</section>





		</div><!-- end:colorlib-main -->
	</div>
	</div>

