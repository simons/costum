
<div class="pageContent">


    <style type="text/css">
      #customHeader{
        margin-top: 0px;
      }
      #costumBanner{
       /* max-height: 375px; */
      }
      #costumBanner h1{
        position: absolute;
        color: white;
        background-color: rgba(0,0,0,0.4);
        font-size: 29px;
        bottom: 0px;
        padding: 20px;
      }
      #costumBanner h1 span{
        color: #eeeeee;
        font-style: italic;
      }
      #costumBanner img{
        min-width: 100%;
      }
      .btn-main-menu{
        background: #3595a8;
        border-radius: 20px;
        padding: 20px !important;
        color: white;
        cursor: pointer;
        border:3px solid transparent;
        /*min-height:100px;*/
      }
      .btn-main-menu:hover{
        border:2px solid #3595a8;
        background-color: white;
        color: #1b7baf;
      }
      .ourvalues img{
        height:70px;
      }
      .main-title{
        color: #3595a8;
      }

      .ourvalues h3{
        font-size: 25px;
      }
      .box-register label.letter-black{
        margin-bottom:3px;
        font-size: 13px;
      }

       .participate {
    background-color: white;
      }
     
       .participate i{
      
    float: left;
    margin-right: 20px;
    color: #ef5b2b ;

      }

      .participate-content{
        font-size: 17px;
      }

    h2 p {
      text-align: left;
      line-height: 1.5em;
       margin-bottom: 1.5em;
    }

    #signature div {
      text-align: right;
    }
     

      @media screen and (min-width: 450px) and (max-width: 1024px) {
        .logoDescription{
          width: 60%;
          margin:auto;
        }
      }

      @media (max-width: 1024px){
        #customHeader{
          margin-top: -1px;
        }
      }
      @media (max-width: 768px){

      }
    </style>

    <div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
      <div id="costumBanner" class="col-xs-12 col-sm-12 col-md-12 no-padding">
        <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/tepozEnComun/banner.jpeg'> 
      </div>
    </div>  
      <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
         style=" position:relative;">-->
    <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="max-width:100%; float:left;">
        
          
      <div class="col-md-12 col-sm-12 col-xs-12 " style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="background-color: #fff;font-size: 14px;z-index: 5;">
              <div class="col-md-12 col-sm-12 col-xs-12 padding-20 participate" style="padding-left:100px; margin-top:0px;">
                  <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 text-justify" style="margin-top: -19px;margin-bottom: -18px;font-size: 14px;z-index: 5;">
                    <h2 class="col-xs-12 text-center" style="/*font-family:'Pacifico', Helvetica, sans-serif;*/color: #00a5a5;   font-size: 30px">
                      <small style="color: #333;text-align:justify !important;">
                      <p>Tepoz en Común es una iniciativa de ciudadanos que nos planteamos otra forma de relacionarnos para revitalizar nuestra comunidad y construir acción colectiva en torno al cuidado de Tepoztlán, nuestros elementos comunes y sus problemáticas.</p>
                      <p>Somos una red social ciudadana libre. Un tejido de colectivos horizontal, autónomo, distribuido, transparente; desde el territorio y en nuestra propia escala. Nos proponemos articularnos en red y compartir propósitos comunes a partir de un profundo vínculo con nuestros territorios.</p>
                      <p>La red la conformamos personas, organizaciones, iniciativas y proyectos existentes. Así compartimos una agenda común en torno a nuestros intereses, esfuerzos y propósitos; socializamos información relevante; generamos espacios colaborativos para debatir y construir propuestas; contamos con un flujo de noticias comunes donde cada una de nosotras y nosotros somos libres de generar comunicación según nuestras necesidades; y construimos inteligencia territorial al mapear e interconectar nuestros territorios físicos y simbólicos. </p>
                      <p>Facilitamos herramientas ciudadanas, libres y gratuitas para una comunidad autogestiva y eficiente, accionando mediante un común digital puesto al servicio de la inteligencia colectiva, en la que el conocimiento que construimos es de la comunidad y sus productos y resultados un bien común.</p>
                      <div id="signature">
                        <div>T E P O Z E N C O M U N . O R G</div>
                        <div><em>¡Actuar y pensar juntxs en territorio!</em></div>
                      </div>  
                      </small>   
                    </h2><br/>
                  </div> 
              </div>
          </div>

        
        </div>
    
    </div>

</div>
