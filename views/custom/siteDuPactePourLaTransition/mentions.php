    <style type="text/css">
    	.text-explain{
    		font-size: 22px;
    	}
    </style>
<div id="sub-doc-page">
    <div class="col-xs-12 support-section section-home col-md-10 col-md-offset-1">
        <div class="col-xs-12 header-section">
        	<h3 class="title-section col-sm-8">Mentions légales</h3>
        	<hr>
        </div>
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12">Éditeur du site :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		Le Collectif pour une Transition Citoyenne<br/>
        		14 Passage Dubail, 75010 Paris<br/>
        		Structure juridique : Association loi 1901<br/>
        		Président : Julien Noé<br/>
        		<a href="https://transition-citoyenne.org/statuts-du-ctc/" target="_blank">
          			-> Consulter ici les statuts de l’association
          		</a>
    		</span>
        </div> 
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12">Hébergement :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		le site est hébergé chez Easter-eggs – <a href="https://www.easter-eggs.com" target="_blank">https://www.easter-eggs.com</a>
    		</span>
        </div>
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12">Protection des données personnelles :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		Les informations collectées nous permettront de mieux vous connaître. Elles seront utilisées pour vous informer sur le Pacte pour la Transition et les actions du Collectif pour une Transition Citoyenne.<br/><br/> Conformément aux dispositions de la loi Informatique et Libertés du 6 Janvier 1978, vous disposez d’un droit d’accès, de modification, de rectification et de suppression des données qui vous concernent. Pour exercer ce droit, adressez-vous à : Collectif pour une Transition Citoyenne, 14 Passage Dubail, 75010 Paris
    		</span>
        </div>
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12">Propriété intellectuelle :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		Les contenus de ce site sont, en dehors des photos, soumis à la licence Creative Commons BY-SA. Pour améliorer l’accès à la culture et à la connaissance libres, le Collectif pour une Transition Citoyenne autorise la redistribution et réutilisation libre de ses contenus pour n’importe quel usage, y compris à des fins commerciales. Une telle utilisation n’est autorisée que si l’auteur est précisé, et la liberté de réutilisation et de redistribution s’applique également à tout travail dérivé de ces contributions.
    		</span>
        </div>
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12"> Responsabilité quant au contenu :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		Alors même que l’association Transition Citoyenne s’efforce que le contenu de son site soit le plus à jour et le plus fiable possible, il ne peut donner aucune garantie quant à l’ensemble des informations présentes sur le site, qu’elles soient fournies par ses soins, par ses partenaires ou par tout tiers, par l’envoi d’e-mails ou de toute autre forme de communication.<br/><br/>
        		Compte tenu des caractéristiques du réseau Internet que sont la libre captation des informations diffusées et la difficulté, voire l’impossibilité, de contrôler l’utilisation qui pourrait en être faite par des tiers, l’association Transition Citoyenne n’apporte aucune garantie quant à l’utilisation des dites informations.<br/><br/>
        		L’association Transition Citoyennene saurait en conséquence être tenue pour responsable du préjudice ou dommage pouvant résulter de l’utilisation des informations présentes sur le site, ni d’erreurs ou omissions dans celles-ci.<br/><br/>
        		L’association Transition Citoyenne décline toute responsabilité en ce qui concerne les contenus des sites web édités par des tiers et accessibles depuis <a href="https://www.pacte-transition.org">www.pacte-transition.org</a> par des liens hypertextes ou répertoriés dans la cartographie du site.
    		</span>
        </div> 
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12"> Crédits :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		Le site <a href="https://www.pacte-transition.org">https://www.pacte-transition.org</a> a été développé par le collectif Pixel Humain sur la base du réseau <a href="https://www.communecter.org">communecter.org</a> . <br/><br/>Les photos de ce site ont été fournies gracieusement par Edouard Marchal. Pour toute réutilisation, merci de le contacter.
    		</span>
        </div> 
    </div>
</div>
