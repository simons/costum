<style type="text/css">
	.ressourcePacte .text-explain{
		line-height: 20px;
	    max-height: inherit;
	    /*white-space: pre-line;*/
	    font-size: 16px;
	    white-space: pre-line;
    word-wrap: break-word;
	}
	.ressourcePacte{
		padding-left: 20px 10px;
		margin-bottom: 25px;
	}
	.ressourcePacte:hover{
		background-color: rgba(211, 211, 211, 0.2);
	}
	.ressourcePacte h3{
		font-size: 20px !important;
	    color: #5b2649 !important;
	    line-height: 25px;
	}
	/*.ressourcePacte .link-files{
		color: white !important;
	    background: #fbae55;
	    font-size: 16px;
		    display: inline-block;
	    padding: 3px 7px;
	    font-weight: 700;
	    color: #fff;
	    text-align: center;
	    white-space: nowrap;
	    vertical-align: middle;
	    border-radius: 10px;
	    margin-bottom: 5px;
	}*/
	.header-section hr{
		margin-left: 0px !important;
	}
</style>
<?php $ressources = PHDB::findAndSort(Poi::COLLECTION, array("category"=>$page, "source.key"=>"siteDuPactePourLaTransition"), array("updated"=>-1)); 

	foreach ($ressources as $key => $value) {
		$where=array("id"=>(string)$value["_id"], "type"=>Poi::COLLECTION, "doctype"=>"file");
	  	$ressources[$key]["files"] = Document::getListDocumentsWhere($where,"file");
	}
	$arrayReference=array(
		"organiser"=>array(
			"name"=>"S'organiser"
		),
		"communiquer"=>array(
			"name"=>"Communiquer"
		),
		"documenter"=>array(
			"name"=>"Se documenter"
		)
	);
?>
<div id="contentRessources" class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 margin-top-10">
	<div class="col-xs-12 no-padding">
		<a href="#ressources" class="lbh btn-main-menu text-purple pull-left" style="font-size:16px;">
	         <i class="fa fa-angle-left"></i> Retour à la page Ressources 
	    </a>
	    <?php if(Authorisation::isInterfaceAdmin()){ ?>
		<a href="javascript:;" data-form-type="doc" class="btn-open-form text-purple pull-right" style="font-size:16px;">
	         <i class="fa fa-plus-circle"></i> Ajouter une ressource (admin)
	    </a>
	    <?php } ?>
    <div>
	<div class="col-xs-12 support-section section-home no-padding">
        <div class="col-xs-12 header-section no-padding margin-bottom-20">
        	<h3 class="title-section col-xs-12 no-padding"> <?php echo $arrayReference[$page]["name"] ?></h3>
        	<hr>
        </div>
        <div class="col-xs-12 no-padding">
        	<?php foreach ($ressources as $key => $v){ ?>
        		<div class="ressourcePacte col-xs-12 shadow2">
        			<a href="#page.type.poi.id.<?php echo $key ?>" class="lbh-preview-element">
	        			<h3 class="sub-header-section col-xs-12 no-padding"><?php echo $v["name"] ?></h3>
	        		</a>
		        </div>
	        <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
	coInterface.bindLBHLinks();
	//coInterface.bindButtonOpenForm();
	//directory.bindBtnElement();
	/*$(".ressourcePacte .text-explain").each(function(){
		descHtml = dataHelper.markdownToHtml($(this).text());
	  	$(this).html(descHtml);
	});*/
	//$(".btn-edit-doc").click(function(){
		//key=(typeof costum != "undefined" && typeof costum.typeObj != "undefined"
			//&& typeof costum.typeObj[poiAlone.typePoi] != "undefined") ? poiAlone.typePoi : "poi";
	//	dyFObj.editElement("poi", $(this).data("id"), "doc" );
	//});
</script>