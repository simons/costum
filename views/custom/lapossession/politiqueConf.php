<div style="margin-top: 2%;" id="container-docs" class="col-md-offset-1 col-md-9 col-sm-12 col-xs-12 no-padding text-center"><div class="panel-heading border-light center text-dark partition-white radius-10" style="background-color: #089238">
	<span class="panel-title"> <i style="color: white" class="fa fa-gavel  faa-pulse animated fa-3x  "></i> <span style="font-size: 48px;color: white">VIE PRIVÉE – COOKIES</span></span>
</div>

<div class="space20"></div>

<style>
.col-md-offset-1 {margin-left: 12.333%;}
</style>
<div style="margin-top:40px">

  	<h2 class="homestead explainTitle radius-10" style="color:white;background-color: #089238"> Données personnelles </h2>
  	<div class="explainDesc">
	    <ul style="font-size: 18px;">
	      	Open atlas s'engage à respecter votre vie privée et à protéger les
			informations que vous lui communiquez. Vous trouverez ci-dessous la présentation
			des règles applicables au traitement des données à caractère personnel dans le cadre du site.
	    </ul>
  	</div>

  	<h2 class="homestead explainTitle radius-10" style="padding: 1% 0% 0% 1%;color:white;background-color: #089238"> Informations collectées </h2>
  	<div class="explainDesc">
	    <ul style="font-size: 18px;">
	     	En général, vous pouvez visiter le Site sans nous dire qui vous êtes ni nous donner d'informations personnelles. Cependant, lorsque vous vous rendez sur le Site, notre serveur web enregistre automatiquement certaines informations telles que vos identifiants et d'autres informations telles que le nom de votre fournisseur de service Internet, votre système de navigation, votre système d'exploitation et la langue utilisée, le site à partir duquel vous vous connectez, les mots clés et les technologies ajoutées au système de navigation disponibles pour l'affichage du contenu, les pages du Site que vous consultez et l'ordre de consultation, ainsi que la date et la durée de votre visite.   Par ailleurs, nous sommes susceptibles de collecter et traiter des données personnelles vous concernant dans le cadre de nos échanges. Ainsi, si vous nous appelez ou nous écrivez par le biais d'un courriel ou en utilisant un formulaire de contacts, vos coordonnées ainsi que l'objet de votre message et les autres informationscommuniquées feront l'objet d'un traitement pour répondre à vos demandes et en assurer le suivi.
	    </ul>
  	</div>

  	<h2 class="homestead explainTitle radius-10" style="padding: 1% 0% 0% 1%;color:white;background-color: #089238"> Partage des données personnelles </h2>
  	<div class="explainDesc">
    	<ul style="font-size: 18px;">
	      	Il est possible que nous transférions (ou rendions accessibles) vos informations
			personnelles aux tiers de confiance qui gèrent le Site pour notre compte ou qui
			assurent des prestations de service s'y rapportant (maintenance). Nous ne transférerons aux tiers que les informations utiles et nécessaires pour qu'ils puissent exécuter les prestations que nous leur avons confiées dans le cadre de leurs obligations contractuelles.  
			Nous sommes susceptibles de communiquer des données personnelles vous
			concernant à des tiers, y compris des autorités publiques compétentes, afin de nous conformer aux lois et règlements applicables. Nous pouvons également être conduits à partager, sous une forme anonyme ou agrégée, des informations sur les visiteurs du Site avec d'autres entités, avec des tiers, dont des annonceurs, des partenaires et des sponsors, dans le but d'analyser les tendances et les habitudes des consommateurs et d'améliorer notre Site.   Le Site peut contenir des liens vers d'autres sites qui ne sont pas soumis aux présentes règles. Nous invitons les visiteurs à prendre connaissance des règles mises à disposition par les autres sites.
    	</ul>
  	</div>

	<h2 class="homestead explainTitle radius-10" style="padding: 1% 0% 0% 1%;color:white;background-color: #089238"> Droits par rapport aux données collectées </h2>
	  		<div class="explainDesc">
		  		<ul style="font-size: 18px;">
					Conformément à la loi Informatique et Libertés du 6 janvier 1978, vous disposez d'un droit d'accès, de rectification, de modification et de suppression aux données personnelles vous concernant. Vous pouvez exercer ces droits en nous adressant un courriel: Open Atlas Adresse du siège social : 56 rue andy | 97460 Saint paul, Mail: contact@communecter.org
		  		</ul>
		  	</div>

  	<h2 class="homestead explainTitle radius-10" style="padding: 1% 0% 0% 1%;color:white;background-color: #089238"> Protection des données à caractère personnel - Entrée en
		vigueur du Règlement européen 2016-679 (RGPD) </h2>
		<div class="explainDesc">
	  		<ul style="font-size: 18px;">
				Dans le cadre de ses missions Open Atlas met en oeuvre différents traitements de données à caractère personnel. A ce titre, elle est soumise aux dispositions du règlement européen 2016/679 (règlement général pour la protection des données, également dénommé RGPD) entrant en vigueur le 25 mai 2018. Ce texte renforce les obligations relatives à la protection des données et des personnes concernées, jusque-là définies par la loi 78-17 dite Informatique et libertés.
			</ul>
  		</div>

	<h2 class="homestead explainTitle radius-10" style="padding: 1% 0% 0% 1%;color:white;background-color: #089238"> Que sont les traitements de données à caractère
		personnel ? </h2>
  	<div class="explainDesc">
	    <ul style="font-size: 18px;">
	      	Ce sont tous les traitements manuels ou informatisés relatifs à des données
			permettant d'identifier directement ou indirectement des personnes physiques (par
			exemple l'utilisation de données telles que : nom prénom, photographie, e-mailnominatif, numéro d'identification, données de localisation, données propres à l'identité physique, physiologique, économique d'une personne…)
	    </ul>
 	</div>

 	<h2 class="homestead explainTitle radius-10" style="padding: 1% 0% 0% 1%;color:white;background-color: #089238"> De quelle protection bénéficient les personnes dont les
		données sont traitées par Open Atlas ? </h2>
  	<div class="explainDesc">
	    <ul style="font-size: 18px;">
	      	Pour chaque traitement, en principe lors de la collecte des données, une information transparente, concise et complète doit être fournie. Cela doit permettre aux personnes concernées d'en comprendre l'objectif et de les aider à assurer la maîtrise de leurs données en facilitant l'exercice de leurs droits (opposition, accès,rectification, effacement, limitation, portabilité).
	    </ul>
 	</div>

 	<h2 class="homestead explainTitle radius-10" style="padding: 1% 0% 0% 1%;color:white;background-color: #089238"> Référentiel Général de Sécurité (RGS) </h2>
  	<div class="explainDesc">
	    <ul style="font-size: 18px;">
		En application de l'article 9 de l'ordonnance n°2005-1516 du 8 décembre 2005
		relative aux échanges électroniques entre les usagers et les autorités administratives
		et entre les autorités administratives, du décret n°2010-12 du 2 février 2010 pris pour l'application des articles 9,10 et 12 de l'ordonnance, et de l'arrêté duOpen Atlas font l'objet d'une homologation de sécurité.
	    </ul>
 	</div>

 	<h2 class="homestead explainTitle radius-10" style="padding: 1% 0% 0% 1%;color:white;background-color: #089238"> Cookies </h2>
  	<div class="explainDesc">
	    <ul style="font-size: 18px;">
		Qu'est ce qu' un témoin de connexion (cookie).
		Le site utilise des cookies. Un cookie est un fichier texte stocké sur votre ordinateur quand vous visitez notre site. Ils permettent d'identifier votre ordinateur lors de votre prochaine visite.  
		Quelle utilisation faisons-nous des cookies ?
		Lorsque vous visitez notre site nous pouvons, selon vos choix exprimés par le
		paramétrage de votre navigateur, installer divers cookies sur le disque dur de votre ordinateur. 
		<br/>Nous pouvons lire les cookies que nous avons installés pendant la durée
		de validité de chaque cookie ou jusqu'à ce que vous supprimiez ledit cookie.
		<br/>Nous utilisons les cookies pour les finalités suivantes sur notre site : 
		<br/>
		<br/>
		<li>Analyse de la performance de notre site à partir d'informations anonymes sur
		votre navigation (par exemple: pages vues, nombre de visites....). Nous
		utilisons pour ce faire l'outil d'analyse «Google Analytics». </li>
		<br/>
		<li>Analyse du partage des pages de notre site à partir d'informations anonymes	sur votre navigation. Nous utilisons pour ce faire l'outil «Addthis». </li>
		<br/>
		Les cookies que nous utilisons ne nous permettent pas de vous identifier
		personnellement et sont conçus pour être utilisés uniquement par La Mairie de La
		Possession.   Les données recueillies sont conservées par La Mairie de La
		Possession pour une durée de 6 mois et ne sont pas cédées à des tiers ni utilisées à
		d'autres fins.
		Puis-je m'opposer à l'enregistrement des cookies ?
		Vous pouvez vous opposer à l'enregistrement de ces "cookies" de façon globale ou
		au cas par cas en ajustant les paramètres de votre navigateur. Nous vous invitons à
		vous reporter à la procédure figurant dans les règles d'utilisation de votre
		navigateur.  Nous vous informons néanmoins que si vous choisissez de refuser
		tout type de cookies, certaines fonctionnalités du site pourraient ne pas être
		accessibles.   Vous pouvez également désactiver les cookies placés dans le
		cadre de l'utilisation de Google Analytics en installant le module complémentaire
		fourni par Google à l'adresse
		suivante: https://tools.google.com/dlpage/gaoptout?hl=fr
	    </ul>
 	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function() {
	setTitle("Politique de confidentitalité","");
});
</script>
</div>