<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	filters : {
	 		scope : {
	 			view : "scope",
	 			type : "scope",
	 			action : "scope"
	 		}
	 	}
	};
	 
	function lazyFilters(time){
	  if(typeof filterObj != "undefined" )
	    filterGroup = filterObj.init(paramsFilter);
	  else
	    setTimeout(function(){
	      lazyFilters(time+200)
	    }, time);
	}

	jQuery(document).ready(function() {
		lazyFilters(0);
	});
</script>