<style type="text/css">
  #home-container{
    background-color:black;
  }
  .playBtn{
    color: black !important;
    font-size: 22px;
    padding: 5px 10px !important;
    margin-top: 20px;
    margin-bottom: 20px;
    font-weight: 800;
  }
</style>
<div id="home-container" class="col-xs-12 no-padding">
<?php 
  $countPerson=PHDB::count(Person::COLLECTION, array('$or'=>array(
      array("source.key"=>"chtitube"),
      array("reference.costum"=>array('$in'=>["chtitube"]))))); 
if(Yii::app()->session["userId"]!=""){
  $name=(!empty(Yii::app()->session["user"]["username"])) ? Yii::app()->session["user"]["username"] : Yii::app()->session["user"]["name"];
  $infosGammifications=Element::getElementById(Yii::app()->session["userId"], Person::COLLECTION, null,  array("gamification"));
  $totalPoint=0;
  if(!empty($infosGammifications["gamification"]) 
    && isset($infosGammifications["gamification"]["source"])
    && isset($infosGammifications["gamification"]["source"]["chtitube"])
    && isset($infosGammifications["gamification"]["source"]["chtitube"]["total"])){
    $totalPoint=$infosGammifications["gamification"]["source"]["chtitube"]["total"];
  }
  $rangeInTournament=PHDB::count(Person::COLLECTION, array(
    '$or'=>array(
      array("source.key"=>"chtitube"),
      array("reference.costum"=>array('$in'=>["chtitube"]))),
      "gamification.source.chtitube.total"=>array('$gte'=>$totalPoint),
      "_id"=>array('$ne'=> new MongoId(Yii::app()->session["userId"]))));
?>
<div class="chtitube-container col-xs-12 no-padding text-center">
  <h3 class="main-title padding-20">Salut <span class="text-yellow"><?php echo  $name ?></span></h3>
  <span style="font-size: 20px;">Vous avez <b class="text-yellow"><?php echo $totalPoint ?></b> points</span><br/><br/>
  <span style="font-size: 20px;">Votre classement :<b class="text-yellow"><?php echo $rangeInTournament+1 ?></b> / <?php echo $countPerson ?></span><br/><br/>
  <span style="font-size:22px;"><b class="text-yellow">Prêt⋅e pour faire monter la pression ?</b></span><br/><br/>
  <a href="#play" class="lbh btn bg-yellow col-xs-6 col-xs-offset-3 playBtn">Jouer</a><br/><br/>
  <a href="#references" class="lbh btn text-yellow col-xs-12" style="font-size: 22px;">Retrouvez vos ajouts</a>
  <span class="col-xs-12 text-center">Jusqu'au <b class="text-yellow">20 Février 2020</b> pour être dans les 5 premiers ch'titubeurs</span>
   <div class="btnShare col-xs-12 margin-top-20 margin-bottom-20">
      <img class="co3BtnShare" width="60" src="<?php echo $this->module->getParentAssetsUrl() ?>/images/social/facebook-icon-64.png" onclick="window.open('https://www.facebook.com/sharer.php?u=www.communecter.org/costum/co/index/id/chtitube','_blank')"/>
      
      <img class="co3BtnShare margin-left-10" width="60" src="<?php echo $this->module->getParentAssetsUrl() ?>/images/social/twitter-icon-64.png" onclick="window.open('https://twitter.com/intent/tweet?url=www.communecter.org/costum/co/index/id/chtitube','_blank')"/>
    </div>
   
 
</div>
<?php } else { ?>
<div class="col-xs-12 no-padding text-center" style="max-height: 350px;">
    <img class="img-responsive start-img" style="max-height: 350px;margin:auto;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/chtitube/logo.png'>
  </div>

  <h3 class="main-title padding-20">L’équipe du <span class="text-yellow">Ch'Titube</span> vous propose de faire monter la <span class="text-yellow">la pression</span> <br/>avec ce jeu 100%  <span class="text-yellow">Libre</span> et 100% <span class="text-yellow">Ouvert</span></h3>
  <span class="col-xs-12 text-center">Gagner de bonnes <a href="#lots" class="lbh text-yellow">bières artisanales du 62</a> pour bien entamer l'année 2020 tout en contribuant à un bien commun <br/><a href="#regles" class="lbh text-yellow">+ en savoir plus</a></span>
  <span class="col-xs-12 text-center margin-top-20">Jusqu'au <b class="text-yellow">20 Février 2020</b> pour être dans les 5 premiers ch'titubeurs</span>
  <!--<span class="col-xs-12 text-center">Déjà <?php echo $countPerson ?> Joueur⋅se⋅s</span>-->
  <div class="col-xs-12 text-center text-white margin-top-20">
    <button class="btn-register col-xs-12" data-toggle="modal" data-target="#modalLogin">Jouer</button>
    <button class="btn-register col-xs-12" data-toggle="modal" data-target="#modalRegister">Inscrivez-vous dès maintenant en cliquant ici</button>
    <span class="italic col-xs-12">Et surtout partagez à vos ami⋅e⋅s</span>
    <div class="btnShare col-xs-12 margin-top-20 margin-bottom-20">
      <img class="co3BtnShare" width="60" src="<?php echo $this->module->getParentAssetsUrl() ?>/images/social/facebook-icon-64.png" onclick="window.open('https://www.facebook.com/sharer.php?u=www.communecter.org/costum/co/index/id/chtitube','_blank')"/>
      
      <img class="co3BtnShare margin-left-10" width="60" src="<?php echo $this->module->getParentAssetsUrl() ?>/images/social/twitter-icon-64.png" onclick="window.open('https://twitter.com/intent/tweet?url=www.communecter.org/costum/co/index/id/chtitube','_blank')"/>
    </div>
   <!-- <a href="#search" class="lbh" class="text-yellow">Carto</a></span>
    <a href="#agenda" class="lbh" class="text-yellow">Agenda</a></span>-->
    
  </div>
</div>
<?php } ?>
<script type="text/javascript">
 
  jQuery(document).ready(function() {
      // Lancement du compte à rebours au chargement de la page
      setTitle("Ch'Titube, fais monter la pression");
      setTimeout(function(){
        coInterface.initHtmlPosition();
      },1000)
      //$(".openRegister").click(function(){
        //Login.openRegister();
      //});
});
</script>


