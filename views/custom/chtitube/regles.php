<div class="col-xs-12">
	<h2>Règles et principes du jeu</h2>
	<h3 class="text-yellow">Comment je fais pour jouer à Chtitube ?</h3>
	<span>Je me rends sur <a style="color:#2E90F3;" href="#apropos" class="lbh">www.chtitube.fr</a> et je m’inscris en cliquant sur " <em style="font-weight: 900;">Inscrivez-vous dès maintenant en cliquant ici</em> " en bas de la page d’accueil.</span></br></br>
	
	<span>Je remplis mes informations personnelles et valide les conditions générales d’utilisation et le règlement du jeu. Puis je me rends sur ma boîte e-mail renseignée ( <i class="fa fa-exclamation-triangle"></i> il se peut que l’e-mail atterrisse dans les spams) et je clique sur le lien d'activation de mon compte.</span></br>

	<span>C’est bon ? Alors vous êtes fin prêt pour faire monter la pression avec Chtitube !</span>

	<h3 class="text-yellow">Le principe ?</h3>
	<span>Faire connaître vos endroits favoris, valoriser les commerçants et artisans les plus (ou moins <i class="fa fa-smile-o"></i> ) sympas et talentueux, mettre en avant une initiative qui mériterait d’être connue du plus grand nombre.</br> Et si vous étiez maire en 2020, vous auriez des  questions, des idées, des propositions ? </br>Votre territoire est vivant et en pleine mutation alors faites savoir ce qu’il s’y passe et ce qu’il serait bon d’y apporter !</span>
	<br/>
	<h3 class="text-yellow">62 POWER !</h3>
	<span>Tout ajout doit être localisé dans le 62 donc n’oubliez pas de préciser l’adresse pour chacun d'eux.</br> 
	Un contenu complet méritera la validation du jury et ainsi l’attribution des points qui font monter la pression. Mais prenez votre temps, vous pourrez revenir sur vos ajouts et les modifier après les avoir créés.</span>

	<h3 class="text-yellow">Gagner des points</h3>
	<span>La valeur du point dépend du type d'ajouts que j'effectue. Tous les détails sont sur la page <a style="color:#2E90F3;" href="#play" class="lbh">Jouer</a>.</span>

	<h3 class="text-yellow">Tout ça pour quoi ?</h3>
	<span>Participer à une carto-partie géante à l’échelle du Pas-de-Calais et me mesurer à tous les gens de min coin. Nous pourrons alors connaître celui qui connaît le plus de choses dans son village, son quartier, sa ville, et saura révéler toute la vie qui s’y grouille … Et  c’est aussi et surtout pour parvenir à <b><a href="#lots" class="lbh" style="color:#2E90F3;">GAGNER UN SUPER LOT</a></b> !</span></br></br>
	<span>Alors je n’hésite plus ! Une fois connecté(e), je clique sur <a style="color:#2E90F3;" href="#play" class="lbh">Jouer</a> sur la page d’accueil ou dans le menu latéral.</span>

	<h3 class="text-yellow">Menu latéral</h3>
	<span>Dans le menu latéral, j’ai accès au lien vers ...</span>
	<ul>
		<li style="font-size: 14px;">Mes ajouts :</li>
			<ul>
				<li style="font-size: 14px;">me permet de voir le nombre d'ajouts que j’ai effectués et ceux qui ont été validés le jury</li>
			</ul>
		<li style="font-size: 14px;">Mon tableau de bord :</li>
			<ul>
				<li style="font-size: 14px;">me salue et m’encourage à faire monter la pression ! Et oui, très important pour la motivation du chtitubeur.</li>
				<li style="font-size: 14px;">me permet de voir le nombre de points et mon classement</li>
				<li style="font-size: 14px;">me permet de jouer en un clic : le bouton <a style="color:#2E90F3;" href="#play" class="lbh">jouer</a> me renvoit au choix des types d'ajouts que je souhaite realiser</li>
				<li style="font-size: 14px;">me permet d’accèder directement à la page “Mes Ajouts”, en plus du menu latéral</li>	
			</ul>
		
	</ul>

	<h3 class="text-yellow">Origine et suite du jeu</h3>
		<span>Pour comprendre toute la génèse et les suites à donner à la plate-forme Chtitube, ça se passe <a style="color:#2E90F3;" href="#apropos" class="lbh">ici</a></span>.

	<h3 class="text-yellow">Réglement et conditions générales</h3>
		<span>Pour consulter le règlement et les conditions générales du jeu Chtitube, cliquez <a style="color:#2E90F3;" href="#reglement" target="_blank" class="lbh">ici</a></span>.


</div>