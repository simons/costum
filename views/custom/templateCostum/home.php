<?php 
$cssAnsScriptFilesTheme = array(
    '/plugins/jsonview/jquery.jsonview.js',
     '/plugins/jsonview/jquery.jsonview.css',
     '/plugins/jQuery/jquery-2.1.1.min.js',
     '/plugins/jQuery/jquery-1.11.1.min.js',
     '/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
     '/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js'             
 );
    /*
    Permet l'affichage des élèment selon la configuration du costum
    */
    // Pour afficher les élèments header
    $logo = @Yii::app()->session["costum"]["htmlConstruct"]["header"]["logo"] ? Yii::app()->session["costum"]["htmlConstruct"]["header"]["logo"] : null;
    $bannerImg = @Yii::app()->session["costum"]["htmlConstruct"]["header"]["bannerImg"] ? Yii::app()->session["costum"]["htmlConstruct"]["header"]["bannerImg"] : null;
    //PREMIÈRE DIV
    $intro = @Yii::app()->session["costum"]["htmlConstruct"]["element"]["intro"]["title"] ? Yii::app()->session["costum"]["htmlConstruct"]["element"]["intro"]["title"] : null; 
    $introtitle = @Yii::app()->session["costum"]["htmlConstruct"]["element"]["intro"]["left"]["title"] ? Yii::app()->session["costum"]["htmlConstruct"]["element"]["intro"]["left"]["title"] : "Bienvenue sur le template Costum";
    $introparagraphe = @Yii::app()->session["costum"]["htmlConstruct"]["element"]["intro"]["left"]["paragraphe"] ? Yii::app()->session["costum"]["htmlConstruct"]["element"]["intro"]["left"]["paragraphe"] : "Ceci est un paragraphe pour l'affichage du template";
    $introimg = @Yii::app()->session["costum"]["htmlConstruct"]["element"]["intro"]["right"]["img"] ? Yii::app()->session["costum"]["htmlConstruct"]["element"]["intro"]["right"]["img"] : null;

    //DEUXIEME DIV
    $dtitle = @Yii::app()->session["costum"]["htmlConstruct"]["element"]["description"]["left"]["title"] ? Yii::app()->session["costum"]["htmlConstruct"]["element"]["description"]["left"]["title"] : "";
    $dparagraphe = @Yii::app()->session["costum"]["htmlConstruct"]["element"]["description"]["left"]["paragraphe"] ? Yii::app()->session["costum"]["htmlConstruct"]["element"]["description"]["left"]["paragraphe"] : "";
    $dimg = @Yii::app()->session["costum"]["htmlConstruct"]["element"]["description"]["right"]["img"] ? Yii::app()->session["costum"]["htmlConstruct"]["element"]["description"]["right"]["img"] : "";

    //TROISIEME DIV 
    $newstitle = @Yii::app()->session["costum"]["htmlConstruct"]["element"]["actu"]["title"] ? Yii::app()->session["costum"]["htmlConstruct"]["element"]["actu"]["title"] : "Actualité";
    $newsactive = @Yii::app()->session["costum"]["htmlConstruct"]["element"]["actu"]["active"] == true ? true : false;
   
    //QUARTRIEME DIV
    $agendatitle = @Yii::app()->session["costum"]["htmlConstruct"]["element"]["agenda"]["title"] ? Yii::app()->session["costum"]["htmlConstruct"]["element"]["agenda"]["title"] : "Agenda";
    $agenda = @Yii::app()->session["costum"]["htmlConstruct"]["element"]["agenda"]["active"] == true ? true : false;

    //CAROUSSEL
    $caroussel = @Yii::app()->session["costum"]["htmlConstruct"]["element"]["agenda"]["caroussel"] == true ? true : false;

?>

<style>
.header{
    background: <?php echo @Yii::app()->session["costum"]["css"]["header"]["background"] ? Yii::app()->session["costum"]["css"]["header"]["background"] : "white"; ?>;
    <?php if(@$bannerImg) { ?>
    background-image : url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?><?php echo $bannerImg; ?>);
    <?php } ?>
    background-size: cover;
    padding-bottom: 58%;
}
.intro {
    margin-top: -7%;
}
.intro-description{
    box-shadow: 0px 0px 20px -2px black;
    width: 80%;
    left: 10%;
    background: white;
    padding: 3%;
    background-color : <?php echo @Yii::app()->session["costum"]["css"]["intro"]["background"] ? Yii::app()->session["costum"]["css"]["intro"]["background"] : "white"; ?>;
    color : <?php echo @Yii::app()->session["costum"]["css"]["intro"]["color"] ? Yii::app()->session["costum"]["css"]["intro"]["color"] : "black"; ?>;
}
.description {
    margin-top: 5%;
    background-color : <?php echo @Yii::app()->session["costum"]["css"]["description"]["background"]; ?>;
    color : <?php echo @Yii::app()->session["costum"]["css"]["description"]["color"] ? Yii::app()->session["costum"]["css"]["description"]["color"] : "black"; ?>;
}

.agenda{
    margin-top: 3%;
    background-color : <?php echo @Yii::app()->session["costum"]["css"]["agenda"]["background"]; ?>;
    color : <?php echo @Yii::app()->session["costum"]["css"]["agenda"]["color"] ? Yii::app()->session["costum"]["css"]["agenda"]["color"] : "black"; ?>;
}
.agenda-title{
    margin-left: 9%;
    margin-top: 1%;
}
.event-lien {
    margin-top: 2%;
    margin-bottom: 5%;
    text-align: center;
    font-size: 2rem;
}
.plus-m {
    width: 2%;
}
.event {
    margin-top : 2%;
}
.description-paragraphe{
    text-align : center;
}
.p-mobile-join{
    font-size: 2.85vw;
    line-height: 42px;
}
.text-m1{
    font-size: 1.2em;
}
.text-m2{
    font-size: 0.86em;        
}
button {
    background-color: Transparent;
    background-repeat:no-repeat;
    border: none;
    cursor:pointer;
    overflow: hidden;
    outline:none;
}
<?php if(@$logo) { ?>
.logofn{
    margin-top: 5%;
margin-left: 3%;
}
<?php } ?>
<?php if(@$intro){ ?>
    .intro-title {
        /* color : <?php echo @Yii::app()->session["costum"]["css"]["intro"]["color"] ? Yii::app()->session["costum"]["css"]["intro"]["color"] : "black"; ?>; */
        color : white;
        font-size: 2vw;
    }
<?php } ?>

<?php if($newsactive == true){ ?>
    .newslettre-title{
        margin-left : 9%;
        color : <?php echo @Yii::app()->session["costum"]["css"]["actu"]["css"]["color"] ? Yii::app()->session["costum"]["css"]["actu"]["color"] : "black"; ?>;
    }
    .newsletter {
        margin-top: 3%;
    }
<?php } ?>

<?php  if($caroussel == true){ ?>
    .agenda-carousel{
        margin-top: 2%;
        background: white;
        width: 85%;
        margin-left: 8%;
    }
    .carousel-inner{
    position: relative;
    width: 100%;
    overflow: hidden;
    height: 500px;
    }
    #caroussel{
    /* background-color: rgba(0,0,0,0.5); */
    opacity: 1;
    width: 31%;
    z-index: 1;
    margin-left: 5%;
    }
    .resume {
        text-align: left;
        z-index: 10;
        position: absolute;
        padding-top: 8%;
        padding-left: 2%;
        background-color:
        rgba(0,0,0,0.7);
        width: 40%;
        padding-bottom: 100%;
    }
    .resume > h2{
        font-size : 2vw;
        color : white;
    }
    .resume > p{
    font-size : 1.3vw;
    color : white;
    }
    #arrow-caroussel{
        margin-top: 100%;
        font-size: 3vw;
        color:white;
        position: absolute;
    }
    #arrow-caroussel > a {
        color : white;
        text-decoration : none;
        margin: 3%;
    }
    .carousel {
        position: revert !important;
    }
    .carousel-border{
      box-shadow: 0px 0px 20px -2px black;
      background: white;
      margin-bottom : 7%;
}
.ctr-img{
    width : 100%; 
    height:auto;
}
<?php } ?>
<?php if(@$agenda == true){ ?>
    .card{
    background: black;
    margin-left: 10%;
    margin-right: 5%;
    color: <?php echo @Yii::app()->session["costum"]["css"]["agenda"]["card"]["color"] ? Yii::app()->session["costum"]["css"]["agenda"]["card"]["color"] : "black"; ?>;
    font-size: 1.5vw;
    box-shadow: 0px 0px 20px -2px black;
}
    .card-info{
        background: <?php echo @Yii::app()->session["costum"]["css"]["agenda"]["card"]["background"] ? Yii::app()->session["costum"]["css"]["agenda"]["card"]["background"] : "white"; ?>;
        padding : 2%;
        font-size : 1.3vw;
    }
    .card-c{
        right: 3.71%;
        margin-top: -35%;
    }
    .img-event {
    height: 250px;
    width: 500px;
}
<?php } ?>

/* MOBILE */
@media (max-width:768px){
    .plus-m {
    width : 5%;
    }

    .card-c{
        right : 16.71%;
        margin-top : -50%;
    }
    .carousel-inner{
        position: relative;
        width: 100%;
        overflow: hidden;
        height: 150px;
    }
    #arrow-caroussel{
        margin-top: 125%;
        font-size: 3vw;
        color:white;
    }
    .img-event {
        height: 75px;
        width: 250px;
    }
    .carto-p-m{
        font-size: 2.65vw;
        margin-top: 2%;
        line-height: 12px;
    }
    .card-info{
        padding: 2%;
        font-size: 2.3vw;
        margin-left: 0%;
        width: 100%;
        margin-top: -24%;
    }
    .agenda-title {
        margin-left: 3%;
        margin-top: 1%;
    }
    .resume > p {
        font-size: 2.5vw;
        color: white;
    }
    .resume > h2 {
        font-size: 3.5vw;
    }
    .resume{
        padding-top: 5%;
    }
    .event-lien {
        font-size: 1.5rem;
    }
    .p-mobile-description{
        font-size: 1.2rem;
    }
    .p-mobile-join{
        font-size: 1.2rem;
        line-height: 14px;
    }
    .intro-title {
        margin-top: -4%;
        font-size : 2.58vw;
    }
    .text-m1{
        font-size: 4.95vw;
    }
    .text-m2{
        font-size : 3.3vw;
    }
}
</style>

<!-- ////// INITIALISATION DU HEADER ////// -->
<div class="header">
<?php if(@$logo) { ?>
    <div class="logofn col-sm-9 col-xs-9">
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?><?php echo $logo; ?>" class="img-responsive" style="width: 65%;">
    </div>
<?php } ?>
</div>

<!-- ////// INITIALISATION INTRO ////// -->
<div class="intro row">
    <?php if(@$intro){ ?>
            <div class="intro-title col-xs-6 col-sm-6 no-padding"><i style="margin-left:19%;"><?php echo $intro; ?></i></div>
            <div class="col-xs-6 col-sm-6"><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/filigram.png" class="img-responsive" style="margin-top: -63%;margin-left: 7%; position: absolute; opacity: 0.7;"></div>
    <?php } ?>
    <div class="intro-description col-xs-12 col-sm-12">
    <?php if(@$introimg){
        ?>
    <div class="intro-paragraphe col-sm-6 col-xs-6">
    <h1 style="color : #41435b;"><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/carto.svg" class="img-responsive" style="width:6.5%;"> <?php echo $introtitle; ?></h1>
    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/trait.svg" class="img-responsive" style="margin-top: -7%; margin-right: 24%;"><br>
        <p class="p-mobile-description"><?php echo $introparagraphe; ?></p>

        <div style="text-align: center;">
        <a href="javascript:;" data-hash="#map" class="lbh-menu-app" style="text-decoration : none; text-align: center">
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/plus.svg" class="img-responsive" style="width:7%;"></a></div>

    </div>
    <div class="intro-img col-sm-6 col-xs-6">
    <a href="javascript:;" data-hash="#map" class="lbh-menu-app" style="text-decoration : none;">
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?><?php echo $introimg; ?>" class="img-responsive" style="width: 100%;">
    </a>
    </div>
    <?php } else {  ?> 
    <div class="intro-paragraphe col-sm-12 col-xs-12">
    <h1><?php echo $introtitle; ?></h1>
        <p class="p-mobile-description"><?php echo $introparagraphe; ?></p>
    </div>
    <?php } ?>
    </div>
</div>

<!-- ////// INITIALISATION DESCRIPTION ///// -->
<div class="description row">
        <div class="description-paragraphe col-xs-6 col-sm-6">
            <h1 style="margin-left : 9%; margin-top: 10%;"> <?php echo $dtitle;  ?></h1>

                <!-- Btn je rejoins -->
    <!-- début svg --> 
    <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 225.3 39.91" style="width:50%;">
	<defs><style>.cls-1{fill:#fff;}</style></defs>
	<title>je rejoins</title>


	<path class="cls-1" d="M192.66,268.72a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm190-19.2H181.14l-11.9,20,11.9,19.95H382.65l11.89-19.95ZM199.53,267.66a4.19,4.19,0,0,1-.16,1.07l8.46,4.07a4.36,4.36,0,0,1,3.38-1.59,4.23,4.23,0,1,1-4.14,3.07c-2.83-1.37-5.65-2.71-8.47-4.08a4.29,4.29,0,0,1-2.67,1.53c-.14,1.86-.27,3.71-.41,5.57a4,4,0,0,1,.74.3,2.9,2.9,0,0,1,1.09,4.06,3.16,3.16,0,0,1-4.23,1,2.91,2.91,0,0,1-1.09-4.06,3.12,3.12,0,0,1,2.26-1.42c.13-1.82.28-3.64.4-5.46a4.28,4.28,0,0,1-2.44-1.12l-4.55,2.82a2.9,2.9,0,0,1-.21,2.59,3.15,3.15,0,0,1-4.23,1,2.91,2.91,0,0,1-1.07-4.07,3.15,3.15,0,0,1,4.22-1,2.91,2.91,0,0,1,.62.49l4.46-2.77a4,4,0,0,1-.56-2,3.6,3.6,0,0,1,.26-1.4l-2.81-2a2.65,2.65,0,0,1-.55.37,2.59,2.59,0,0,1-3.41-1,2.41,2.41,0,0,1,1.1-3.29,2.6,2.6,0,0,1,3.42,1.05,2.41,2.41,0,0,1,0,2.14l2.66,1.88a4.28,4.28,0,0,1,3-1.81c0-.94,0-1.9,0-2.85a2.51,2.51,0,0,1-1.77-1.26,2.39,2.39,0,0,1,1.09-3.29,2.59,2.59,0,0,1,3.41,1.05,2.4,2.4,0,0,1-1.1,3.29,2,2,0,0,1-.62.2l0,2.83a4.38,4.38,0,0,1,3,1.58l8.47-4.08a4.12,4.12,0,0,1-.16-1.06,4.3,4.3,0,1,1,4.3,4.13,4.33,4.33,0,0,1-3.38-1.59l-8.46,4.07A4.19,4.19,0,0,1,199.53,267.66ZM192.7,269l0,0a2,2,0,0,1-.08-.22A.77.77,0,0,0,192.7,269Zm0-.25a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Z" transform="translate(-169.24 -249.52)"/>
	<g>
		<text x="59" y="26" class="text-m1"><a href="javascript:;" onclick="dyFObj.openForm('organization'); " style="text-decoration : none;"> Je rejoins la filière</a></text>
	</g>
</svg>
    <!-- fin svg --> 
<br>
    <!-- Btn je propose -->
    <!-- Debut svg --> 
    <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 220.44 29.05" style="width:75%;">
	<defs><style>.cls-1{fill:#fff;}</style></defs>
	<title>je souhaite</title>

	<path class="cls-1" d="M168.56,424.7a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm194.73-14H160.17l-8.66,14.53,8.66,14.52H363.29L372,425.26Zm-189.73,13.2a2.89,2.89,0,0,1-.12.78c2.05,1,4.11,2,6.16,3a3.19,3.19,0,0,1,2.46-1.16,3,3,0,1,1-3.13,3,2.88,2.88,0,0,1,.12-.77l-6.17-3a3.13,3.13,0,0,1-1.94,1.12c-.11,1.35-.2,2.7-.3,4.05a2.52,2.52,0,0,1,.54.21,2.12,2.12,0,0,1,.79,3,2.3,2.3,0,0,1-3.08.76,2.12,2.12,0,0,1-.79-3,2.29,2.29,0,0,1,1.64-1c.1-1.33.21-2.65.3-4a3.13,3.13,0,0,1-1.78-.82L165,428.16a2.1,2.1,0,0,1-.16,1.88,2.3,2.3,0,0,1-3.07.76,2.13,2.13,0,0,1-.79-3,2.29,2.29,0,0,1,3.07-.76,2,2,0,0,1,.46.36l3.25-2a2.8,2.8,0,0,1-.41-1.49,2.88,2.88,0,0,1,.18-1l-2-1.44a1.65,1.65,0,0,1-.4.27,1.88,1.88,0,0,1-2.48-.77,1.75,1.75,0,0,1,.8-2.39,1.88,1.88,0,0,1,2.49.77,1.76,1.76,0,0,1,0,1.55l1.94,1.37A3.1,3.1,0,0,1,170,421c0-.68,0-1.38,0-2.07a1.84,1.84,0,0,1-1.29-.92,1.74,1.74,0,0,1,.8-2.39,1.88,1.88,0,0,1,2.48.76,1.75,1.75,0,0,1-.8,2.4,1.39,1.39,0,0,1-.45.14c0,.69,0,1.38,0,2.06a3.17,3.17,0,0,1,2.16,1.15l6.17-3a2.76,2.76,0,0,1-.12-.77,3.13,3.13,0,1,1,3.13,3,3.19,3.19,0,0,1-2.46-1.16l-6.16,3A2.89,2.89,0,0,1,173.56,423.93Zm-5,.95,0,0a.75.75,0,0,1-.05-.16A.57.57,0,0,0,168.59,424.88Zm0-.18a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Z" transform="translate(-151.51 -410.73)"/>
	<g>
		<text x="43" y="19" class="text-m2"><a onclick="dyFObj.openForm('project');" style="text-decoration : none" href="javascript:;">Je souhaites ajouter un projet</a></text>
	</g>
</svg>
    <!-- fin svg --> 

            <p style="margin-top:6%;" class="p-mobile-join">
                <i><?php echo $dparagraphe; ?></i>
            </p>
        </div>
        <div class="description-image col-xs-6 col-sm-6">
            
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?><?php echo $dimg; ?>" class="img-responsive">
        </div>
</div>

<?php if(@$newsactive == true){
    ?>
<!-- ///// INITILISATION NEWS ///// --> 
<div class="newsletter row">
        <div class="newslettre-title col-xs-6 col-sm-6">
            <h1 style="color : #41435b;"><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/actus.svg" class="img-responsive" style="width: 4%;"> <?php echo $newstitle; ?></h1>
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/trait.svg" class="img-responsive" style="margin-top: -5%; margin-right: 42%;"><br>
        </div>
        <div class="newsletter-file col-xs-12 col-sm-12">
            <div id="newsstream" style="width: 90%; margin-left: 6%; margin-top: 3%;"></div>
        </div>
        <div class="newsletter-a col-xs-12 col-sm-12" style="text-align: center;">
        <a href="javascript:;" data-hash="#live" class="lbh-menu-app " style="text-decoration : none;" class="event-lien">
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/templateCostum/plus.svg" class="img-responsive plus-m"> <br>
       Voir plus <br> l'actualité </a>
        </div>
</div>
<?php } ?>

<?php if(@$agenda == true){ ?>
<!-- ///// INITILISATION AGENDA //// -->
<div class="agenda row">
        <div class="agenda-title col-xs-12 col-sm-12">
            <h1><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/agenda.svg" class="img-responsive" style="width: 3%;"> <?php echo $agendatitle; ?></h1>
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/trait-blanc.svg" class="img-responsive" style="margin-top: -2%; padding-right: 81%;;">
            <p style="margin-top: -1%;">Évènement à venir</p>
        </div>    
            <div class="event col-sm-12 col-sm-12"><div id="event-affiche"></div></div>
    <div class="event-lien col-xs-12 col-sm-12">
  
            <a href="javascript:;" data-hash="#agenda" class="lbh-menu-app " style="text-decoration : none; color:white;">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/templateCostum/plus-blanc.svg" class="img-responsive plus-m" style="margin: 1%;"><br>            Voir plus<br>
            d'évènement</a>
        </div>
</div>
<?php } ?>

<?php if(@$caroussel == true){ ?>
<!-- //// INITILISATION DU CAROUSSEL ///// --> 

<div class="agenda-carousel">

<div class="col-xs-12 col-sm-12 col-md-12 no-padding carousel-border" style="margin-top : -5.5%;">
    	<div id="docCarousel" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner itemctr">
            </div>
            <!-- <div class="background-carousel">
            .
            </div> -->
            <div id="caroussel" class="carousel-control col-xs-6 col-sm-6 col-md-6">
                <div id="arrow-caroussel">
               <!-- Début svg --> 
               <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 152.1 54.9" style="width:35%;">
	<defs><style>.cls-3{fill:none;}.cls-2{fill:#fff;}</style></defs>
	<title>fleche</title>
    <a href="#docCarousel" data-slide="prev"> <rect class="cls-3" x="73.74" width="78.36" height="54.9"/><polygon class="cls-2" points="87.57 43.75 119.64 43.75 119.64 54.9 147.08 27.45 119.64 0 119.64 11.14 87.57 11.14 87.57 43.75"/></a>
	<a href="#docCarousel" data-slide="next"><rect class="cls-3" x="131.87" y="340.95" width="78.36" height="54.9" transform="translate(210.23 395.84) rotate(180)"/><polygon class="cls-2" points="64.53 11.14 32.46 11.14 32.46 0 5.01 27.45 32.46 54.9 32.46 43.75 64.53 43.75 64.53 11.14"/></a>
</svg>
               <!-- Fin svg --> 
                </div>
            </div>
    			<!-- <a class="left carousel-control" href="#docCarousel" data-slide="prev">
    				<span class="glyphicon glyphicon-chevron-left"></span>
    				<span class="sr-only">Previous</span>
  				</a>
  				<a class="right carousel-control" href="#docCarousel" data-slide="next">
    				<span class="glyphicon glyphicon-chevron-right"></span>
    				<span class="sr-only">Next</span>
  				</a> -->
    	</div>
  	</div> 

</div>
<?php } ?>

<!-- SCRIPT --> 
<script>
$(document).ready(function(){
    console.log("---------------- Document ready");
    news();
    event();
});


function news(){
    if(costum.htmlConstruct.element.actu.active == true){
        console.log("----------------- Affichage News");
        urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false";
        ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false}, function(news){}, "html");
    }
}

function event(){
    if(costum.htmlConstruct.element.agenda.active == true){
        console.log("----------------- Affichage évènement");
        var params = {
            "slug" : costum.slug
        }
        $.ajax({
            type : "GET",
            url : baseUrl + "/costum/templateCostum/geteventaction",
            data : params,
            dataType : "json",
            async : false,
            success : function(data){
                console.log("success : ",data);
                var str = "";
                var ctr = "";
                var itr = "";
                // var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + costum.htmlConstruct.directory.results.events.defaultImg;
                // console.log(url);
                var i = 1;
                if(data.result == true){
                    $(data.element).each(function(key,value){
                        var startDate = (typeof value.startDate != "undefined") ? "Du "+dateToStr(value.startDate, "fr", false, false) : null;

                   //Dev prod
                    var imgMedium = (value.imgMedium != "none") ? value.imgMedium : null;
                    var img = (value.img != "none") ? value.img : null;

                   //Local
                    // var imgMedium = (value.imgMedium != "none") ? '/ph/'+value.imgMedium : null;
                    // var img = (value.img != "none") ? '/ph/'+value.img : null;

                    str += "<div class='col-xs-4 col-sm-4 col-md-4'>";
                    str += "<div class='card'>";
                    str += "<button class='lbh-preview-element' data-hash='#page.type.events.id."+value.id+"' href='@"+value.slug+"' style='text-decoration : none;'>";
                    str += "<img src='"+imgMedium+"' class='img-responsive img-event'>";
                    str += "<div class='col-xs-12 col-sm-12 col-md-12 card-c'>";
                    str += "<div class='col-xs-6 col-sm-6 col-md-6 card-info'>"+startDate+"<br>"+value.type;
                    str += "</button></div></div></div></div>";

                    if(i == 1) ctr += "<div class='item active'>";
                    else ctr += "<div class='item'>";

                    ctr += "<div class='resume col-xs-3 col-sm-3 col-md-3'>";
                    ctr += "<h2 class='h2-day'>"+startDate+"</h2>";
                    ctr += "<h2>"+value.name+"</h2>";
                    ctr += "<p>"+value.resume+"<p>";
                    ctr += "</div>";

                    ctr += "<img src='"+img+"' class='ctr-img img-responsive'>";
                    ctr += "</div>";

                    i++;
                    });
                    console.log(ctr);
                    $(".itemctr").html(ctr);
                    // $("#tclass").html(ctr);
                    // $(".resume").html(resume);
                    console.log(ctr);
                    // console.log(resume);

                    }
                    else{
                    str += "<div class='col-xs-12 col-sm-12 col-md-12'><b  style='font-size: 1.5vw;' >Aucun évènement n'est prévu</b></div>";
                    $(".agenda-carousel").hide();
                    }
                    $("#event-affiche").html(str);
                    console.log(str);

            },
            error : function(e){
                console.log("error : ",e);
            }
        });
    }
}
</script>